<?php
namespace app\statistics\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use think\Cache; 
use app\statistics\controller\Father;   
class Video extends Father
{      
    public function video_statis(){
        $_GPC = input();
        $page = isset($_GPC['page'])?$_GPC['page']:1;
        //时间排序
        if(empty($_GPC['time'])){
            $_GPC['time']['start'] = date('Y-m-d H:i:s',strtotime("-1 month"));
            $_GPC['time']['end'] = date('Y-m-d H:i:s',time());
        } 

        $start = $_GPC['time']['start'] ;
        $end = $_GPC['time']['end'] ;
        if (!empty($_GPC['start'])) {
            $start = $_GPC['start'] ;
        }
        if (!empty($_GPC['end'])) {
            $end = $_GPC['end'] ;
        }

        $start_time  = strtotime($start);
        if($end == date("Y-m-d")){
            $end_time  = strtotime($end.' '.date("H:i:s"));
        }else{
            $end_time  = strtotime($end);
        }


        $pindex = max(1, intval($page));
        $producer = isset($_GPC['producer'])?$_GPC['producer']:0;
        $psize = 500;
        $where = 'uniacid =8';


        $hwhere = '';
        if(!empty($start_time) && !empty($end_time)){
            $hwhere .= " AND addtime >= '{$start_time}' AND addtime <= '{$end_time}' ";
        }

        $keyword  = isset($_GPC['keyword'])?$_GPC['keyword']:'';
        if(!empty($keyword)){
            $where .= " AND title like '%".$keyword."%'";
        } 
        if ($producer) {
            $video = Db::query("SELECT id,producer,count(id) as v_num,sum(browsenum) as browsenum  FROM y_med_video WHERE ".$where." group by producer");

            foreach ($video as $k => $v) {
            
                $vids =  Db::query("SELECT * FROM " .tablename($this->table_video). " WHERE producer='".$v['producer']."'");

                if (!empty($vids) && count($vids) >0) {
                    $arr = [];
                    foreach ($vids as $key => $val) {
                        $arr[] = $val['id'];
                    }
                    $video[$k]['ids'] = implode($arr,',');
                }
            }

        }else{
            $video = Db::query("SELECT * FROM ims_y_med_video WHERE ".$where." ORDER BY displayorder DESC LIMIT " . ($pindex - 1) * $psize . ',' . $psize);
            $total = Db::query("SELECT count(id) FROM ims_y_med_video WHERE ".$where);  
        } 

        foreach ($video as $k => $v) {

            $where =  "vid=".$v['id'];

            if (@count(explode(',',$v['ids'])) >1) {
                $where = ' vid in ('.$v['ids'].')';
            }

            // 观看人数
            $sql = "SELECT * FROM ims_y_med_history WHERE ".$where.$hwhere.' group by uid';
            $count  = Db::query("SELECT COUNT(1) as c FROM(".$sql.") as a");
            
            $video[$k]['user_count'] = $count[0]['c'];
            // $user_count_all += $video[$k]['user_count'];

            // 播放量
            $browsenum = Db::query("SELECT sum(num) as num FROM ims_y_med_history WHERE ".$where.$hwhere);
            $video[$k]['browsenum'] = $browsenum[0]['num'];
            // $browsenum_all += $video[$k]['browsenum'];
            //收藏
            $collection = Db::query(" SELECT count(id) as count_id FROM ims_y_med_collect WHERE ctype='视频' AND ".$where.$hwhere.' GROUP BY id');
            $video[$k]['collection'] = isset($collection[0]['count_id'])?$collection[0]['count_id']:0;
            // $collection_all += $video[$k]['collection'];

        }
        // pe($browsenum_all);
        $sort  = isset($_GPC['sort']) ? intval($_GPC['sort']) : 1 ;
        $field = isset($_GPC['field']) ? $_GPC['field'] : 'browsenum' ;

        // $video = arr_sort($field,$sort,$video);
        // echo "<pre>";
        // print_r($video);die;
        $res = array('code'=>1,'msg'=>$video);
        return json($res);
    }
}