<?php
namespace app\statistics\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use think\Cache; 
use app\statistics\controller\Father;   
class Column extends Father
{     
    public function column_statis(){
        $_GPC = input();
       
        $pindex = 1;
        $psize = 2000;    
        $where = 1;
        $keyword = isset($_GPC['keyword'])?$_GPC['keyword']:'';
        //关键字搜索
        if($keyword){
            $where .= " and c.title like '%".$keyword."%'";
        }
        //统计区间排序
        if(empty($_GPC['time'])){
            $_GPC['timet']['start'] = date('Y-m-d H:i:s',strtotime("-1 year"));
            $_GPC['timet']['end'] = date('Y-m-d H:i:s',time());
        } 
        $tstart_time  = strtotime($_GPC['timet']['start']);
        if($_GPC['timet']['end'] == date("Y-m-d")){
            $tend_time  = strtotime($_GPC['timet']['end'].' '.date("H:i:s"));
        }else{
            $tend_time  = strtotime($_GPC['timet']['end']);
        }

        //时间排序
        if(empty($_GPC['time'])){
            $_GPC['time']['start'] = date('Y-m-d H:i:s',strtotime("-1 year"));
            $_GPC['time']['end'] = date('Y-m-d H:i:s',time());
        } 
        $start_time  = strtotime($_GPC['time']['start']);
        if($_GPC['time']['end'] == date("Y-m-d")){
            $end_time  = strtotime($_GPC['time']['end'].' '.date("H:i:s"));
        }else{
            $end_time  = strtotime($_GPC['time']['end']);
        }
        if(!empty($start_time) && !empty($end_time)){
            $where .= " AND c.addtime >= '{$start_time}' AND c.addtime <= '{$end_time}' ";
        }

        //是否免费
        if(empty($_GPC['is_price']) == 1){
            //收费
            $where .= " AND c.price != 0";
        }elseif($_GPC['is_price'] == 2){
            //免费
            $where .= " AND c.price = 0";
        }

        //获取栏目数据
        $column = Db::query("SELECT c.id,c.title,c.price,c.addtime FROM ims_y_med_column c WHERE ".$where." ORDER BY addtime DESC LIMIT " . ($pindex - 1) * $psize . ',' . $psize); 
        
        //查询所有的栏目订阅数 
        $column_sub_num = Db::query("SELECT count(id) num FROM ims_y_med_collect where ctype=3");

        foreach($column as $k=>$v){ 
            //查询该栏目视频数量
            $video_num = Db::query("SELECT count(*) num FROM ims_y_med_relation where type=1 and colid = ".$v['id']); 
            $column[$k]['video_num'] = $video_num[0]['num']; 

            //查询该栏目购买数量
            $order_num = Db::query("SELECT count(id) num FROM ims_y_med_column_order where colid = ".$v['id']." AND addtime >= '{$tstart_time}' AND addtime <= '{$tend_time}' ");
             
            $column[$k]['order_num'] = $order_num[0]['num']; 

            //查询该栏目订阅数量
            $sub_num = Db::query("SELECT count(id) num FROM ims_y_med_collect where ctype=3 and colid = ".$v['id']." AND addtime >= '{$tstart_time}' AND addtime <= '{$tend_time}' ");
            $column[$k]['sub_num'] = $sub_num[0]['num']; 
            

            //查询该栏目浏览量
            $look_num = Db::query("SELECT count(id) num FROM ims_y_med_history where colid = ".$v['id']." AND addtime >= '{$tstart_time}' AND addtime <= '{$tend_time}' ");
            $column[$k]['look_num'] = $look_num[0]['num']; 
            //转换率
            $subnum = $sub_num[0]['num'];
            $looknum = $look_num[0]['num'];
            if($subnum != 0 && $looknum != 0){ 
                $column[$k]['sub_num_rate'] = round(($subnum/$looknum)*100,2); 
            }else{
                $column[$k]['sub_num_rate'] = 0;
            }
            

        }
        $columnsort = isset($_GPC['columnsort'])?1:0;
        $ordersort = isset($_GPC['ordersort'])?1:0;
        $subsort = isset($_GPC['subsort'])?1:0;
        $ratesort = isset($_GPC['ratesort'])?1:0;
        if($columnsort == 1){
            $arr1 = array_map(create_function('$n', 'return $n["look_num"];'), $column);
            array_multisort($arr1,SORT_DESC,$column );//多维数组的排序
        }else{
            $arr1 = array_map(create_function('$n', 'return $n["look_num"];'), $column);
            array_multisort($arr1,SORT_DESC,$column );//多维数组的排序
        }
        if($ordersort == 1){
            $arr2 = array_map(create_function('$n', 'return $n["order_num"];'), $column);
            array_multisort($arr2,SORT_DESC,$column );//多维数组的排序
        } 
        if($subsort == 1){
            $arr3 = array_map(create_function('$n', 'return $n["sub_num"];'), $column);
            array_multisort($arr3,SORT_DESC,$column );//多维数组的排序
        } 
        if($ratesort == 1){
            $arr4 = array_map(create_function('$n', 'return $n["sub_num_rate"];'), $column);
            array_multisort($arr4,SORT_DESC,$column );//多维数组的排序
        }
         
        $res = array('code'=>1,'msg'=>$column);
        return json($res);
    }
}   