<?php
namespace app\statistics\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use think\Cache; 
use app\statistics\controller\Father;   
class User extends Father
{     
    public function user_follow(){
        $_GPC = input();
        $y = isset($_GPC['date'])?$_GPC['date']:'2017-11';  
        $time = date('Y-m-d',time()); 
        $starttimed = $y . '-01 00:00:00';
        $StartMonth   = $starttimed; //开始日期
        $EndMonth     = $time; //结束日期
        $arr = $this->getym($StartMonth,$EndMonth); 
         
        $table = array();
        if(!empty($arr)){
            $s = strtotime($y.'-01 00:00:00');
            $e = strtotime($y.'-30 23:59:59');
          
            $where = array(
                "followtime" => ['>=',$s],
                "followtime" => ['<=',$e],

            );
            $nowtimeall = Db::name('mc_mapping_fans')->field("follow,followtime,unfollowtime")->where($where)->select(); 
            
            foreach($arr as $key => $ym){
                $starttime = strtotime($ym.'-01 00:00:00');
                $endtime = strtotime($ym.'-30 23:59:59'); 
                $count = count($nowtimeall);
                if($count>0){
                    $takeoffnum = array();
                    $follownum = array();
                    foreach ($nowtimeall as $key => $value) {
                        // 开始时间必须是从开始时间判断，否则当月/总数看不出什么效果 
                        if($value['unfollowtime']!=0 && $value['unfollowtime'] >= strtotime($starttimed) && $value['unfollowtime'] <= $endtime){
                            $takeoffnum[] = $key;
                        }else{
                            $follownum[] = $key;
                        }
                    }
                    // p($takeoffnum);
                    // pe($follownum);
                }
                $table[$ym]['count'] = $count;//总用户数
                $table[$ym]['takeoffnum'] = $takeoffnum = count($takeoffnum);//取消关注数6980
                $table[$ym]['follownum'] = $follownum = count($follownum);//关注数5005 
                $table[$ym]['retentionnum'] = round($follownum / $count * 100);
                $table[$ym]['retention'] = round($follownum / $count * 100).'%';
            } 
        } 

        // echo "<pre>";
        // print_r($table);die;

        $res = array('code'=>1,'msg'=>$table);
        return json($res);
    }
    public function getym($StartMonth,$EndMonth){
        $ToStartMonth = strtotime( $StartMonth ); //转换一下
        $ToEndMonth   = strtotime( $EndMonth ); //一样转换一下
        $i            = false; //开始标示
        $arr          = array();
        while( $ToStartMonth <= $ToEndMonth ) {
          $NewMonth = !$i ? date('Y-m', strtotime('+0 Month', $ToStartMonth)) : date('Y-m', strtotime('+1 Month', $ToStartMonth));
          $ToStartMonth = strtotime( $NewMonth );
          $i = true;
          $arr[] = $NewMonth;
        }    
        array_pop($arr);
        return $arr; 
    }

}