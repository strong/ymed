<?php
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
class Doctor extends Base
{ 
    public function __construct(){
        parent::__construct();  
    }
	/**
     * 医生详情页 (只返回医生的详细信息)
     * @return [type] [description]
     */
    public function detail()
    {  
        //检测参数
        // $cc = $this->decrypt();  
        // if($cc['code'] != 1){ return json($cc);die;} 

        $teacherid = input('param.tid');
        if(!isset($teacherid)){
            $res = array('code'=>0,'msg'=>'参数有误');
            return json($res);
        }

        $doctor = $this->get_doctor([
            'tid'=>$teacherid,
            'uid'=>input('param.uid')
        ]);
        // echo "<pre>";
        // print_r($doctor);die;
        if(empty($doctor)){
            return json(['code' =>0, 'msg' => '不存在该医生']);
        }

        return json(['code' => 1,'msg' => $doctor]);
    }

    /**
     * [follow 关注医生]
     */
    public function follow(){
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $tid = input('param.tid');
        if($tid) json(['code' => 0, 'msg' => '您要添加哪个医生呢？']);
        $uniacid = input('param.uniacid');
        $uid = input('param.uid');

        $teacher = $this->get_doctor(['tid'=>$tid,'uid'=>$uid]);
        if($teacher == null) return json(['code' => 0, 'msg' => '该医生不存在']);

        $data = [
            'uid' => $uid,
            'uniacid' => $uniacid,
            'tid' => $tid,
            'ctype' => '讲师',
        ];

        $res = Db::name('y_med_collect')->where($data)->find();
        if(empty($res)){
            $id = Db::name('y_med_collect')->insertGetId($data);
            return json(['code' => 1, 'msg' => $id]);
        }else{
            Db::name('y_med_collect')->where('id', $res['id'])->delete();
            return json(['code' => 1, 'msg' => '取消成功']);
        }
    }
}