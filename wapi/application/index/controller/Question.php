<?php
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
class Question extends Base
{ 
    public function __construct(){
        parent::__construct();  
        
    } 

	/**
	 * [add_question 提问]
	 * @return [int] [添加的id] 
	 */
	public function add_question(){  
		//检测参数
        // $cc = $this->decrypt();
        // if($cc['code'] != 1){ return json($cc);die;}  
// echo "<pre>";
//         print_r($data);die;
		$input = input(); 
		// if(!isset($input['uid']))     return json(['code'=>0,'msg'=>'请登陆']); 
		// if(empty($input['cname']))   return json(['code'=>0,'msg'=>'请确认疾病']);
		// if(!isset($input['content'])) return json(['code'=>0,'msg'=>'请确认您的问题']);
		// if(!isset($input['age']))     return json(['code'=>0,'msg'=>'请确认您的年龄']);
		// if(!isset($input['sex']))     return json(['code'=>0,'msg'=>'请确认您的性别']);

		//查看是否存在该分类
        // $cate = Db::name('y_med_category')->where(['id' => $input['cid'], 'uniacid' => $input['uniacid']])->find();
        // if(empty($cate)) {
        // 	return json(['code' => 0, 'msg' => '没有该分类']);
        // }

        //查看是否存在该用户
        $user = Db::name('mc_members')->where(['uid' => $input['uid']])->find();
        if(empty($user)) {
        	return json(['code' => 0, 'msg' => '没有该用户']);
        }    
        if(empty($input['hid'])){
            $hid = 0;
        }else{
            $hid = $input['hid'];
        }
		$arr = array(
			'uid' => $input['uid'], 
            'cname' => $input['cname'],
			'relation' => $input['relation'],
			'content' => trim($input['content']),
			'age' => $input['age'],
			'sex' => $input['sex'],
			'status' => 1,
			'uniacid' => $this->uniacid,
			'addtime' => time(),
            'hid' => $hid
		);  
        $this->log_message($arr,'wapi.txt');
		$res = Db::name('y_med_question')->insert($arr);
        
$this->log_message($res,'wapi.txt');
		if($res){ 
            return json(['code'=>1,'msg'=>'成功']);
		}else{ 
            return json(['code'=>0,'msg'=>'添加失败']);
		}
	}

	/**
	 * [add_cate 添加关注分类]
	 * @return   [json]
	 */
	public function add_rel_cate(){
		$cc = $this->decrypt();     
        if($cc['code'] != 1){ return json($cc);die;} 

        $input = input();
        if(!isset($input['cid'])) {
        	return json(['code'=>0,'msg'=>'请选择疾病']);
        }

        //查看是否存在该分类
        $cate = Db::name('y_med_category')->where(['id' => $input['cid'], 'uniacid' => $input['uniacid']])->find();
        if(empty($cate)) {
        	return json(['code' => 0, 'msg' => '没有该分类']);
        }

        $data = [
            'uid' => $input['uid'], 
            'cid' => $input['cid'], 
            'ctype' => '疾病',
            'hid' => $input['hid'],
            'uniacid' => $input['uniacid']
        ];
        //查看当前用户是否添加过该分类
        $rel = Db::name('y_med_collect')->where($data)->find();
        if(!empty($rel)) {
        	return json(['code' => 2, 'msg' => '已经添加过该分类']);
        }

        $res = Db::name('y_med_collect')->insert($data);
        if($res){
            $id = Db::name('y_med_collect')->getLastInsID();
        	return json(['code' => 1, 'msg' => $id]);
        }else{
			return json(['code'=>0,'msg'=>'添加失败']);
        }
	}
}