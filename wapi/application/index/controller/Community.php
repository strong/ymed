<?php
namespace app\index\controller; 
use think\controller;
use think\Db;

use app\common\controller\Base; 
class Community extends Base
{ 
    public function __construct(){
        parent::__construct();     
    }
   
	//群组列表
    public function com_list()
    {    
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}

        $where['g.uniacid'] = input('param.uniacid')?input('param.uniacid'):$this->uniacid;
        if (!empty(input('param.cid'))) { 
            $where['gc.cid'] = ['in',input('param.cid')];
        }
        if (!empty(input('param.keyword'))) {
            $where['g.group_name'] = ['like','%'.trim(input('param.keyword')).'%'];
        }
        if (!empty(input('param.id'))) {
            $where['g.id'] = input('param.id');
        }
        $arr['limit'] = $this->pagetLimit();
        $where['g.status'] = '启用'; 
        $com_list = Db::name('y_med_group g')->field('gc.id,group_name,num,content,qr_img,max,uniacid,gc.cid,img,status,g.id as gid')->join('y_med_group_cat gc','g.id=gc.gid','left')->where($where)->limit($arr['limit'])->select(); 

        if(empty($com_list)){
            return json(['code' => 0, 'msg' => '没有数据']);
        }
        return json(['code' => 1, 'msg' => $com_list]);
    }  


}