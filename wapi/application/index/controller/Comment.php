<?php
/**
 * 评论模块
 */
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
class Comment extends Base
{   
    /**
     * [comment 评论]http://www.yuntehu.com/wapi/public/index.php/index/comment/comment?vid=11&content=123123123
     * param vid   [视频id]
     * @return [json] [description]
     */
    public function comment(){ 
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $pid = intval(input('param.pid'));
        $vid = intval(input('param.vid'));
        if(empty($vid)){
            return json(array('code'=>0,'msg'=>"参数有误"));
        } 
        $res = $this->videopinlun($vid,$pid);
        return json($res);
    }
 
    /**
     * [fabulous 评论点赞]
     * @return [type] [description]
     */
    public function fabulous_commont(){  
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}  

        $eid = intval(input('param.eid'));
        if(empty($eid)){
            return json(array('code'=>0,'msg'=>"参数有误"));
        } 
        $this->eid = $eid; 
        $res = $this->commenzan();
        return json($res);
    }
 
    /**
     * 获取评论列表
     * @param  string  $vid   [视频id] 
     * @return [type]        [description]
     */
    public function evaluate_list(){ 
        $limit = $this->pagetLimit();
        $vid = intval(input('param.vid'));
        $pid = intval(input('param.pid'))?intval(input('param.pid')):0;
        if(empty($vid)){
            return json(array('code'=>0,'msg'=>"参数有误"));
        }
        $where = array('e.vid'=>$vid,'pid'=>$pid,'e.status'=>3);
 
        $pllist = Db::name('y_med_evaluate e')->field('e.*,u.avatar,v.type')->join('mc_members u','u.uid = e.uid','left')->join('y_med_app_vote v', 'e.uid=v.uid and e.vid = e.uid','left')->where($where)->limit($limit)->order('e.addtime desc')->select(); 
        $plcount = Db::name('y_med_evaluate e')->where($where)->count();
        if(!empty($pllist)){  
            foreach ($pllist as $k => $v) {
                //查询点赞
                $isevaluatezan = Db::name('y_med_collect')->field('id')->where(['ctype'=>"评论",'uid'=>$this->uid,'eid'=>$v['id']])->find();  
                $evaluateNum = Db::name('y_med_collect')->where(['ctype'=>"评论",'eid'=>$v['id']])->count();
                
                $pllist[$k]['isevaluateNum'] = $evaluateNum;
                $pllist[$k]['isevaluatezan'] = intval($isevaluatezan['id'])?true:false;
            } 
        }

        return json(array('list'=>$pllist,'count'=>$plcount));
    }

    //视频评论
    private function videopinlun($vid,$pid=0){
        //查询最近一次评论
        $lastC = Db::name('y_med_evaluate')->field('addtime')->where(['uid'=>$this->uid,'vid'=>$vid])->order('addtime desc')->find();
        if($lastC['addtime'] > (time()-10)){ 
            return array('code'=>0,'msg'=>'您发言太过频繁请稍后再试。。。'); 
        }else{ 
            //解密评论内容
            $con = urldecode(input('param.content'));
            $content = $this->urlencodeEmoji($con); 
            $input = input('param.');
            //组合内容写入数据库
            $data = array();    
            $data['uid'] = $input['uid'];
            $data['nickname'] = $input['nickname'];
            $data['addtime'] = time();
            $data['content'] = $content;
            $data['vid'] = $vid;
            $data['pid'] = $pid;
            $data['status'] = 3;
            $commid = Db::name('y_med_evaluate')->insertGetId($data); 
            return array('code'=>1,'msg'=>"评论成功！"); 
        } 
    }

    // urlencode emoji表情
    public function urlencodeEmoji($str)
    {
        $str = preg_replace_callback(
           '/./u',
           function (array $match) {
            return strlen($match[0]) >= 4 ? urlencode($match[0]) : $match[0];
           },
           $str); 
        return $str;
    } 

    
    
    //评论点赞
    private function commenzan(){
        $ctype = 5; 
        $lastC = Db::name('y_med_collect')->field('id')->where(['ctype'=>"评论",'uid'=>$this->uid,'eid'=>$this->eid])->find();
        
        if(empty($lastC)){
            $data = array(); 
            $data['uid'] = $this->uid; 
            $data['eid'] = $this->eid;
            $data['addtime'] = time();
            $data['ctype'] = $ctype; 
            Db::name('y_med_collect')->insert($data);
            return array('code'=>1,'msg'=>'已点赞！');
        }else{ //存在就取消
            Db::name('y_med_collect')->where('id',$lastC['id'])->delete();
            return array('code'=>1,'msg'=>'已取消赞！');

        }
    } 
}