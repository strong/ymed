<?php
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
class Ucenter extends Base
{ 
    public function __construct(){
        parent::__construct();    

    }

    public function get_user(){
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}

        $input = input();
        if(!isset($input['uid'])) return json(['code'=>0,'msg'=>'请登陆']);
       
        $field = isset($input['field']) ? $input['field']: '*';

        //获取用户信息
        $user = Db::name('mc_members')->field($field)->where(['uid' => $input['uid'], 'uniacid' => $input['uniacid']])->find();
        // echo "<pre>";
        // print_r($user);die;
        $user['lookingfor'] = json_decode($user['lookingfor'], true);
        $user['lookingfor']['cid'] =  Db::name('y_med_category')->where(['id' => $user['lookingfor']['cid']])->find();
        $user['birthday'] = $user['birthyear'].'-'.$user['birthmonth'].'-'.$user['birthday'];
        
        if($user){
            return json(['code' => 1, 'msg' => $user]);
        }
        return json(['code' => 0, 'msg' => '没有该用户']);
    }

    /**
     * [get_history 获取浏览记录]
     * @return [type] [description]
     */
    public function get_history(){
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 
 
        $input = input();
        if(!isset($input['uniacid']) || !isset($input['uid'])) return json(['code' => 0, 'msg' => '请确认参数']);

        $where = [
            'uid' => $input['uid'], 
            'uniacid' => $input['uniacid'],
        ];

        $lastC = Db::name('y_med_history')->field('vid')->where($where)->order('addtime desc')->limit($this->pagetLimit())->select();
        $videocount = Db::name('y_med_history')->field('vid')->where($where)->count();
        
        if(empty($lastC)){
            $res = array('code'=>0,'msg'=>'您还没有记录');
            return json($res);
        }
        
        $vids = [];
        foreach($lastC as $v){
            $vids[] = $v['vid'];
        }
        $videolist = $this->getVideo(['vid'=>implode(',', $vids)]);
        if(empty($videolist)){
            $res = array('code'=>0,'msg'=>'您还没有记录');
            return json($res);
        }

        $res = array('code'=>1,'msg'=>array('count'=>$videocount,'list'=>$videolist));
        return json($res); 
    }

     /**
     * [get_collection 获取收藏]
     * @return [type] [description]
     */
    public function get_collection(){ 
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 
    
        $input = input();
        if(!isset($input['uniacid']) || !isset($input['uid'])) return json(['code' => 0, 'msg' => '请确认参数']);

        $where = [
            'uid' => $input['uid'], 
            'uniacid' => $input['uniacid'],
            'ctype' => '视频'
        ];
       
        $lastC = Db::name('y_med_collect')->field('vid')->where($where)->order('addtime desc')->limit($this->pagetLimit())->select();
        $videocount = Db::name('y_med_collect')->field('vid')->where($where)->count();
       
        if(empty($lastC)){
            $res = array('code'=>0,'msg'=>'您还没有记录');
            return json($res);
        }

        $vids = [];
        foreach($lastC as $v){
            $vids[] = $v['vid'];
        }

        $videolist = $this->getVideo(['vid'=>implode(',', $vids)]);
        
        if(empty($videolist)){
            $res = array('code'=>0,'msg'=>'您还没有记录');
            return json($res);
        }

        $res = array('code'=>1,'msg'=>array('count'=>$videocount,'list'=>$videolist));
        return json($res); 
    }

     
	/**
     * [doctor 我关注的医生]
     * @return [type] [description]
     */
    public function follow_doctor()
    {  
    	//检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $uid = input('param.uid'); 
        $where = ['ctype' => '讲师','uid' => $uid];
        //我关注的医生id
        $doctorids = Db::name('y_med_collect')->field('tid')->where($where)->order('addtime desc')->select();
        $doctorcount = Db::name('y_med_collect')->field('tid')->where($where)->count();
        if(empty($doctorids)){
            $res = array('code'=>0,'msg'=>'您还没有关注医生');
            return json($res);
        }
        $dids = '';
        foreach($doctorids as $k=>$v){
            $dids .=$v['tid'].',';
        }

        $doctorlist = $this->get_doctor(['tid'=>trim($dids,','), 'uid'=>$uid]);
        
        $res = array('code'=>1,'msg'=>['list'=>$doctorlist, 'count'=>$doctorcount]);
        return json($res); 
    }   

    //获取用户订阅
    public function get_subscribe(){
         //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $uid = input('param.uid');
        if(!$uid) return json(['code' => 0, 'msg' =>'请确认参数']);

        $co = Db::name('y_med_collect')->field('colid')->where(['uid' => $uid,'ctype' =>'栏目'])->select();
        if(empty($co)){
            return json(['code'=>0,'msg'=>'暂时没有订阅']);
        }
        $colids = [];
        foreach($co as $v){
            $colids[] = $v['colid'];
        }
        $col = $this->getcol(['colid'=>implode(',',$colids)]);
        if(empty($col)){
            return json(['code' => 0, 'msg' => '暂时没有订阅的栏目']);
        }
        return json(['code' => 1, 'msg' => $col]);
    }

    

    /**
     * [information 完善个人信息]
     * @return [type] [description]
     */
    public function information(){
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $nickname = addslashes(input('param.nickname'));//姓名
        $mobile = input('param.mobile');//手机号
        $gender = trim(input('param.gender'));//1男2女
        $birthday = addslashes(trim(input('param.birthday')));//年龄
        $cid = intval(input('param.cid'));//年龄
        $jb = addslashes(trim(input('param.jb')));//年龄
        $content = addslashes(trim(input('param.content')));//年龄
        
        if($nickname){
            $data['nickname'] = $nickname;
        }
        if($mobile){
            $data['mobile'] = $mobile;
        }
        if($gender){
            $data['gender'] = $gender;
        }
        
        if($birthday){
            $bir = explode('-',$birthday);
            $data['birthyear'] = $bir[0];
            $data['birthmonth'] = $bir[1];
            $data['birthday'] = $bir[2];
        }
        if($cid && $jb && $content){
            $res = Db::name('y_med_category')->where(['id'=>$cid])->find();
            if(empty($res)){
                return json(['code' => 0, 'msg' => '该科室不存在']);
            }

            $data['lookingfor'] = json_encode([
                'cid' => $cid,
                'jb' => $jb,
                'content' => $content
            ]);
        }
        $res = Db::name('mc_members')->where('uid', input('param.uid'))->update($data); 
        //echo Db::name('mc_members')->getlastsql();
        if($res){
            $return = array('code'=>1,'msg'=>'修改成功');
        }else{
            $return = array('code'=>0,'msg'=>'修改失败');
        }
          
        return json($return); 
    } 

    /**
     * 合作/意见反馈
     * @return [type] [description]
     */
    public function cooperation(){
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $data = [
            'uid' => input("param.uid"),
            'cooperation' => input("param.cooperation"),//合作意向
            'type' => input("param.type"),
        ];
        
        if(input('param.type') == 1){
            $data['name'] = input("param.name");
            $data['position'] = input("param.position");//职称
            $data['phone'] = input("param.phone");
            $data['gender'] = input("param.gender");
            $data['age'] = input("param.age");
            $data['hospital'] = input("param.hospital"); //医院
            $data['department'] = input("param.department"); //科室
            $data['email'] = input("param.email");
        }

        if(input('param.hid')){
            $data['hid'] = input("param.hid");
        }

        $res = Db::name('y_med_cooperation')->insertGetId($data);
        if($res){
            return json(array('code'=>1,'msg'=>$res));
        }
        return json(array('code'=>0,'msg'=>'失败'));
    }

    /**
     * [get_buy 我的购买]
     * @return [type] [description]
     */
    public function get_buy(){
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}

        $uid = input('param.uid');
        $col = Db::name('y_med_column_order')->where(['uid'=>$uid])->select();
        if(empty($col)) return json(['code'=>0, 'msg'=>'暂时没有购买']);
        $cols = '';
        foreach($col as $k=>$v){
            $cols .=','.$v['colid'];
        }
        $cols = trim($cols,',');
        $order = $this->getcol(['colid'=>$cols]);
        // echo "<pre>";
        // print_r($order);die;
        
        return json(['code'=>1,'msg'=>$order]);
    }

    /**
     * 发送短信验证码
     * @return [type] [description]
     */
    public function getcode(){
        $mobile = input('param.mobile');
        $type = input('param.type');
        $checkcode = input('param.code');
        
        $code = $this->yzmGet($mobile,$type); 
        
        return json(['code'=>$code,'msg'=>'操作完成']);
    }

    /**
     * [firstsub 第一次订阅]
     * @return [type] [description]
     */
    public function firstsub(){
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}

        $uid = input('param.uid');
        $hid = input('param.hid');
        $where = [
            'uid'=> $uid,
            'hid'=> $hid,
            'ctype'=> '一次性订阅'
        ];
        $already = Db::name('y_med_collect')->where($where)->find();
        if($already) return json(['code'=>0,'msg'=>'已经订阅']);

        $where['addtime'] = time();
        $res = Db::name('y_med_collect')->insert($where);
        if(!$res) return json(['code'=>0,'msg'=>'订阅失败']);

        return json(['code'=>1,'msg'=>'订阅成功']);
    }

    /*
        手机绑定页面
    */
    public function userbmobile(){
        //接受验证手机号
        $uid = $this->uid; 
        if(!empty($uid)){
            $mobile = input('param.mobile');
            $code = input('param.code');
            if(!empty($mobile)){
                $is_mobile = yz_mobile($mobile);
                if(empty($is_mobile)){ 
                    $res = array('code'=>0,'msg'=>'请输入正确的手机号'); 
                    return json($res); 
                }
                if(empty($code)){ 
                    $res = array('code'=>0,'msg'=>'请填写验证码');
                    return json($res); 
                }
                //验证code
                $is = $this->yzmyanzheng($mobile,$code,'small');
                if($is){
                    // 进行绑定操作
                    // 现有用户绑定手机号
                    if($uid){
                        $dataP['mobile'] = $mobile;  
                        Db::name('mc_members')->where('uid',$uid)->update($dataP);
                        $res = array('code'=>1,'msg'=>'绑定成功'); 
                        return json($res);
                    } 
                }else{ 
                    $res = array('code'=>0,'msg'=>'验证码已失效'); 
                    return json($res); 
                }
            } 
        }else{
            $res = array('code'=>0,'msg'=>'非法请求'); 
            return json($res); 
        }
    }

   
    /**
     * [synopsis 公司介绍]
     * @return [type] [description]
     */
    public function synopsis(){ 
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $info = array();

        $info['jianjie'] = "　　医知TV是面向广大患者朋友的权威疾病知识发布平台，由中国医药教育协会（CMEA）与北京医脉达科技有限公司（Y-MED）联手打造，是医脉达旗下面向患者进行权威、客观疾病知识普及的患者教育产品。
　　现已有全国300多位医疗行业领袖加入，并与全国200多家三级甲等医院达成长期战略合作，通过采集权威专家对疾病的深入剖析内容，深入浅出讲解疾病治疗、康复与预防知识，已为全国50多万名患者带去福音。我们不懈努力，力求做到“疾病资讯最权威普及”，“患者问题最深入解答”，“健康知识最快速获取”，多维度、多病种的，消除网络及现实生活中充斥于患者周边的健康知识杂音，保障和服务于广大患者朋友康复全过程。正本清源，为提高全民健康素养贡献一份力量！
　　我们真诚欢迎，权威的医疗专家、优秀的医学人才加入我们，共同发声：
“让患者朋友……
少走弯路，多些坦途；
少些困惑，多些选择；
少些无助，多些自助；
少些遗憾，多些幸福！“";
        $info['hezuojj'] = "
        说明：
    　　欢迎热衷患者教育事业的各级医疗机构、优秀医护人员，医学界的有识之士，加入我们，共同支持患者教育事业。（要求：临床医生或学科带头人，扎根于某一疾病领域研究10年以上，有着丰富的治愈病患经历，能够将患者关心的问题，深入浅出、客观的进行分析与讲解。） 
    　　诚邀各方平台，优势互补，共同发展患者教育事业。";
        $info['lianxi']['dizhi'] = "北京中关村医学工程转化中心1号 医脉达科技
      北京市丰台区六里桥1号奈伦大厦";
        $info['lianxi']['kefu'] = "010-80456160"; 
        $info['lianxi']['wangzhi'] = "www.yuntehu.com";
        $info['lianxi']['youxiang'] = "ymed@y-med.com.cn";
        $info['lianxi']['gongzhonghao'] = "服务号：医脉达
订阅号：医脉达官微
小程序：医知TV
";

        $res = array('code'=>1,'msg'=>$info); 
        return json($res); 
    }
     
}