<?php
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use think\Cache;
use app\common\controller\Base;  
class Index extends Base
{ 
	public function __construct(){
        parent::__construct();   
    } 

    //回调地址
    public function notifyUrl(){
        echo 1; 
    }
        
    /**
     * 获取广告信息（只要是广告都可以）
     */
    public function get_advertisement(){
        //检测参数
        $cc = $this->decrypt(); 
        if($cc['code'] != 1){ return json($cc);}

        $hid = input('param.hid')?input('param.hid'):0;
        $locat = input('param.locat')?input('param.locat'):'';
        if(empty($locat)){
            $res = array('code'=>0,'msg'=>"参数有误");
            return json($res);
        }
        $where = array(
            'a.uniacid'=>$this->uniacid, 
            'a.status'=>"启用",
            'ap.status'=>1,
            'ap.location'=>$locat,
            'a.hid'=>$hid
        ); 
        $advert = Db::name('y_med_advertisement a')->field('a.title,a.img,a.sort,a.status')->join("y_med_advertising_position ap","ap.id=a.locationid",'left')->where($where)->select();
        // print_r($advert);die;
        if(empty($advert)){
            $res = array('code'=>0,'msg'=>"没有数据！");
            return json($res);
        }
        $res = array('code'=>1,'msg'=>$advert);
        return json($res);
        
    } 

    /**
     * [get_categoryList description]
     * param.pid  父id
     * param.cid  疾病id
     * param.keyword  搜索关键词
     * param.isuser   是否返回用户关注疾病 true or false
     * @return [type] [description]
     */
    public function get_categoryList(){
    	//检测
    	$cc = $this->decrypt();
    	if($cc['code'] != 1){ return json($cc);}

        $where = array(
            'uniacid'=>$this->uniacid 
        );
        $input = input();
        
        if (isset($input['pid'])) {
            $where['parentid'] = $input['pid'];
        }

        // if (isset($input['hid'])) {
        //     $where['hid'] = $input['hid'];
        // }
        
        $cid = input('param.cid')?input('param.cid'):'';
        if($cid){
            $where['id'] = $cid;
        }
        $keyword = input('param.keyword')?input('param.keyword'):'';
        if($keyword){
            $where['title'] = $keyword;
        }

        $isparent = input('param.isparent')?input('param.isparent'):'';
        if($isparent){
            $where['parentid'] = ['<>', 0];
        }

        $isuser = input('param.isuser')?input('param.isuser'):false;
        if($isuser){
            //获取用户关注的疾病
            $user_cat_ids = Db::name('y_med_collect')->field('cid')->where(array('uid'=>$this->uid,'ctype'=>'疾病'))->order('addtime desc')->select(); 
            if($user_cat_ids){
                foreach ($user_cat_ids as $k => $v) {
                    $ids[] = $v['cid'];
                }
                $string_ids = implode(',', $ids);
                $where['id'] = ['in',$string_ids];
            }else{
                $res = array('code'=>0,'msg'=>"用户没有关注任何病例");
                return json($res);
            }  
        }
        $field = 'id,title,section,ico,parentid,content,en_title,img,symptom';
        if(input('field')){
            $field = input('param.field');
        }
        
    	//获取分类
        $categoryList = Db::name('y_med_category')->field($field)->where($where)->order('sort desc')->select(); 
        if(empty($categoryList)){
            $res = array('code'=>0,'msg'=>"没有该病例");
            return json($res);
        } 
    	$list = array( 
    		'categoryList'=>$categoryList,  
    	);
    	$res = array('code'=>1,'msg'=>$list);
    	return json($res);
    }

    /**
     * [add_categoryid 关注疾病] 
     * param.cid  疾病id   123,123,123
     * @return [type] [description]
     */
    public function add_categoryid(){  
    	//检测
    	$cc = $this->decrypt(); 
    	if($cc['code'] != 1){ return json($cc);}

    	$c_id = input('param.cid')?input('param.cid'):'';
    	$uid = input('param.uid')?input('param.uid'):'';
    	
        if(!empty($uid)){
            Db::name('y_med_collect')->where(['uid'=>$uid,'ctype'=>'疾病'])->delete();
            if(!empty($c_id)){
                $c_id = explode(',', $c_id); 
                foreach($c_id as $v){
                    $category = array(
                        'uid' => $uid, 
                        'ctype' => 4,
                        'cid' => $v,
                        'addtime' => time()
                    ); 
                    if(!empty($v)){
                        Db::name('y_med_collect')->insert($category);
                    } 
                }
            }
            $res = array('code'=>1,'msg'=>$c_id);
        }else{
            $res = array('code'=>0,'msg'=>"失败");
        }  
    	return json($res);
    } 

    public function getsearch(){
    	//检测
    	$cc = $this->decrypt(); 
    	if($cc['code'] != 1){ return json($cc);}

    	$data  = $this->searchResult();
    	return json($data);
    } 

    //更新云搜数据
    public function get_data(){
        header("Content-type: text/html; charset=utf-8"); 
        $sql = "SELECT flp.bookname,flt.company,fls.id,fls.kuozhanlabel,flc.name,fls.question,fls.synopsis,flt.teacher,fls.title FROM ims_fy_lesson_parent as flp inner join ims_fy_lesson_son as fls on flp.id = fls.parentid inner join ims_fy_lesson_teacher as flt on flp.teacherid = flt.id inner join ims_fy_lesson_category as flc on flp.cid = flc.id WHERE recommendid in (38,39) and flp.status = 1 and fls.status = 1";
        $data = Db::query($sql);
        //echo "<pre>";
        // print_r($data);die;
        // $arr = [];
        // $arr['content'] = $data;
        // echo "<pre>";
        // print_r($arr);die;
        print_r(json_encode($data,JSON_UNESCAPED_UNICODE));
    }
}