<?php
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
class Video extends Base
{ 
    public function __construct(){
        parent::__construct();    
    }

     /**
     * [video_list          获取视频信息]
     * param.colid          栏目id
     * param.vid            视频id
     * param.cid            疾病id
     * param.keyword        栏目关键词 like
     * param.hid            医院id
     * param.type           视频类型 免费/收费/变现
     * param.order          排序 默认displayorder desc,id asc
     * param.limit          分页
     * param.t_u            是否为用户推荐 1推荐已关注 2推荐未关注
     * @return              视频列表
     */
    public function video_list()
    {
        //检测参数
        // $cc = $this->decrypt();  
        // if($cc['code'] != 1){ return json($cc);die;}
        
        $vlist = $this->getVideo(input()); 
        
        if(empty($vlist)){
            $res = array('code'=>0,'msg'=>'');
        }else{
            $res = array('code'=>1,'msg'=>$vlist);  
        }
        return json($res);
    }

	/**
     * 视频详情 http://www.yuntehu.com/wapi/public/index.php/index/video/detail?vid=5
     * @return [type] [description]
     */
    public function detail()
    {    
        //检测参数
        // $cc = $this->decrypt();  
        // if($cc['code'] != 1){ return json($cc);die;}

        $vlist = $this->getVideo();
 
    	if(empty($vlist) && empty($vlist['0'])){
    		$res = array('code'=>0,'msg'=>'数据有误');
            return json($res);
    	}
        $vlist = $vlist['0']?$vlist['0']:'';
         
    	$list = array(
            'video_detail'=>$vlist,  
    	);

// print_r($list);die;
    	$res = array('code'=>1,'msg'=>$list);
       
    	return json($res);
    }   

    /**
     * [collection 收藏]
     * @return [type] [description]
     */
    public function collection(){  
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}  

        $vid = input('param.vid');
        $uniacid = input('param.uniacid');
        $uid = input('param.uid');
        if(!$vid || !$uid || !$uniacid){
            return json(array('code'=>0,'msg'=>input()));
        } 

        $data = [
            'uid' => $uid,
            'uniacid' => $uniacid,
            'vid' => $vid,
            'ctype' => '视频',
        ];

        $res = Db::name('y_med_collect')->field('id')->where($data)->find();
        if(empty($res)){
            $data['addtime'] = time();
            $id = Db::name('y_med_collect')->insertGetId($data);
            Db::name('y_med_video')->where(['id'=>$vid])->setInc('collection');
            return json(['code' => 1, 'msg' => $id]);
        }else{
            Db::name('y_med_collect')->where('id', $res['id'])->delete();
            Db::name('y_med_video')->where(['id'=>$vid])->setDec('collection');
            return json(['code' => 1, 'msg' => '取消收藏成功']);
        }
        
    }

    
    /**
     * [browse_records 浏览记录]
     * @return [type] [description]
     */
    public function browse_records(){ 
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}  
        $input = input();
        $vid = intval(input('param.vid'));
        if(empty($vid)){
            return json(array('code'=>0,'msg'=>"参数有误"));
        } 
        
        $where = [
            'uid' => $input['uid'],
            'vid' => $vid,
            'uniacid' => $input['uniacid'],
        ];
        $lastC = Db::name('y_med_history')->field('id')->where($where)->find();
        
        //更新播放次数
        Db::name('y_med_video')->where('id',$vid)->setInc('browsenum');
        //没有就添加
        if(empty($lastC)){ 
            $where['addtime'] = time();
            Db::name('y_med_history')->insert($where); 
        }else{ //存在就修改addtime  
            Db::name('y_med_history')->where('id',$lastC['id'])->update(['addtime' =>time()]); 
            Db::name('y_med_history')->where('id',$lastC['id'])->setInc('num');
        }
        return json(array('code'=>1,'msg'=>'成功')); 
    } 
    /**
     * [栏目浏览记录]
     * @return [type] [description]
     */
    public function column_records(){ 
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}  
        $input = input();
        $colid = intval(input('param.colid'));
        if(empty($colid)){
            return json(array('code'=>0,'msg'=>"参数有误"));
        } 
        
        $where = [
            'uid' => $input['uid'],
            'colid' => $colid,
            'uniacid' => $input['uniacid'],
            'addtime' => time()
        ];
        Db::name('y_med_history')->insert($where);
        
        return json(array('code'=>1,'msg'=>'成功')); 
    } 

    //验证视频是否收费
    public function check_free(){
        //检测参数
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;}  
        $input = input();

        $vid = input('param.vid');
        $uid = input('param.uid');
        if(!isset($vid)) return json(['code'=>0,'msg'=>'视频不存在']);

        $col = $this->getcol(['vid'=>$vid,'uid'=>$uid,'uniacid'=>$this->uniacid,'is_money'=>1]);
        
        if(!empty($col)){
            if($col[0]['price'] > 0){
                if($col[0]['user']['isbuy'] == 0){
                    return json(['code'=>0,'msg'=>$col[0]['id']]);
                }
            }
        }
        return json(['code'=>1,'msg'=>$col]);
    }
}