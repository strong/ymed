<?php
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
use app\index\controller\Message;

class Column extends Base
{ 
    public function __construct(){
        parent::__construct();    
    }

     /**
     * [col_list            获取栏目信息]
     * param.colid          栏目id
     * param.vid            视频id
     * param.cid            疾病id
     * param.keyword        栏目关键词 like
     * param.hid            医院id
     * param.is_money       是否收费 1 收费 2 免费
     * param.order          排序 默认id desc
     * param.limit          分页
     * param.t_u            是否为用户推荐 1推荐已关注 2推荐未关注
     * @return              栏目
     */
    public function col_list()
    {     
        //检测参数 
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $arr = input();
        if (isset($arr['uid']) && empty($arr['uid'])) {
            unset($arr['uid']);
        }
        $t_u = isset($arr['t_u']) ? $arr['t_u'] : input('param.t_u');
 
        if ($t_u == 2) {
            // 获取关注的数据
            $cols_in = $this->get_relation('colid',['hid'=>input('param.hid'),'cid'=>input('param.cid')]);
            
            $cols_all = $this->get_relation('colid',['cid'=>'']);

            $cols_in = explode(',',$cols_in);
            $cols_all = explode(',',$cols_all);
            $colids = array_diff($cols_all,$cols_in);

            $cols = $this->getcol(['colid'=>$colids,'cid'=>0]);
            
        }else{
            $cols = $this->getcol($arr);
        }

        if(empty($cols)){
            $res = array('code'=>0,'msg'=>'没有数据了');
        }else{

            $res = array('code'=>1,'msg'=>$cols);
        }

        return json($res);

    }  

    //栏目订阅
    public function col_add(){
        //检测参数
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $input = input();
        if(!isset($input['colid']) || !isset($input['uid'])) {
            return json(['code' => 0, 'msg' =>'数据有误']);
        }

        //查看栏目是否存在
        $res = Db::name('y_med_column')->where(['id'=>$input['colid']])->find();
        if(empty($res))return json(['code' => 0, 'msg' => '该栏目不存在']);

        $data = [
            'uid' => $input['uid'],
            'uniacid' => $input['uniacid'],
            'colid' => $input['colid'],
            'ctype' => '栏目'
        ];

        //查看是否已经订阅
        $res = Db::name('y_med_collect')->where($data)->find();
        if($res){
            Db::name('y_med_collect')->where($data)->delete();
            Db::name('y_med_column')->where(['id'=>$res['colid']])->setDec('subscribe');
            return json(['code'=>1,'msg'=>'取消关注']);
        }

        $data['addtime'] = time();
        $res = Db::name('y_med_collect')->insert($data);
        if($res){
            $insertid = Db::name('y_med_collect')->getLastInsID();
            Db::name('y_med_column')->where(['id'=>$input['colid']])->setInc('subscribe');
            $this->add_message(['colid'=>$input['colid'],'uid'=>$input['uid'],'hid'=>input('param.hid')]);
            return json(['code' => 1, 'msg' => $insertid]);
        }
        return json(['code' => 0, 'msg' => '添加失败']);
    }

    public function add_message($ar=[])
    {

        $message = New Message();
        $arr['type'] = input('param.type') ? input('param.type') : 1;
        $arr['colid'] = input('param.colid') ? input('param.colid') : $ar['colid'];
        $arr['uid'] = input('param.uid') ? input('param.uid') : $ar['uid'];

        $col = $this->getcol(['colid'=>$arr['colid'],'hid'=>input('param.hid')]);
        $col = $col[0];
        $user = Db::name('mc_mapping_fans')->field('openid')->where(['uid'=>$arr['uid']])->find();
        
        $data['type'] = $arr['type'];
        $data['url']  = 'http://wechat.y-med.com.cn/wb/index.php/index/column/col_detail/id/'.$col['id'];
        $data['openid']  = $user['openid'];
        if ($data['type'] ==1) {
            $data['key1'] = $col['title'];
            $data['key2'] = $col['doctor'];
            $data['key3'] = "共{$col['v_num']}节课程";
        }else if($data['type'] ==2){
            $data['key1'] = $col['title'];
            $data['key2'] = $col['price'].'元';
            $data['key3'] = date("Y年m月d日 h:i",time());

        }

        $message->add_message($data);
        return json(['code'=>1,'msg'=>$data]);
    }
}