<?php
namespace app\index\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
class Message extends Base
{   
    public $template_column = '-ZIRWNlxSKYu8K4nXQSH8LL3TrnigkOQ9E4zeGDKqDs';
    public $template_buy_column = 'lXe-OOjCOOp_zXpplbXlcEQrPsmJgfh8IIm9hFnG4EE';
    public $template_hospital = '';
    public $template_doctor = '';

    public $template_lesson_up = '03fkgh9YWwHNQk0FV7x1ZEp1Fjlk8WBqc5dMH1czjzg'; //视频上新
    public $template_lesson_jd = 'cO8PuZkHhoIo0P295mP0SYcHJq1Bh9tHD3B__qKODrw'; //视频进度
    public function __construct(){
        parent::__construct();    
    }

    public function aaa(){ 
        $arr = array();
        $arr['qwe'] = '123';
        $arr['asd'] = '456';
        unset($arr['asd']);
        // $res = $this->get_openid_4(array('openid'=>'oSZTVwnogrtFGVn62E303JAjB-7Y'));
        print_r($arr);
        // $template_column = OPENTM408355854
        // $template_lesson_up = TM408917738
        // $template_buy_column = OPENTM401262563
    }


    public function get_openid_4($arr=array()){ 
        $where = array(); 
        if(!empty($arr['uid'])){
            $where['uid'] = $arr['uid'];
        }
        if(!empty($arr['openid'])){
            $where['openid'] = $arr['openid']?$arr['openid']:'';
        } 

        $info = Db::name('mc_mapping_fans')->field('uid,unionid')->where($where)->find();
        $platform = Db::name('mc_members')->field('platform')->where('uid',$info['uid'])->find();
        if($platform['platform'] == 1 || $platform['platform'] == 3){  
            $this->template_column = "lBVFN90xvtYuyZRgKvWcucR0p6-SctDcOWxGTCq6Y7s";
            $this->template_lesson_up = "U_SpT2xAVX67gP5H2EmRWewkh8rDGOOUqGAoyQFkCRI";
            $this->template_buy_column = "30ZMf4mijhlovKPzGmwzSOYNQPKGDHz-Oy4uzHOU0FI";
            $info_4 = Db::name('mc_mapping_fans')->field('openid')->where(['unionid'=>$info['unionid'],'uniacid'=>4])->find(); 
            if(!empty($info_4['openid'])){
                return $info_4['openid'];
            }
        }
        return '';
    }

    public function is_platform($arr=array()){
        $where = array(); 
        if(!empty($arr['uid'])){
            $where['uid'] = $arr['uid'];
        }
        if(!empty($arr['openid'])){
            $where['openid'] = $arr['openid']?$arr['openid']:'';
        } 

        $wechat_4 = Db::name('account_wechats')->where('uniacid',4)->find(); 
        $appid = $wechat_4['key'];
        $secret = $wechat_4['secret'];
        $token = $this->getAuthorizationCode($appid,$secret,true); 

        $info = Db::name('mc_mapping_fans')->field('uid,uniacid,unionid')->where($where)->find();
        $platform = Db::name('mc_members')->field('platform')->where('uid',$info['uid'])->find();
        if($info['uniacid'] == 4){ //openid 换过的 就是患教的 
            return $token;
        }else{
            if($platform['platform'] == 1 || $platform['platform'] == 3){  
                return $token;
            }
        }
        return '';
    }

    //www.yuntehu.com/wapi/public/index.php/index/message/add_message?type=5&url=www.yuntehu.com/wb&openid=ok0umxG27hieIhPhsm8DaHSStUbU&vid=8&key1=栏目名称&key2=栏目类别&key3=栏目讲师&key4=2017.11.11
    /**
     * [add_message description]
     * @param type 1订阅栏目，2购买栏目，3关注医院，4关注医生，5更新栏目
     * @param url  
     * @param openid  
     * @param string $key1 [description]
     * @param string $key2 [description]
     */
    public function add_message($arr=[]){
        //检测参数
        // $cc = $this->decrypt();  
        // if($cc['code'] != 1){ return json($cc);die;}  
        $this->log_message($arr,'message.txt');
        
        if(empty($arr['type'])){ $arr['type'] = ''; }
        if(empty($arr['url'])){ $arr['url'] = ''; }
        if(empty($arr['openid'])){ $arr['openid'] = ''; }
        if(empty($arr['key1'])){ $arr['key1'] = ''; }
        if(empty($arr['key2'])){ $arr['key2'] = ''; }
        if(empty($arr['key3'])){ $arr['key3'] = ''; }
        if(empty($arr['key4'])){ $arr['key4'] = ''; }
        if(empty($arr['vid'])){ $arr['vid'] = ''; }

        $type = input('param.type')?input('param.type'):$arr['type'];
        $url = input('param.url')?input('param.url'):$arr['url'];
        $openid = input('param.openid')?input('param.openid'):$arr['openid'];
        $key1 = input('param.key1')?input('param.key1'):$arr['key1'];
        $key2 = input('param.key2')?input('param.key2'):$arr['key2'];
        $key3 = input('param.key3')?input('param.key3'):$arr['key3'];
        $key4 = input('param.key4')?input('param.key4'):$arr['key4'];
        $vid = input('param.vid')?input('param.vid'):$arr['vid']; 
        
        //检测是否是患者教育中心用户  platform = 1医院用户   platform = 0平台医脉达   platform = 3平台患教
        $op = $this->get_openid_4(array('openid'=>$openid));
        if(!empty($op)){
            $openid = $op;
        }

        $arr = array();
        $data = array();
        switch ($type) {
            case 1: //订阅栏目 
                $arr['openid'] = $openid;
                $arr['url'] = $url;
                $arr['template_id'] = $this->template_column;
                $arr['data'] = array(
                    "first"=>array(
                        "value"=>"您好，您订阅栏目成功！",
                        "color"=>"#173177"
                    ),
                    "keyword1"=>array(
                        "value"=>$key1,
                        "color"=>"#173177"
                    ),
                    "keyword2"=>array(
                        "value"=>$key2,
                        "color"=>"#173177"
                    ),
                    "keyword3"=>array(
                        "value"=>$key3,
                        "color"=>"#173177"
                    ),  
                    "remark"=>array(
                        "value"=>"点击详情，进入栏目观看。",
                        "color"=>"#173177"
                     )
                );
                break;
            case 2: //购买栏目
                $arr['openid'] = $openid;
                $arr['url'] = $url;
                $arr['template_id'] = $this->template_buy_column;
                $arr['data'] = array(
                    "first"=>array(
                        "value"=>"亲爱的学员，您已成功购买课程。",
                        "color"=>"#173177"
                    ),
                    "keyword1"=>array(
                        "value"=>$key1,
                        "color"=>"#173177"
                    ),
                    "keyword2"=>array(
                        "value"=>$key2,
                        "color"=>"#173177"
                    ),
                    "keyword3"=>array(
                        "value"=>$key3,
                        "color"=>"#173177"
                    ),  
                    "remark"=>array(
                        "value"=>"欢迎收看疾病课堂，更多精彩内容营养专家告诉您!",
                        "color"=>"#173177"
                     )
                );
                break;
            case 3: //关注医院
                $arr['openid'] = $openid;
                $arr['url'] = $url;
                $arr['template_id'] = $this->template_hospital;
                $arr['data'] = array(
                    "first"=>array(
                        "value"=>"亲爱的学员，学习时间到了，继续按计划循序渐进地掌握控糖知识吧！",
                        "color"=>"#173177"
                    ),
                    "keyword1"=>array(
                        "value"=>$key1,
                        "color"=>"#173177"
                    ),
                    "keyword2"=>array(
                        "value"=>$key2,
                        "color"=>"#173177"
                    ),
                    "keyword3"=>array(
                        "value"=>$key3,
                        "color"=>"#173177"
                    ),  
                    "remark"=>array(
                        "value"=>"欢迎收看疾病课堂，更多精彩内容营养专家告诉您!",
                        "color"=>"#173177"
                    )
                );
                break;
            case 4: //关注医生
                $arr['openid'] = $openid;
                $arr['url'] = $url;
                $arr['template_id'] = $this->template_doctor;
                $arr['data'] = array(
                    "first"=>array(
                        "value"=>"亲爱的学员，学习时间到了，继续按计划循序渐进地掌握控糖知识吧！",
                        "color"=>"#173177"
                    ),
                    "keyword1"=>array(
                        "value"=>$key1,
                        "color"=>"#173177"
                    ),
                    "keyword2"=>array(
                        "value"=>$key2,
                        "color"=>"#173177"
                    ),
                    "keyword3"=>array(
                        "value"=>$key3,
                        "color"=>"#173177"
                    ),  
                    "remark"=>array(
                        "value"=>"欢迎收看疾病课堂，更多精彩内容营养专家告诉您!",
                        "color"=>"#173177"
                    )
                );
                break; 
            case 5: //视频上新 
                $arr['vid'] = $vid; //视频id
                $arr['url'] = $url;
                $arr['template_id'] = $this->template_lesson_up;
                $arr['data'] = array(
                    "first"=>array(
                        "value"=>"亲爱的学员，您订阅的课程更新了",
                        "color"=>"#173177"
                    ),
                    "keyword1"=>array(
                        "value"=>$key1,
                        "color"=>"#173177"
                    ),
                    "keyword2"=>array(
                        "value"=>$key2,
                        "color"=>"#173177"
                    ),
                    "keyword3"=>array(
                        "value"=>$key3,
                        "color"=>"#173177"
                    ),  
                    "keyword4"=>array(
                        "value"=>$key4,
                        "color"=>"#173177"
                    ), 
                    "remark"=>array(
                        "value"=>"你订阅的专栏发布了新课程，请赶快学习吧！",
                        "color"=>"#173177"
                    )
                );
                break; 
        }

        $data['content'] = json_encode($arr);
        $data['type'] = $type;
        $data['addtime'] = date("Y-m-d H:i:s",time());
        $res = Db::name('y_med_send_message')->insert($data);
        if($res){
            $this->send_message();
        }
        
    }
 
	//主动发送消息
    public function send_message($istype=1){    
        //检测参数
        // $cc = $this->decrypt();  
        // if($cc['code'] != 1){ return json($cc);die;}  

        //获取待发送消息
        $message = $this->get_messages();

        if(empty($message)){
            return json(array('errcode'=>40000,'没有待发送消息！'));
        }
        foreach($message as $mess){  
            $con = json_decode($mess['content'],true);
            switch ($mess['type']) {
                case 1:  
                case 2:  
                    $arr = array(
                        "touser"=>$con['openid'],
                        "template_id"=>$con['template_id'],
                        "url"=>$con['url'],
                        "topcolor"=>"#FF0000",
                        "data"=>$con['data'],
                    ); 
                    $this->_send(json_encode($arr),array('openid'=>$con['openid']));
                    break;
                case 5:
                    //获取待发送消息的用户列表
                    $this->log_message($con);
                    $users = $this->get_users($con['vid']);  
                    if(!empty($users)){
                        foreach($users as $u){  
                            $arr = array(
                                "touser"=>$u['openid'],
                                "template_id"=>$con['template_id'],
                                "url"=>$con['url'],
                                "topcolor"=>"#FF0000",
                                "data"=>$con['data'],
                            ); 
                            $this->_send(json_encode($arr),array('uid'=>$u['uid']));  
                        } 
                    } 
                    break; 
            }
        }
         
    }  

    /*
        获取用户access_token
    */
    private function getAuthorizationCode($appid,$secret,$ist=false){ 
        $token = Cache::get('access_token');
        if(!$token || $ist==true){
            $oauth2_code = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
            // $content = file_get_contents($oauth2_code);
            $content = $this->makeRequest($oauth2_code); 
            $token = @json_decode($content['result'], true); 
            if (empty($token) || !is_array($token) || empty($token['access_token'])) {
                echo '<h1>获取微信公众号授权失败[无法取得token以及openid], 请稍后重试！ <br /><h1>';
                die;
            }
            if($ist == true){
                Cache::set('access_token','');
            }else{
                Cache::set('access_token',$token['access_token'],$token['expires_in']);
            }
            
            return $token['access_token'];
        }
        return $token;
    }

    private function _send($json,$info=array(),$ist=false){
        
        /**
         * 检测是否是医院用户   医院用户推送用患者教育公众号推送 
         */
        $platform = $this->is_platform($info);
        if(!empty($platform)){
            $token = $platform;
        }else{
            $token = $this->getAuthorizationCode($this->lesson_setting['account']['key'],$this->lesson_setting['account']['secret']);
        } 
        $send_url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$token;
        $result = $this->makeRequest($send_url,$json); 
        $res = json_decode($result['result'],true);
        $this->log_message($res);

        //发送失败写入数据库
        if($res['errcode'] == 0){  
            $this->insert_log($uid,$json,$res['errmsg']);
        }elseif($res['errcode'] == 40001){
            Cache::set('access_token','');
            $this->insert_log($uid,$json,$res['errmsg'],0); 
        }else{
            Cache::set('access_token','');
            $this->insert_log($uid,$json,$res['errmsg'],0); 
        } 
    }

    private function curl($url,$data_string){ 
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url); 

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($curl, CURLOPT_POSTFIELDS,$data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        
        return curl_exec($curl);
    }

    private function get_messages(){
        $where['status'] = 0;
        $result = Db::name('y_med_send_message')->where($where)->order('addtime desc')->select(); 
        if(!empty($result)){
            foreach($result as $re){
                $w['id'] = $re['id'];
                $where['status'] = 1;
                Db::name('y_med_send_message')->where($w)->update($where); 
            }
        } 
        return $result;
    }
    
    public function get_users($vid = ''){
        $vid = $vid?$vid:input('vid');
        $this->log_message('vid');
        $this->log_message($vid);
        //查询相关栏目 
        // $where = array("vid"=>$vid,"type" =>1); 
        $column_ids = Db::query("select colid from ims_y_med_relation where vid=$vid and type=1");//->field('colid')->where($where)->select(); 
        $this->log_message('column');
        $this->log_message($column_ids);
        // var_dump($column_ids);die;
        $str_col_ids = '';
        if(empty($column_ids)){
            return '';die; 
        } 
        foreach($column_ids as $col){
            $col_ids[] = $col['colid'];
        }

        $str_col_ids = implode(',', $col_ids); 
        $where['c.colid'] = ['in',$str_col_ids]; 
        
        $result = Db::name('y_med_collect c')->distinct(true)->field('c.uid,m.openid')->join("mc_mapping_fans m","m.uid=c.uid","left")->where($where)->select(); 
        $this->log_message('users');
        $this->log_message($result); 
        // var_dump($result);die;
        return $result;
    }

    private function insert_log($uid,$json,$err,$status=1){
        $data = array();  
        $data['uid'] = $uid;
        $data['content'] = $json;
        $data['sendtime'] = date("Y-m-d H:i:s",time());
        $data['err'] = $err; 
        $data['status'] = $status; 
        Db::name('y_med_log_message')->insert($data);
    }
     
}