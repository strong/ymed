<?php
namespace app\common\controller;
use think\Controller;
use think\Request;
use think\Db;
use \think\Cache;
use think\Session;
class All extends Controller
{
    public function _initialize(){
        header("Content-Type:text/html; charset=utf-8");

        $this->appkey = 'yimaida'; 

    } 


    /**
     * url检测操作
     * @return [type] [description]
     */
    public function decrypt(){  
    	// return array(
    	// 	'code' => 1,
    	// 	'msg' => "成功"
    	// );   
        $arr = input('get.'); 
        $code = '';
    	if(empty($arr['_sign'])){
    		return array(
	    		'code' => 0,
	    		'msg' => "请求超时！不是自己人吧"
	    	);  
    	} 

    	$code = $arr['_sign']; 


    	//检测失效时间
    	if($arr['_timestamp'] < (time()-20)){
    		return array(
	    		'code' => 0,
	    		'msg' => "请求超时！"
	    	);  
    	}

    	unset($arr['_sign']);
        unset($arr['session3rd']);
        ksort($arr);
    	$_sign = $this->get_sign($arr);
        //&_timestamp=1528961837&tid=10&time=1528961872&uid=147049&uniacid=8
        //                       tid=10&uniacid=8&uid=147049&time=1528961872
    	$dcode = $this->get_md5($_sign);  //dump($dcode);
    	if($dcode != $code){
			return array(
	    		'code' => 0,
	    		'msg' => '请求超时！不是自己人啊！'
	    	); 
	    	 
    	} 

		return array(
    		'code' => 1,
    		'msg' => "成功"
    	);  
    }
 
    //加密操作
    public function encryption($arr){
        
        $_timestamp = time();
        $arr['_timestamp'] = $_timestamp;  
        ksort($arr);
        $_sign = $this->get_sign($arr);
        return $_sign.'&_sign='.$this->get_md5($_sign);
    }

    private function get_sign($arr=array()){ 
        $sb = '';
        if(!empty($arr)){
            foreach($arr as $k => $v){
                if(isset($v)){
                    $sb .= '&'.$k.'='.$v;
                } 
            } 
        }
        return $sb;
    }

    private function get_md5($_sign = ''){
        if(!empty($_sign)){
            // print_r($this->appkey.$_sign);
            //$this->log_message($this->appkey.$_sign,'small.txt');

            $_sign = md5($this->appkey.$_sign);
        }
        return strtoupper($_sign);
    }


    /**
	 * 发起http请求
	 * @param string $url 访问路径
	 * @param array $params 参数，该数组多于1个，表示为POST
	 * @param int $expire 请求超时时间
	 * @param array $extend 请求伪造包头参数
	 * @param string $hostIp HOST的地址
	 * @return array    返回的为一个请求状态，一个内容
	 */
	public function makeRequest($url, $params = array(), $expire = 0, $extend = array(), $hostIp = '')
	{
	    if (empty($url)) {
	        return array('code' => '100');
	    }

	    $_curl = curl_init();
	    $_header = array(
	        'Accept-Language: zh-CN',
	        'Connection: Keep-Alive',
	        'Cache-Control: no-cache',
            'Content-Type: application/json'
	    );
	    // 方便直接访问要设置host的地址
	    if (!empty($hostIp)) {
	        $urlInfo = parse_url($url);
	        if (empty($urlInfo['host'])) {
	            $urlInfo['host'] = substr(DOMAIN, 7, -1);
	            $url = "http://{$hostIp}{$url}";
	        } else {
	            $url = str_replace($urlInfo['host'], $hostIp, $url);
	        }
	        $_header[] = "Host: {$urlInfo['host']}";
	    }

	    // 只要第二个参数传了值之后，就是POST的
	    if (!empty($params)) {
            // curl_setopt($_curl, CURLOPT_POSTFIELDS, http_build_query($params));
	        curl_setopt($_curl, CURLOPT_POSTFIELDS, $params);
	        curl_setopt($_curl, CURLOPT_POST, true);
	    }

	    if (substr($url, 0, 8) == 'https://') {
	        curl_setopt($_curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	        curl_setopt($_curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	    }
	    curl_setopt($_curl, CURLOPT_URL, $url);
	    curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($_curl, CURLOPT_USERAGENT, 'API PHP CURL');
	    curl_setopt($_curl, CURLOPT_HTTPHEADER, $_header);

	    if ($expire > 0) {
	        curl_setopt($_curl, CURLOPT_TIMEOUT, $expire); // 处理超时时间
	        curl_setopt($_curl, CURLOPT_CONNECTTIMEOUT, $expire); // 建立连接超时时间
	    }

	    // 额外的配置
	    if (!empty($extend)) {
	        curl_setopt_array($_curl, $extend);
	    }

	    $result['result'] = curl_exec($_curl);
	    $result['code'] = curl_getinfo($_curl, CURLINFO_HTTP_CODE);
	    $result['info'] = curl_getinfo($_curl);
	    if ($result['result'] === false) {
	        $result['result'] = curl_error($_curl);
	        $result['code'] = -curl_errno($_curl);
	    }

	    curl_close($_curl);
	    return $result;
	}

	/**
	 * 返回信息
	 * @param $message
	 * @return array
	 */
	function ret_message($message = "") {
	    if ($message == "") return ['code'=>0, 'msg'=>''];
	    $ret = lang($message);

	    if (count($ret) != 2) {
	        return ['code'=>-1,'msg'=>'未知错误'];
	    }
	    return array(
	        'code'  => $ret[0],
	        'msg' => $ret[1]
	    );
	}

	/**
	 * 读取/dev/urandom获取随机数
	 * @param $len
	 * @return mixed|string
	 */
	function randomFromDev($len) {
	    $fp = @fopen('/dev/urandom','rb');
	    $result = '';
	    if ($fp !== FALSE) {
	        $result .= @fread($fp, $len);
	        @fclose($fp);
	    }
	    else
	    {
	        trigger_error('Can not open /dev/urandom.');
	    }
	    // convert from binary to string
	    $result = base64_encode($result);
	    // remove none url chars
	    $result = strtr($result, '+/', '-_');

	    return substr($result, 0, $len);
	}

	/*
        统一登录/微信
    */
    public function add_user($info = array()){
        if(!empty($info)){  
        	$nickName = $this->filterEmoji($info['nickName']);
        	$openid = $info['openId'];
        	$fans = $this->mc_mapping_fans(array('openid'=>$openid,'status'=>1));
            $userinfo = array(
       			'openid' => $openid,
       			'nickname' => $nickName ? $nickName : '匿名用户'.rand(1,9999),
       			'gender' => $info['gender'],
       			'nationality' => $info['country'],
       			'resideprovince' => $info['city'],
       			'residecity' => $info['province'],
       			'headerimg' => $info['avatarUrl'],
       			'addtime' => date("Y-m-d H:i:s",time()),
       			'status' => 1,
       		); 
           	if((!empty($openid)) && empty($fans)){   
           		Db::name('small_members')->insert($userinfo);
           		$fans = $this->mc_mapping_fans(array('openid'=>$openid,'status'=>1));
           	} 
           	Cache::set($info['session3rd'],$fans);  
           	return $fans['uid'];
        }
    }  

    /*
        获取用户信息
    */
    public function mc_mapping_fans($where=array()){
        if($where){ 
            return Db::name('small_members')->where($where)->find();
        }
    }



    public function log_message($content='',$file='wapi.txt'){ 
        $dir = iconv("UTF-8", "GBK", "/tmp/log/");
        if (!file_exists($dir)){
            mkdir ($dir,0777,true); 
        } 
        file_put_contents($dir.$file, print_r($content,true).PHP_EOL, FILE_APPEND);
    }

    // 过滤掉emoji表情
    public function filterEmoji($str)
    {
        $str = preg_replace_callback(
           '/./u',
           function (array $match) {
            return strlen($match[0]) >= 4 ? '' : $match[0];
           },
           $str); 
        return $str;
    }

    // urlencode emoji表情
    public function urlencodeEmoji($str)
    {
        $str = preg_replace_callback(
           '/./u',
           function (array $match) {
            return strlen($match[0]) >= 4 ? urlencode($match[0]) : $match[0];
           },
           $str); 
        return $str;
    } 
	
}