<?php
namespace app\common\controller;
use think\controller;
use think\Db;
use think\Cache;
use think\Session;
use think\request;  
use app\common\controller\All;
class Base extends All
{
    public function _initialize(){
    	parent::_initialize();
        //获取url 
        $request = Request::instance();
        //获取所有值
        $d = input('param.');
        ksort($d);
        $this->cachekey = md5($request->url(true).serialize($d));

        //上传的域名
        $this->attachment = config('GW_URL');

        //获取微课堂配置信息
        $this->setting();
        $this->mu_uniacid = 8;
        $this->uniacid = input('param.uniacid') ? input('param.uniacid') : 8;
        $this->themeid = 2; 
        $this->small_members = 'small_members';
        $this->url = config('GW_URL');
        //设置标识
        $this->_appid = 'wxd44c56ded2770f7a';
        $this->_secret = '77339a3cf72be0a201289e473c27e9ec'; 
        $this->uid = input('param.uid') ? input('param.uid') : '';
        //用户信息
        $this->member = Cache::get(input('param.session3rd'));
        if($this->member){
        	$this->uid = $this->member['uid'];
	        $this->openid = $this->member['openid'];
	        $this->nickname = $this->member['nickname'];
        }
    }  

    /**
     * 获取视频信息
     * @param    $colid [栏目id]
     * @param    $type [视频类型  免费收费 变现]
     * @param    $cid [疾病]
     */
    protected function getVideo($arr = []){
        $id = $this->get_relation('vid',$arr);
        if (empty($id)) { return 0; }

        $where['id'] = ['in',$id];

        $arr['keyword'] = isset($arr['keyword']) ? $arr['keyword'] : input('param.keyword');
        if (!empty($arr['keyword'])) {
            $where['title'] = ['like','%'.trim($arr['keyword']).'%'];
        }
            
        $cid =  isset($arr['cid']) ? $arr['cid'] : input('param.cid');
        $hid =  isset($arr['hid']) ? $arr['hid'] : input('param.hid');
        $tid =  isset($arr['tid']) ? $arr['tid'] : input('param.tid');
        $type =  isset($arr['type']) ? $arr['type'] : input('param.type');
        $is_free =  isset($arr['is_free']) ? $arr['is_free'] : input('param.is_free'); 
        
        $where['uniacid'] = $this->uniacid;

        if (!empty($cid)) {
            if (count(explode(',',$cid))>1) {
                $cid = ['in',$cid];
            }
            
            $where['cid'] = $cid;
        }
        if (!empty($tid)) {
            if (count(explode(',',$tid))>1) {
                $tid = ['in',$tid];
            }
            // if (is_string($tid) && strpos($tid,',') > 0){
            //     $tid = ['in',$tid];
            // }
            $where['teacherid'] = $tid;
        }


        if (!empty($type)) {
            $where['type'] = $type;
        }
        if (!empty($is_free)) {
            $where['is_free'] = $is_free;
        }
        $arr['limit'] = isset($arr['limit']) ? $arr['limit'] : $this->pagetLimit();
        $arr['order'] = isset($arr['order']) ? $arr['order'] : 'displayorder desc,id asc';
        $where['status'] = '上架';
        
        // 用于签名的公钥和私钥
        $qiniu = unserialize($this->lesson_setting['qiniu']); 
        $accessKey = $qiniu['access_key']; 
        $secretKey = $qiniu['secret_key'];
        $list = Db::name('y_med_video')->where($where)->order($arr['order'])->limit($arr['limit'])->select();
        
        $uid = isset($arr['uid']) ? $arr['uid'] : input('param.uid');

        foreach($list as $k => $v) {
            // return $v;die;
            $list[$k]['videourl'] = $this->privateDownloadUrl($accessKey,$secretKey,$v['videourl']);
            $list[$k]['img_dr'] = $this->privateDownloadUrl($accessKey,$secretKey,$v['img']);
            $video_head = Db::name('y_med_video_head_foot')->find($v['video_head_id']);

            //print_r($video_head['url']);die;
            $video_foot = Db::name('y_med_video_head_foot')->find($v['video_foot_id']);

            $list[$k]['video_head'] = $this->privateDownloadUrl($accessKey,$secretKey,$video_head['url']);
            
            $list[$k]['video_foot'] = $this->privateDownloadUrl($accessKey,$secretKey,$video_foot['url']);
            // $list[$k]['video_head'] = $video_head;
            // $list[$k]['video_foot'] = $video_foot;
            // $list[$k]['video_head'] = $this->privateDownloadUrl($accessKey,$secretKey,$v['video_head']);
            // $list[$k]['video_foot'] = $this->privateDownloadUrl($accessKey,$secretKey,$v['video_foot']);
            //$list[$k]['timg'] = $this->privateDownloadUrl($accessKey,$secretKey,$v['timg']);
            
            //收藏点赞量
            $collection = Db::name('y_med_collect')->where(['ctype'=>'视频','vid'=>$v['id']])->count();
            $list[$k]['collection'] = $v['collection'] +$collection;
            //浏览量
            $browsenum = Db::name('y_med_history')->where(['vid'=>$v['id']])->count();
            
            //医生
            $t = Db::name('fy_lesson_teacher')->where(['id'=>$v['teacherid']])->find();
            $list[$k]['tname'] = $t['teacher'];
            $list[$k]['timg'] = $t['teacherphoto'];
            $list[$k]['tid'] = $t['id'];

            //医院
            $h = Db::name('fy_lesson_organization')->where(['id'=>$t['orgid']])->find();
            
            $list[$k]['hname'] = $h['name'];

            $list[$k]['browsenum'] = $v['browsenum'] +$browsenum;
            //评论量
            $list[$k]['evaluate'] = Db::name('y_med_evaluate')->where(['vid'=>$v['id']])->count();
            //是否点赞收藏
            if (empty($uid)) {
                $list[$k]['user']['is_collection'] = 0;
            }else{
                $is = Db::name('y_med_collect')->where(['ctype'=>'视频','vid'=>$v['id'],'uid'=>$uid])->count();
                $list[$k]['user']['is_collection'] = $is;
                $doctorcount = Db::name('y_med_collect')->where(['uid'=>$uid,'ctype'=>'讲师','tid'=> $list[$k]['tid']])->count();
                $list[$k]['user']['is_follow_doctor'] = $doctorcount; 
                $list[$k]['user']['is_look'] = Db::name('y_med_history')->where(['vid'=>$v['id'],'uid'=>$uid,'uniacid'=>$this->uniacid])->count();
            }
        }

        return $list;
    }
 
    /**
     *  [getcol 获取栏目列表]
     *  [cid,hid,tid]      [分类id,栏目id,医生id 支持单个或多个]
     */
    protected function getcol($arr = []){ 
        //获取当前医院下的栏目
        $colids = $this->get_relation('colid',$arr);
        if (empty($colids)) { return 0;die; }

        if (isset($colids)) {
            $id = $colids; 
        }

        if (!empty($id)) {
            if (count(explode(',',$id))>1) {
                $id = ['in',$id];
            }
            $where['id'] = $id;
        }

        $arr['keyword'] = isset($arr['keyword']) ? $arr['keyword'] : input('param.keyword');
        if (!empty($arr['keyword'])) {
            $where['title'] = ['like','%'.trim($arr['keyword']).'%'];
        }
        //is_money [2免费  1收费] 
        $where['uniacid'] = $this->uniacid;

        $arr['is_money'] = isset($arr['is_money']) ? $arr['is_money'] : input('param.is_money');
        if ($arr['is_money'] == 2) {
            $where['price'] = ['=',0];
        }elseif($arr['is_money']==1){
            $where['price'] = ['>',0];
        }
        $where['status'] ='启用';

        $arr['order'] = isset($arr['order']) ? $arr['order'].' desc,id desc' : 'id desc';
        $arr['limit'] = isset($arr['limit']) ? $arr['limit'] : $this->pagetLimit();
        // $this->log_message($where);
        $cols = Db::name('y_med_column')->where($where)->order($arr['order'])->limit($arr['limit'])->select();

        $uid = isset($arr['uid']) ? $arr['uid'] : input('param.uid');

        foreach($cols as $k => $v) {
            //$this->log_message($v);
            //$vids = $this->get_relation('vid',['colid'=>$v['id']]);
            $col_vids = Db::name('y_med_relation')->field('vid')->where(['colid'=>$v['id'],'type'=>1])->select();
             
            $cols[$k]['v_num'] = 0;
            if (!empty($col_vids)) {
                $cols[$k]['v_num'] = count($col_vids);
            }

            if($v['comid']){
                $cols[$k]['group'] = Db::name('y_med_group')->find($v['comid']);
            }else{
                $cols[$k]['group'] = '';
            } 
           

            if (empty($uid)) {
                $cols[$k]['user']['iscollect'] = 0;
            }else{
                //是否订阅 
                $cols[$k]['user']['iscollect'] = Db::name('y_med_collect')->where(['ctype'=>'栏目','colid'=>$v['id'],'uid'=>$uid])->count();
                $cols[$k]['user']['isbuy'] =0;
                if ($v['price'] > 0) {
                    $cols[$k]['buynum'] = Db::name('y_med_column_order')->where(['colid'=>$v['id']])->count();
                    // 是否购买
                    $cols[$k]['user']['isbuy'] = Db::name('y_med_column_order')->where(['uid'=>$uid,'colid'=>$v['id']])->count();
                }

            }
        }
        return $cols; 
    }


     /**
     * [get_relation        获取栏目视频id集]
     * string   $field      查询类型  栏目/视频
     * param.colid          栏目id
     * param.vid            视频id
     * param.cid            疾病id
     * param.hid            医院id
     * @return              string 1,2,3,4
     */
    protected function get_relation($field='colid',$arr=[])
    {
        $arr['colid']   = isset($arr['colid']) ? $arr['colid'] : input('param.colid');
        $arr['hid']     = isset($arr['hid']) ? $arr['hid'] : input('param.hid');
        $rwhere['hid']  = isset($arr['hid'])  ? $arr['hid'] : 0 ;
        if(!empty($arr['hid_all'])){
            unset($rwhere['hid']);
        } 
        $rwhere['type'] =4;
        $this->log_message($rwhere);
        //筛选符合当前医院的栏目
        if (!empty($arr['colid'])) {
            $rwhere['colid'] = ['in',$arr['colid']];
        }
        
        $res = Db::name('y_med_relation')->where($rwhere)->select();
        if (empty($res)) { return $res; die; }
        
        // 拼接栏目id
        $colids = '';

        foreach ($res as $k => $v) {
            $colids .= ','.$v['colid'];
        }

        $colids = trim($colids,',');
        
        if (isset($arr['colid']) && $field != 'vid') { return $colids; die; }

        $vid = isset($arr['vid']) ? $arr['vid'] : input('param.vid');

        // 根据视频查询
        if (!empty($vid) || $field == 'vid') {
            
            $vwhere = ['type'=>1,'colid'=>['in',$colids]];
            if (!empty($vid)) {
                $vwhere['vid'] = ['in',$vid];
            }
           
            $datas = Db::name('y_med_relation')->field('colid,vid')->where($vwhere)->select();
            if (empty($datas)) { return 0;die; }
            
            //根据参数 拼接id并返回
            $ids = [];
            foreach ($datas as $k => $v) {
                $ids[] = $v[$field];
            }
            $ids = implode(',',array_unique($ids));
            
            return $ids;die;
        }

        $cid = isset($arr['cid']) ? $arr['cid'] : input('param.cid');
        
           
        //二次筛选 获取栏目分类
        if (!empty($cid)) {

            if (count(explode(',',$cid))>1) {
                $cids = ['in',$cid];
            }

            $where['cid'] = isset($cids) ? $cids : $cid;

        }
        // 二次筛选
        if (!empty($where)) {
            $where['colid'] = $colids;
            $where['type'] = 2;
            if (count(explode(',',$colids))>1) {
                $where['colid'] = ['in',$colids];
            }

            $res = Db::name('y_med_relation')->where($where)->select();

            if (empty($res)) {  return 0;die;  }

            $colids = [];
            foreach ($res as $k => $v) {
                $colids[] = $v['colid'];
            }
            $colids = implode(',',array_unique($colids));
        }

        return $colids;
    }

    protected function col_vids($colid){
        $col = Db::name('y_med_relation')->field('vid')->where(['colid'=>$colid,'type'=>1])->select();
        if (!empty($col)) {
            $ids ='';
            foreach ($col as $k => $v) {
                $ids .= $v['vid'].',';
            }
            $col = trim($ids,',');
        }
        return $col;
    }

    //获取医生资料
    protected function get_doctor($arr){ 
        $field = ['id','zhiwu','company','teacherphoto','teacher','lcid','pgoodat'];
        //医师信息
        $doctor = Db::name('fy_lesson_teacher')->field($field)->where('id','in',$arr['tid'])->select();
        if(empty($doctor)){
            return null;
        }
        $vids = '';
        foreach($doctor as $k=>$v){
            $doctor['videolist'][$k] = $this->getVideo(['tid'=>$v['id']]);
            if($doctor['videolist'][$k]){
                foreach($doctor['videolist'][$k] as $ke=>$ve){
                    $vids .=','.$ve['id'];

                }
            }
            //查询用户是否关注过该医生
            if(isset($arr['uid'])){
                $res = Db::name('y_med_collect')->where(['uid'=>$arr['uid'],'ctype'=>'讲师','tid'=>$v['id']])->count();
                $doctor[$k]['is_collection'] = $res;
            }
            $doctor['collist'][$k] = $this->getcol(['vid'=>trim($vids,',')]);
        }
        return $doctor;
    }

    /**
     * 搜索结果
     */
    public function searchResult(){
        $keywords = input('param.keyword'); 
        if (empty($keywords)) { 
            return array('code'=>0,'msg'=>'请输入关键字');
        }
      	$type = 0;
        $table = ['y_med_videos','fy_lesson_teacher'];
        $res = $this->getSearchIds($keywords,$table[$type],$type);
        
        if(!empty($res['list'])){ 
            return array('code'=>1,'msg'=>$res);
        }else{
            return array('code'=>0,'msg'=>'没有更多数据！');
        } 
    }
    public function getSearchIds($keywords,$table,$type = 0){
        $res = array();
        $list = '';
        $data = $this->sphinxFC($keywords,$table);

        if (!empty($data['matches'])) {
            $ids = '';
            foreach ($data['matches'] as $k => $v) {
                $ids[] = $k;
            }
            $arr = ['vid'=>$ids]; 
            if ($type == 0) {
                $list = $this->getVideo($arr);
            } 
        } 
        $res['total_found'] = isset($data['total_found']) ? $data['total_found'] : 0;
        $res['list'] = $list;
        return $res;
    }

    /**
     * @param  [type]  $keyword [关键词]
     * @param  string  $divide  [截取位置]
     * @param  integer $val     [截取大小]
     * right 截取尾部
     * left 截取头部
     */
    protected function getguanjianci($keyword,$divide='',$val=1){
        if ($divide == 'right') {
            $guanjianci = mb_substr($keyword,0,mb_strlen($keyword)-$val,'utf-8');
        }elseif($divide == 'left'){
            $guanjianci = mb_substr($keyword,$val,mb_strlen($keyword),'utf-8');
        }
        $where = [
            'pinyin'=> [ 'like','%'.pinyin($guanjianci).'%'],
            'guanjianci'=> [ 'like','%'.$guanjianci.'%'],
        ];
        $data = Db::name('fy_lesson_guanjianci')->where($where)->select();
        return $data;
    }

    /*
        分词查询
        $kw 查询的词
        $k  索引 
                fy_lesson_son 视频
                fy_lesson_teacher 医生

        return 
                matches
                    [这里是id,唯一标识] 
    */
    protected function sphinxFC($kw,$k){
        $limit = $this->pagetLimit();
         
        $l = explode(',',$limit);
        if($l[0]>0){
            $t = $l[0] * $l[1];
        }else{
            $t =  $l[1];
        } 
        $sphinx = new \SphinxClient();
        
        $sphinx -> SetServer("10.141.183.138", 9312);

        if($k == 'fy_lesson_teacher'){
            $sphinx -> SetSortMode(4,'weights DESC');
        }  
        $sphinx -> SetLimits($l[0],$l[1],$t);

        $result = $sphinx -> Query($kw,$k); 
        echo "<pre>";
        var_dump($result);die;
        if(!empty($result['matches'])){
            $arr['matches'] = $result['matches'];
            $arr['total'] = $result['total'];
            $arr['total_found'] = $result['total_found'];

            return $arr;
        }else{
            return;
        }
    }

    //分页计算
    public function pagetLimit(){
        $p = intval(input('param.p'))?intval(input('param.p')):1;
        $ps = intval(input('param.ps'))?intval(input('param.ps')):10;
        $p -= 1;
        $ps = $ps;
        return $p*$ps.','.$ps;
    }

    // 获取公众号信息
    private function setting(){
        //基本设置fy
        $this->lesson_setting = $this->settingGet(8);
 
    }

    protected function settingGet($uniacid){
        //基本设置fy
        $lesson_setting = $this->setting_1($uniacid); 
        //微信公众号信息
        $lesson_setting['account'] = $this->setting_2($uniacid);
        return $lesson_setting;
    }
    protected function setting_1($uniacid){
        return Db::name('fy_lesson_setting')->where('uniacid',$uniacid)->find();
    }  
    protected function setting_2($uniacid){
        return Db::name('account_wechats')->where('uniacid',$uniacid)->find();
    }
    /*
        七牛视频播放组合
    */
    protected function privateDownloadUrl($accessKey, $secretKey, $baseUrl, $expires = 3600)
    {

        $deadline = time() + $expires;
        $pos = strpos($baseUrl, '?');
        if ($pos !== false) {
            $baseUrl .= '&e=';
        } else {
            $baseUrl .= '?e=';
        }
        $baseUrl .= $deadline;
        $hmac = hash_hmac('sha1', $baseUrl, $secretKey, true);
        $find = array('+', '/');
        $replace = array('-', '_');
        $hmac = str_replace($find, $replace, base64_encode($hmac));
        $token = $accessKey . ':' . $hmac;
        return "{$baseUrl}&token={$token}";
    }   

    public function checkkey(){
    	$sql = Db::name('small_qa_question')->getLastSql();
    	$data = Db::query('EXPLAIN '.$sql); 
    	$message = "checkkey:{table:".$data[0]['table'].",key:".$data[0]['possible_keys']?$data[0]['possible_keys']:'null'."}";
       	$this->log_message($message);
    }

    /*
        获取验证码
        mobile  手机号
        type    类型 
                    register    注册
                    find        找回
                    bound       绑定

        返回状态码 
                未知类型  10002
                手机号不存在 10003
                请正确输入手机号 10004
                验证码已发送！   20000
                已经绑定了手机号 20001
                服务器错误！     40000


    */
    protected function yzmGet($mobile,$type){
        $is_mobile = yz_mobile($mobile);
        if(!empty($is_mobile)){
            //判断类型
            $data = array();
            $data['type'] = $type;
            $data['mobile'] = $mobile;
            $data['addtime'] = time();
            $data['code'] = rand(111111,999999);
            $data['ip'] =  Request::instance()->ip();
            $data['status'] = 1;
           if($type == 'small'){
                //查询该用户是否绑定
                // $user = Db::name('small_members')->field('mobile')->where('mobile',$mobile)->find();
                 
                // if(!empty($user)){
                //     return '20001';
                // }
                $tmp = 3110014;
            }else{
                return '10002';
            }
            //查询是否存在
            $yzmCode = Db::name('mc_yzm_code')->field('id,addtime,status')->where(['mobile'=>$mobile,'type'=>$type])->find();
            if(empty($yzmCode)){
                //验证码写入数据库
                Db::name('mc_yzm_code')->insert($data);
            }else{
                if($yzmCode['addtime'] > (time()-(10*60)) ){
                    //验证码超时处理
                    if($yzmCode['status'] == 0){
                        //修改数据库验证码
                        Db::name('mc_yzm_code')->where(['mobile'=>$mobile,'type'=>$type])->update($data);
                    }else{
                        return '20000';
                    }

                }else{
                    //修改数据库验证码
                    Db::name('mc_yzm_code')->where(['mobile'=>$mobile,'type'=>$type])->update($data);
                }
            }
            //发送验证码
            $Tduanxin = new \org\Tduanxin();
            $c = $Tduanxin->sedSMSYzm($tmp,$mobile,$data['code']);
            if($c['code'] == 200){
                return '20000';
            }else{
                return '40000';
            }
        }else{
            return '10004';
        }
    } 

    // 查询验证码是不是存在
    protected function yzmyanzheng($mobile,$code,$type){
        $code = Db::name('mc_yzm_code')->field('id,mobile')->where(['mobile'=>$mobile,'code'=>$code,'type'=>$type,'status'=>1,'addtime'=>['>=',time()-(10*60)]])->find();
        if(empty($code)){
            return false;
        }else{
            Db::name('mc_yzm_code')->where(['id'=>$code['id']])->update(['status'=>0]);
            return true;
        }

    }


}