<?php
namespace app\app\lib; 

class IMServer {

	//开发者平台分配的appkey
	private $AppKey    = '6e91969fea69df12f726b410a714f324';  
	//AppSecret,         
    private $AppSecret = '3ebff5faf571';     
    //随机数（最大长度128个字符）
    private $Nonce;     
    //当前UTC时间戳，        
    private $CurTime;  
    //CheckSum 请求之前校验       
    private $CheckSum;       

	const HEX_DIGITS =  '0123456789abcdef';

	public function __construct(){ 

    } 

    /*
     *消息抄送验证数据是否在传输过程中被篡改
     *，CheckSum = sha1(AppSecret + MD5 + CurTime)，
     */
    public function getMessageCheckSum($md5, $curTime){
        return sha1($this->AppSecret.$md5.$curTime);
    }

     /**
     * API checksum校验生成  SHA1(AppSecret + Nonce + CurTime) ，三个参数拼接的字符串，进行SHA1哈希计算，转化成16进制字
     * @param  void
     * @return $CheckSum(对象私有属性)
     */
    public function CheckSumBuilder(){
    	//生成随机数
    	$hex_digits = self::HEX_DIGITS;
        $this->Nonce='';
        $join_string = '';

        for($i=0;$i<128;$i++){			//随机字符串最大128个字符，也可以小于该数
            $this->Nonce.= $hex_digits[rand(0,15)];
        }

        $this->CurTime = (string)(time());	//当前时间戳，以秒为单位

        $join_string = $this->AppSecret.$this->Nonce.$this->CurTime;
        $this->CheckSum = sha1($join_string);

        //echo $this->CheckSum;
    }

    
    /**
     * 使用CURL方式发送post请求
     * @param  $url     [请求地址]
     * @param  $data    [array格式数据]
     * @return $请求返回结果(array)
     */
    public function postDataCurl($url,$post_data){
        $this->checkSumBuilder();       //发送请求前需先生成checkSum

        $timeout = 5000;
        $http_header = array(
            'AppKey:'.$this->AppKey,
            'Nonce:'.$this->Nonce,
            'CurTime:'.$this->CurTime,
            'CheckSum:'.$this->CheckSum,
            'Content-Type:application/x-www-form-urlencoded;charset=utf-8'
        );
        
        $postdataArray = array();
        foreach ($post_data as $key=>$value){
            array_push($postdataArray, $key.'='.urlencode($value));
        }
        $postdata = join('&', $postdataArray);

        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_POST, 1);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt ($ch, CURLOPT_HEADER, false );
        curl_setopt ($ch, CURLOPT_HTTPHEADER,$http_header);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER,false); //处理http证书问题
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        if (false === $result) {
            $result =  curl_errno($ch);
        }
        curl_close($ch);
        return $this->json_to_array($result) ;
    }

    /**
     * 将json字符串转化成php数组
     * @param  $json_str
     * @return $json_arr
     */
    public function json_to_array($json_str){
    	if(is_array($json_str) || is_object($json_str)){
            $json_str = $json_str;
        }else if(is_null(json_decode($json_str))){
            $json_str = $json_str;
        }else{
            $json_str =  strval($json_str);
            $json_str = json_decode($json_str,true);
        }
        $json_arr=array();
        foreach($json_str as $k=>$w){
            if(is_object($w)){
                $json_arr[$k]= $this->json_to_array($w); //判断类型是不是object
            }else if(is_array($w)){
                $json_arr[$k]= $this->json_to_array($w);
            }else{
                $json_arr[$k]= $w;
            }
        }
        return $json_arr;
    }

    /**
     * 创建群
     * @param  $tname        [群名称，最大长度64字节
     * @return $result       [返回array数组对象]
     */
    public function createGroup($param){
    
        if (empty($param)){
            return json_encode(['code'=>0, 'msg'=>'参数为空']);
        }
        $url = 'https://api.netease.im/nimserver/team/create.action';
        $data= $param;

        $result = $this->postDataCurl($url,$data);
       
        return $result;
    }

    /**
     * 查询指定群的详细信息（群信息+成员详细信息）
     * @param  $tid        [群tid列表，如[\"3083\",\"3084"]]
     * @return $result     [返回array数组对象]
     */
    public function getGroupInfo($params){
        $tid = $params['tid'];
        if (empty($tid)){
            returnjson_encode(array('code'=>414, 'desc'=>'群ID为空'));
        }

        $url = 'https://api.netease.im/nimserver/team/queryDetail.action';
        $data= array(
            'tid' => $tid,
        );

        $result = $this->postDataCurl($url,$data);
       
        return $result;
    }

    /**
     * 拉人入群
     * @param  $params['tid']         [云信服务器产生，群唯一标识，创建群时会返回，最大长度128字节]
     * @param  $params['owner']       [群主用户帐号，最大长度32字节]
     * @param  $params['members']     [["aaa","bbb"](JsonArray对应的accid，如果解析出错会报414)，长度最大1024字节]
     * @param  $params['magree']      [管理后台建群时，0不需要被邀请人同意加入群，1需要被邀请人同意才可以加入群。其它会返回414。]
     * @param  $params['joinmode']    [群建好后，sdk操作时，0不用验证，1需要验证,2不允许任何人加入。其它返回414]
     * @param  $params['custom']      [自定义高级群扩展属性，第三方可以跟据此属性自定义扩展自己的群属性。（建议为json）,最大长度1024字节.]
     * @return $result      [返回array数组对象]
     */
    public function addIntoGroup($params){

        $url     = 'https://api.netease.im/nimserver/team/add.action';

        $result = $this->postDataCurl($url,$params);
        
        return $result;
    }

    /**
    * 踢出群  
    * @param $params['tid']     [群id]
    * @param $params['owner']   [群主账号]
    * @param $params['members'] [被踢人IM账号集合(多人)]
    * @return $result      [返回array数组对象]
    */
    public function kick($params){

        if (empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(array('code'=>414, 'desc'=>'用户账号为空'));
        }
        
        $url = 'https://api.netease.im/nimserver/team/kick.action';
        $data= array(
            'tid' => $params['tid'],
            'owner' => $params['owner'],
            'members' => json_encode($params['members'])
        );
        
        $result = $this->postDataCurl($url,$data);
        return $result;
    }

    /**
    * 修改群消息
    * @param $params['tid']   [群ID]
    * @param $params['owner'] [群主用户帐号]
    * @return $result      [返回array数组对象]
    */
    public function updGroup($params){
    
        if (empty($params) || empty($params['tid']) || empty($params['owner'])){
            return json_encode(['code'=>0, 'msg'=>'参数为空']);
        }
        $url = 'https://api.netease.im/nimserver/team/update.action';
        $result = $this->postDataCurl($url,$params);
       
        return $result;
    }

    /**
    * 解散群（删除群）
    * @param post参数
    * @param $params['tid']     [群ID]
    * @param $params['owner']   [群主用户帐号]
    * @return $result      [返回array数组对象]
    */
    public function removerGoup($params){
        if (empty($params) || empty($params['tid']) || empty($params['owner'])){
            return json_encode(['code'=>0, 'msg'=>'参数为空']);
        }
        $url = 'https://api.netease.im/nimserver/team/remove.action';
        $result = $this->postDataCurl($url,$params);
       
        return $result;
    }

    /**
    *  任命管理员 提升普通成员为群管理员，可以批量，但是一次添加最多不超过10个人。
    * @param 参数
    * @param $params['tid']     [群ID]
    * @param $params['owner']   [群主ID]
    * @param $params['members'] [被任命为管理员的账号]   
    * @return $result      [返回array数组对象]      
    */
    public function addManager($params){
        if (empty($params) || empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数为空']);
        }

        if (count($params['members']) > 10){
             return json_encode(['code'=>414, 'msg'=>'一次被任命管理员最多十个']);
        }
        $url = 'https://api.netease.im/nimserver/team/addManager.action';
        $data = array(
            'tid' => $params['tid'],
            'owner' => $params['owner'],
            'members' => json_encode($params['members'])
        );

        $result = $this->postDataCurl($url,$data);
       
        return $result;
    }

    /**
     * 移除管理员 
     * @param $params['tid'] 群ID]  
     * @param $params['owner'] 群主用户帐号，最大长度32字符 
     * @param $params['members']  长度最大1024字符（一次解除最多10个管理员）
     * @return $result    [返回array数组对象]
     */
    public function removeManager($params){
        if (empty($params) || empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数为空']);
        }

        if (count($params['members']) > 10){
             return json_encode(['code'=>414, 'msg'=>'一次解除最多10个管理员']);
        }
        $url = 'https://api.netease.im/nimserver/team/removeManager.action';
        $data = array(
            'tid' => $params['tid'],
            'owner' => $params['owner'],
            'members' => json_encode($params['members'])
        );

        $result = $this->postDataCurl($url,$data);
       
        return $result;
    }

    /**
    * 获取用户名片，可批量
    * @param  $accids    [用户帐号（例如：JSONArray对应的accid串，（一次查询最多为200）]
    * @return $result    [返回array数组对象]
    */
    public function getUinfos($accids){

        if(count($accids) < 1){
            return json_encode(array('code'=>414, 'desc'=>'用户账号为空'));
        }
        $url = 'https://api.netease.im/nimserver/user/getUinfos.action';
        $data= array(
            'accids' => json_encode($accids)
        );
        
        $result = $this->postDataCurl($url,$data);
        return $result;
    }

    /**
    * 获取用户所加入的群
    * @param  $accids    [用户帐号（例如：JSONArray对应的accid串，（一次查询最多为200）
    * @return $result    [返回array数组对象]
    */
    public function getJoinTeams($accid){

        if(count($accid) < 1){
            return json_encode(array('code'=>414, 'desc'=>'用户账号为空'));
        }
        $url = 'https://api.netease.im/nimserver/team/joinTeams.action';
        $data= array(
            'accid' => $accid
        );
        
        $result = $this->postDataCurl($url,$data);
        return $result;
    }

    /**
    * 创建云信账号
    * @param params['accid'] 云信ID params['token']密码 
    * @return $result    [返回array数组对象]
    */
    public function createUser($params){
        $url = 'https://api.netease.im/nimserver/user/create.action';
        
        $result = $this->postDataCurl($url,$params);
        return $result;
    }

    /**
    * 更新云信token
    * @param params['accid'] 云信ID params['token'] 新的token （密码）
    * @return $result    [返回array数组对象]
    */
    public function updateUserToken($params){
        if (empty($params['accid']) || empty($params['token'])){
            return json_encode(array('code'=>414, 'desc'=>'用户账号为空'));
        }
        $url = 'https://api.netease.im/nimserver/user/update.action';
         
        $result = $this->postDataCurl($url,$params);
        return $result;
    }

    /**
    * 更新用户基本信息
    * @param 用户信息
    * @return $result    [返回array数组对象]
    */
    public function updUserInfo($params){

        if (empty($params['accid'])){
            return json_encode(array('code'=>414, 'desc'=>'用户账号为空'));
        }
        $url = 'https://api.netease.im/nimserver/user/updateUinfo.action';

        $result = $this->postDataCurl($url,$params);
        return $result;
    }

    /**
    *  移交群主
    * @param 参数
    * @param $params['tid']     [群ID]
    * @param $params['owner']   [群主ID]
    * @param $params['newowner'] [新群主账号]   
    * @param $params['leave'] [1:群主解除群主后离开群，2：群主解除群主后成为普通成员。其它414]
    * @return $result      [返回array数组对象]      
    */
    public function changeOwner($params){
        if (empty($params) || empty($params['tid']) || empty($params['owner']) || empty($params['newowner']) || empty($params['leave'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确']);
        }

        if ($params['leave'] != 1 && $params['leave'] != 2 ){
             return json_encode(['code'=>414, 'msg'=>'移交类型不对']);
        }
        
        $url = 'https://api.netease.im/nimserver/team/changeOwner.action';
        $data = array(
            'tid'      => $params['tid'],
            'owner'    => $params['owner'],
            'newowner' => $params['newowner'],
            'leave'    => $params['leave']
        );

        $result = $this->postDataCurl($url,$data);
       
        return $result;
    }

    /**
    * 主动退群
    * @param 参数
    *  tid 群ID
    *  accid 退群的accid
    */
    public function leaveGroup($params){
        if (empty($params['accid']) || empty($params['tid'])){
            return json_encode(array('code'=>414, 'desc'=>'用户账号为空'));
        }
        $url = 'https://api.netease.im/nimserver/team/leave.action';

        $result = $this->postDataCurl($url,$params);
        return $result;
    }
}