<?php
namespace app\app\controller;  

use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
use app\app\lib\IMServer;

class Imindex extends Base
{ 
    public function __construct(){
        parent::__construct();    
    }

    /*
    * 创建群信息
    * http://localhost.web/wapi/public/index.php/app/imindex/create_group
    * @param POST 参数说明
    *    tname    必填 群名称 最大长度64字符 
    *    owner    必填 群主用户帐号最大长度32字符 
    *    members  必填 群成员 一次最多拉200个成员 
    *    msg      必填 邀请发送的文字  最大长度150字符
    *    announcement    群公告，最大长度1024字符  
    *    intro   群描述，最大长度512字符
    *    custom  第三方扩展
    *    icon  群图标
    *    beinvitemode   被邀请人同意方式，0-需要同意(默认),1-不需要同意。其它返回414
    *    invitemode  谁可以邀请他人入群，0-管理员(默认),1-所有人。其它返回414
    *    uptinfomode 谁可以修改群资料，0-管理员(默认),1-所有人。其它返回414
    *    upcustommode    谁可以更新群自定义属性，0-管理员(默认),1-所有人。其它返回414
    */
    public function  create_group(){
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();


        $im = new IMServer();
        if(empty($params['tname']) || empty($params['owner']) || empty($params['members']) || empty($params['msg']) ){
            return json_encode(['code'=> 414, 'msg'=>'参数不正确']);
            exit;
        }

        if (count($params['members']) > 200){
            return json_encode(['code'=> 414, 'msg'=>'群成员人数太多了，一次最多200人']);
        }

        $data = $params;
        $data['members']      = json_encode($params['members']);   //群成员
        $data['magree']       = isset($params['$magree']) ? (int)$params['magree'] : 0;            
        $data['joinmode']     = isset($params['$joinmode']) ? (int)$params['$joinmode'] : 0; 
        $data['beinvitemode'] = isset($params['$beinvitemode']) ? $params['$beinvitemode'] : 0; 
        $data['invitemode']   = isset($params['$invitemode']) ? $params['$invitemode'] : 0;
        $data['uptinfomode']  = isset($params['$uptinfomode']) ? $params['$uptinfomode'] : 0; 
        $data['upcustommode'] = isset($params['$upcustommode']) ? $params['$upcustommode'] : 0;

        $res_data = $im->createGroup($data);
        
        return json_encode($res_data);        
    }

    /**
    * 高级群基本信息修改
    * localhost.web/wapi/public/index.php/app/imindex/upd_group?tid=672637468&owner=test&intro=放假放假
    * @param POST 参数说明
    *   tid           群ID 必填
    *   tname         群名称，最大长度64字符
    *   owner         群主用户帐号，最大长度32字符 必填
    *   announcement  群公告，最大长度1024字符
    *   intro         群描述，最大长度512字符
    *   joinmode      群建好后，sdk操作时，0不用验证，1需要验证,2不允许任何人加入。其它返回414
    *   custom        自定义高级群扩展属性，,最大长度1024字符
    *   icon          群头像，最大长度1024字符
    *   beinvitemode  被邀请人同意方式，0-需要同意(默认),1-不需要同意。其它返回414
    *   invitemode    谁可以邀请他人入群，0-管理员(默认),1-所有人。其它返回414
    *   uptinfomode   谁可以修改群资料，0-管理员(默认),1-所有人。其它返回414
    *   upcustommode  谁可以更新群自定义属性，0-管理员(默认),1-所有人。其它返回414
    */
    public function upd_group(){
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();

        if(empty($params['tid']) || empty($params['owner'])){
            return json_encode(['code'=> 414, 'msg'=>'参数不正确']);
            exit;
        }
        $im = new IMServer();
        $res_data = $im->updGroup($params);
        
        return json_encode($res_data);      
    }


    /**
    * 获取IM用户信息
    * localhost.web/wapi/public/index.php/app/imindex/get_user_info?accids=test
    * @param accids IM账号名称
    */
    public function get_user_info(){
        //检测参数 
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input(); 
        if (empty($params['accids'])){
            return json_encode(['code'=>414, 'msg'=>'用户ID 为空！']);
            exit;
        }

        $im = new IMServer();
        $res_data = $im->getUinfos(array($params['accids']));
        
        return json_encode($res_data);
    }

    /**
    * 拉人入群
    * localhost.web/wapi/public/index.php/app/imindex/add_group?tid=672637468&owner=test&member_info[]=17281_liuxuan(geturl)
    * @param 参数说明
    *   tid  必填 群ID 最大长度128字符
    *   owner   群主用户帐号，最大长度32字符
    *   app_ids   用户ID
    *   msg     邀请发送的文字
    *   attach  扩展字段
    *   magree   管理后台建群时，0不需要被邀请人同意加入群，1需要被邀请人同意才可以加入群。其它会返回414
    */
    public function add_group(){
        //检测参数
        //$cc = $this->decrypt();
        //if($cc['code'] != 1){ return json($cc);die;} 
        
        $params = input();

        if (empty($params['tid']) || empty($params['owner']) || empty($params['app_ids'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
            exit;
        }

        if (count($params['app_ids']) > 200){
            return json_encode(['code'=> 414, 'msg'=>'群成员人数太多了，一次最多200人']);
        }

        $data['tid'] = $params['tid'];
        $data['owner'] = $params['owner'];
        $data['magree']   = isset($params['magree']) ? $params['magree'] : 0;
        $data['msg']      = isset($params['msg']) ? $params['msg'] : '欢迎入群';

        $users = Db::name('y_med_app_members')->where(['app_id'=>array('in', $params['app_ids']), 'status'=>1, 'status_im'=>1])->select();
        
        $i_data = array();
        if (is_array($users)){
            foreach ($users as $user_info) {
                $i_data['members'][] = $user_info['im_user'];
                $i_data['app_ids'][] = $user_info['app_id'];
            }
        }else{
            return json_encode(['code'=> 414, 'msg'=>'成员无效']);
        }

        $data['members'] = json_encode($i_data['members']);
        $im = new IMServer();

        $res_data = $im->addIntoGroup($data);
        if ($res_data['code'] != '200'){
             return json_encode($res_data);
             exit;
        }

        //插入成员表
        $members = $i_data['members'];
        $db_group_info = Db::name('y_med_app_im_group')->where(['tid'=>$params['tid']])->find();
        if (is_array($members)){
            foreach ($members as $key => $accid) {
                $ins_date = array(
                    'gid'   => $db_group_info['id'],
                    'im_user'=> $accid,
                    'type'  => 1,
                    'app_id' => $i_data['app_ids'][$key],
                );
                $exist_flg = Db::name('y_med_app_im_group_members')->where(['gid'=>$db_group_info['id'], 'app_id'=>$i_data['app_ids'][$key]])->count();
               
                if (!$exist_flg){
                    $ins_res = Db::name('y_med_app_im_group_members')->insert($ins_date);
                    if ($ins_res === false){
                        return json_encode(['code'=> 414, 'msg'=>'插入成员表失败']);
                        die;
                    }
                }
            }
        }

        //更新群人数
        $this->get_group_info(['tid'=>$params['tid']]);
        
        return json_encode(['code'=>200]);
    }

    /** 
    * 踢人出群 (需要提供群主accid以及要踢除人的accid)
    * /wapi/public/index.php/app/imindex/kick_out_group?tid=1359794918&owner=test&members[]=liuxuan&leave=2
    * @param Post 参数
    * tid 群ID 
    * owner 群主的帐号，最大长度32字符
    * members 移除人的账号集合 [aaa","bbb"] 一次最多操作200个accid
    * attach 扩展 可用于些踢出群原因
    */
    public function kick_out_group(){
        //检测参数
        //$cc = $this->decrypt();
        //if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();

        if (empty($params['tid']) || empty($params['owner']) || empty($params['members']) ){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        
        $im = new IMServer();
        $res_data = $im->kick($params);
        //踢人成功了减少群成员人数
        if ($res_data['code'] == '200'){

            //从群成员表删除
            $db_group_info = Db::name('y_med_app_im_group')->where(['tid'=>$params['tid']])->find();
            $del_flg = Db::name('y_med_app_im_group_members')->where(['gid'=>$db_group_info['id'], 'im_user'=>array('in', $params['members'])])->delete();
            if ($del_flg === false){
                return json_encode(['code'=>414, 'msg'=>'删除群成员失败']);
            }
            //获取im人数更新数据库群人数
            $this->get_group_info(['tid'=>$params['tid']]);

        }
        return json_encode($res_data);
    }

    /**
    * 删除群
    * localhost.web/wapi/public/index.php/app/imindex/remove_group?tid=672637468&owner=test
    * @param Post参数
    * tid    群ID
    * owner  群主用户帐号，最大长度32字符
    */
    public function remove_group(){
        //检测参数
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();

        if (empty($params['tid']) || empty($params['owner'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        
        $im = new IMServer();
        $res_data = $im->removerGoup($params);

        return json_encode($res_data);
    }

    /**
    * 任命管理员提升普通成员为群管理员，可以批量，但是一次添加最多不超过10个人。
    * localhost.web/wapi/public/index.php/app/imindex/add_manager?tid=672637468&owner=test&members[]=test
    * @param post 参数说明
        tid 群ID
        owner    群主用户帐号，最大长度32字符
        members  被任命为管理员的账号长度最大1024字符（一次添加最多10个管理员）
     */
    public function add_manager(){
        //检测参数
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();
        if (empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        if (count($params['members']) > 10){
             return json_encode(['code'=>414, 'msg'=>'一次任命管理员不能超过10个']);
        }

        $im = new IMServer();
        $res_data = $im->addManager($params);
        //变更数据库存储的成员类型由管理员变为普通用户
        if ($res_data['code'] == '200'){
            $db_group_info = Db::name('y_med_app_im_group')->where(['tid'=>$params['tid']])->find();
            $gid = $db_group_info['id'];
           
            $upd_res = Db::name('y_med_app_im_group_members')->where(['gid'=>$gid, 'im_user'=>array('in', $params['members'])])->update(['type'=>2]);   
            if (false === $upd_res){
                return json_encode(['code'=>414, 'msg'=>'移除管理员数据库处理失败']);
            }        
        }

        return json_encode($res_data);
    }
 
    /**
    * 移除管理员 一次最多十个
    * localhost.web/wapi/public/index.php/app/imindex/remove_manager?tid=672637468&owner=test&members[]=test
    * @param post 参数说明
        tid 群ID
        owner    群主用户帐号，最大长度32字符
        members  被任命为管理员的账号长度最大1024字符（一次添加最多10个管理员）
     */
    public function remove_manager(){
        //检测参数
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();
        if (empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        if (count($params['members']) > 10){
            return json_encode(['code'=>414, 'msg'=>'一次解除管理员不能超过10个']);
        }

        $im = new IMServer();
        $res_data = $im->removeManager($params);
        //变更数据库存储的成员类型由管理员变为普通用户
        if ($res_data['code'] == '200'){
            $db_group_info = Db::name('y_med_app_im_group')->where(['tid'=>$params['tid']])->find();
            $gid = $db_group_info['id'];
           
            $upd_res = Db::name('y_med_app_im_group_members')->where(['gid'=>$gid, 'im_user'=>array('in', $params['members'])])->update(['type'=>1]);   
            if (false === $upd_res){
                return json_encode(['code'=>414, 'msg'=>'移除管理员数据库处理失败']);
            }        
        }

        return json_encode($res_data);
    }
 

    /**
    * 查询指定群的详细信息（群信息+成员详细信息）
    * localhost.web/wapi/public/index.php/app/imindex/get_group_info?tid=672637468
    * @param Post 参数
    * $tid     指定群id
    */
    public function get_group_info($params=array()){
        if(empty($params)){
            //检测参数
            $cc = $this->decrypt();
            if($cc['code'] != 1){ return json($cc);die;} 
            
            $params = input();
        }
        
        if (empty($params['tid'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        
        $im = new IMServer();
        $res_data = $im->getGroupInfo($params);

        //更新一下群人数
        if ($res_data['code'] == '200'){
            $member_cnt = count($res_data['tinfo']['members']);
            $admin_cnt = isset($res_data['tinfo']['admins']) ? count($res_data['tinfo']['admins']) :0;
            $total_cnt = (int)$member_cnt + (int)$admin_cnt+1;
            $upd_res = Db::name('y_med_app_im_group')->where(['tid'=>$params['tid']])->update(['member_cnt'=>$total_cnt]);
            if (false === $upd_res){
                return json_encode(['code'=>414, 'msg'=>'同步群成员人数失败']);
            }
        }
        return json_encode($res_data);
    }

    //消息抄送
    public function message_copy(){
        $post_data = file_get_contents("php://input");
        $post_data = mb_convert_encoding($post_data, 'UTF-8', 'auto');

        $header_data = array();
        foreach (getallheaders() as $name => $value) {
            $header_data[$name] = $value;
        }
        $im = new IMServer();
        $app_key   = '';
        $md5_data  = '';
        $check_sum = '';
        $cur_time  = '';

        if (!empty($header_data)){
            $app_key   = $header_data['AppKey'];
            $md5_data  = $header_data['MD5'];
            $check_sum = $header_data['CheckSum'];
            $cur_time  = $header_data['CurTime'];
        }
        
        $verifyMD5 = md5($post_data);
        //md5值校验。判断传输中值是否发生改变
        if ($md5_data != $verifyMD5){
            echo json_encode(array('code'=>414));
            exit;   
        }

        //验证CheckSum
        $verifyChecksum = $im->getMessageCheckSum($md5_data, $cur_time);

        if ($check_sum != $verifyChecksum){
            echo json_decode(array('code'=>414));
            exit;
        }
        $content = $im->json_to_array($post_data);
        $content['base_data'] = stripslashes($post_data);

        if (!empty($content['eventType'])){
            Db::name('y_med_app_im_message_record')->insert($content);
        }
       
        echo json_encode(array('code'=>200));
    }

    /**
    *创建用户
    * localhost.web/wapi/public/index.php/app/imindex/create_user?_sign=XXXX&_timestamp=XXX&nikename=XXXX&pwd_im=XXX
    * @param Post 参数
    * im_user 账号
    * nikename 用户昵称
    * im_pwd  密码
    * icon 头像
    */
    public function create_user($params = array()){ 
        if(empty($params)){
            //检测参数
            $cc = $this->decrypt();
            if($cc['code'] != 1){ return json($cc);die;} 

            $params = input();
        } 

        if (empty($params['im_user']) || empty($params['nickname']) || empty($params['im_pwd'])){
            return json_encode(array('code'=>414, 'msg'=>'参数不正确！'));
            exit;
        }

        $data = $params;
        //网易云通信ID，最大长度32字符 
        $data['accid'] = $params['im_user'];
        if (mb_strlen($data['accid']) > 32){
            return json_encode(array('code'=>405, 'msg'=>'账号参数长度过长'));
        }
        //网易云通信ID昵称，最大长度64字符，
        $data['name']  = $params['nickname'];
        if(mb_strlen($data['name']) > 64){
            return json_encode(array('code'=>405, 'msg'=>'昵称参数长度过长'));
        }
        //登录token值，最大长度128字符，
        $data['token'] = $params['im_pwd'];
        if(mb_strlen($data['token']) > 128){
            return json_encode(array('code'=>405, 'msg'=>'token参数长度过长'));
        }

        $im = new IMServer();
        $res_data = $im->createUser($data); 

        return json_encode($res_data);
    }


    /**
    * 更新IM用户头像
    * @param Post参数
    *   im_user  用户帐号，最大长度32字符，必须保证一个APP内唯一
        icon   用户头像，最大长度1024字节  http://wechat.y-med.com.cn/attachment/images/8/2017/07/k2zGR0wayWc4RWKRmWWHcH8a38ARs3.png
    */
    public function upd_user_info($params=array()){
        if(empty($params)){
            //检测参数
            $cc = $this->decrypt();
            if($cc['code'] != 1){ return json($cc);die;} 

            $params = input();
        } 

        if (empty($params['im_user']) || (!isset($params['icon']) && !isset($params['nickname']))){
            return json_encode(array('code'=>414, 'msg'=>'参数不正确！'));
        }

        $im = new IMServer();
        $im_data['accid'] = $params['im_user'];
        if (isset($params['icon'])){
            $im_data['icon']  = $params['icon'];
        }
        if (isset($params['nickname'])){
            $im_data['name'] = $params['nickname'];
        }
        
        $res_data = $im->updUserInfo($im_data);

        return json_encode($res_data);
    }

    /**
    * 移交群主
    * localhost.web/wapi/public/index.php/app/imindex/change_owner?tid=1359794918&owner=test&newowner=liuxuan&leave=2
    * @param Post 参数
    * tid 群ID
    * owner  群主账号
    * newowner   新群主帐号，最大长度32字符
    * leave   1:群主解除群主后离开群，2：群主解除群主后成为普通成员
    */
    public function change_owner(){
        
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();
        if (empty($params) || empty($params['tid']) || empty($params['owner']) || empty($params['newowner']) || empty($params['leave'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确']);
        }
       
        if ($params['leave'] != 1 && $params['leave'] != 2 ){
            return json_encode(['code'=>414, 'msg'=>'移交类型不对']);
        }

        $im = new IMServer();
        $res_data = $im->changeOwner($params);

        //更新数据库群主名称
        $upd_res = Db::name('y_med_app_im_group')->where(['tid'=>$params['tid'], 'owner'=>$params['owner']])->update(['owner'=>$params['newowner']]);

        if (false === $upd_res){
            return json_encode(['code'=>414, 'msg'=>'移交群主失败']);
        }
        return json_encode($res_data);
    }

    /**
    * 健康交流群列表/搜索群列表
    * /wapi/public/index.php/app/imindex/get_group_list?p=1&uid=17282
    * @param 参数
    *  p: 页码
    *  order:排序
    *  keywords: 搜索关键字
    *  uid 用户ID
    *  is_all 默认为1  不取全部赋值0
    */
    public function get_group_list(){
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 
        
        $input = input();
        $uid   = isset($this->uid) ? $this->uid: '';
        $limit = isset($input['limit']) ? $input['limit'] : $this->pagetLimit();
        $order = isset($input['order']) ? $input['order'] : 'member_cnt desc';
        $p     = isset($input['p']) ? intval($input['p']) : 1;
        $keywords = isset($input['keywords'])  ? trim($input['keywords'])  : '';
        $is_all = isset($input['is_all']) ? intval($input['is_all']) : 1;
        $where = [];

        if(!empty($keywords)){
            $where['tname'] = ['like','%'.$keywords.'%'];
        }
        //如果uid存在 查询关注的疾病关联的群
        if (!empty($uid)  && $is_all != 1 ){
            $where['c.id']=['exp', "in ( SELECT cid FROM ims_y_med_collect WHERE ctype = 4 AND uid = ".$uid." )"];
        }
        $where['g.status'] = 1;
        //先根据条件获取一下数据。如果数据为零，清空条件查询全部，主要用于首页使用
        $groupcount = Db::name('y_med_app_im_group g')->join('y_med_category c', 'g.cid = c.id', 'left')->where($where)->order($order)->count();
        if ($groupcount < 1 && $is_all == 0 ){
            $where = [];
            $where['g.status'] = 1;
            $grouplist = Db::name('y_med_app_im_group g')->field('g.*, c.title')->join('y_med_category c', 'g.cid = c.id', 'left')->where($where)->order($order)->limit($limit)->select();
            $groupcount = Db::name('y_med_app_im_group g')->join('y_med_category c', 'g.cid = c.id', 'left')->where($where)->order($order)->count();
        }else{
            $grouplist = Db::name('y_med_app_im_group g')->field('g.*, c.title')->join('y_med_category c', 'g.cid = c.id', 'left')->where($where)->order($order)->limit($limit)->select();
        }

        if(empty($grouplist))
            return json(['code'=>0,'msg'=>'暂时没有该数据']);

        foreach($grouplist as $k=>$v){
            if(!empty($uid)){
                //检查用户是否加入
                $grouplist[$k]['is_join'] = Db::name('y_med_app_im_group_members gm')->join('y_med_app_members m', 'gm.im_user=m.im_user', 'left')->where(['gm.gid'=>$v['id'],'m.app_id'=>$uid,'status'=>1,'status_im'=>1])->count();
                //echo Db::name('y_med_app_im_group_members gm')->getLastSql();
            }else {
                $grouplist[$k]['is_join'] = 0;
            }
        }

        return json(['code'=>1,'total'=>$groupcount,'p'=>(int)$p,'msg'=>$grouplist]);
    }

    /**
    *  加入的群列表 (我加入的 好友加入的群)
    *  http://localhost.web/wapi/public/index.php/app/imindex/get_join_terms?uid=17282&friendid=17275
    *  uid 登录用户ID
    *  friendid 好用用户id
    *   
    */
    public function get_join_terms(){
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        if(!isset($this->uid) || empty($this->uid)){
            return json_encode(['code'=>0, 'msg'=>'参数不正确']);
            die;
        }
        $input = input();

        $uid   = isset($this->uid) ? $this->uid: '';
        $friendid = isset($input['friendid']) ?$input['friendid'] : '';

        $limit = isset($input['limit']) ? $input['limit'] : $this->pagetLimit();
        $p     = isset($input['p']) ? intval($input['p']) : 1;

        $selectid = $this->uid;
        $sel_field = '';
        if (!empty($friendid)){
            $frind_info = Db::name('y_med_app_members')->where(array('im_user'=>$friendid, 'status'=>1))->find();;
            $selectid = $frind_info['app_id'];
            $sel_field = " , (SELECT COUNT(`mid`) FROM ims_y_med_app_im_group_members igm WHERE app_id=" . $this->uid . " AND igm.gid = g.id ) AS is_join";
        }

        $where = array('m.app_id'=>$selectid, 
                       'm.status'=> 1 ,
                       'm.status_im'=>1, 
                       'g.id'=>array('exp', ' is not null'));

        //如果没有好友ID friendid 参数 检索的是自己加入的群列表，如果有好友id 则是好友加入的群列表 
        $grouplist = Db::name('y_med_app_im_group_members gm')->field('g.*' . $sel_field)->join('y_med_app_im_group g', 'gm.gid = g.id', 'left')->join('y_med_app_members m', '`gm`.`app_id`=`m`.`app_id`', 'left')->where($where)->order('member_cnt desc')->limit($limit)->select();
        
        $groupcount = Db::name('y_med_app_im_group_members gm')->join('y_med_app_im_group g', 'gm.gid = g.id', 'left')->join('y_med_app_members m', '`gm`.`app_id`=`m`.`app_id`', 'left')->where($where)->order('member_cnt desc')->count();

        if(empty($grouplist))
            return json(['code'=>0,'msg'=>'暂时没有该数据']);

        return json(['code'=>1,'total'=>$groupcount,'p'=>(int)$p,'msg'=>$grouplist]);
    }

    /**
    * 主动退群
    * @param 参数
          tid   群ID
          uid   退群的uid
    */
    public function leave_group(){
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        $params = input();

        if(!isset($this->uid) || empty($this->uid) || empty($params['tid'])){
            return json_encode(['code'=>0, 'msg'=>'参数不正确']);
            die;
        }

        //更新群成员信息表
        $this->get_group_info(['tid'=>$params['tid']]);
           
        //删除群成员表中的记录
        $group_info = Db::name('y_med_app_im_group')->where(['tid'=>$params['tid']])->find();
        $del_flg = Db::name('y_med_app_im_group_members')->where(['gid'=>$group_info['id'], 'app_id'=>$this->uid])->delete();
        if ($del_flg === false){
            return json_encode(['code'=>0, 'msg'=>'删除群成员失败']);
        }

        return json_encode(['code'=>1,'msg'=>'退群成功']);
    }

    /**
    * 主动加入群变更数据库
    * tid IM群ID
    * uid 用户ID
    */
    public function join_terms(){
        $cc = $this->decrypt();  
        if($cc['code'] != 1){ return json($cc);die;} 

        if(!isset($this->uid) || empty($this->uid)){
            return json_encode(['code'=>0, 'msg'=>'参数不正确']);
            die;
        }
        $params = input();

        if(empty($params['tid'])){
            return json_encode(['code'=>0, 'msg'=>'参数不正确']);
            die;
        } 
        $tid = $params['tid'];
        $uid = $this->uid;
        //检测用户有效
        $user_info = Db::name('y_med_app_members')->where(['app_id'=>$uid, 'status'=>1,'status_im'=>1])->find();

        if(empty($user_info)){
            return json_encode(['code'=>0, 'msg'=>'用户无效']);
            die;
        }
        //检测群有效
        $group_info = Db::name('y_med_app_im_group')->where(['tid'=>$tid, 'status'=>1])->find();

        if (empty($group_info)){
            return json_encode(['code'=>0, 'msg'=>'群无效']);
            die;
        }
        //检测是否已经是成员
        $gid = $group_info['id'];
        $is_exist = Db::name('y_med_app_im_group_members')->where(['gid'=>$gid, 'app_id'=>$uid])->count();
        if (!$is_exist){
            //插入群成员表
            $ins_date = ['gid'=>$gid, 'type'=>1, 'app_id'=>$uid, 'im_user'=>$user_info['im_user']];
            $ins_res = Db::name('y_med_app_im_group_members')->insert($ins_date);
            if ($ins_res){
                $this->get_group_info($tid);
            }else{
                return json_encode(['code'=>0, 'msg'=>'插入群成员是失败']);
                die;
            }
        }
        
        return json_encode(['code'=>1, 'msg'=>'加入成功']);
    }

    
}