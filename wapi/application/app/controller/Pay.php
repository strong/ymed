<?php
namespace app\app\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use think\Cache; 
use app\common\controller\Client;   
class Pay extends Client
{      
    /*
        wx创建订单 
        title       标题
        type        付款类型 1会员 2是赞赏 3 4公众号栏目支付 5是App栏目支付
        colid       栏目id
        uid         
        回调地址    http://wechat.y-med.com.cn/wb/index.php/index/pay/wxpayhy/
    */
    public function inorder(){ 
        $input = input();
        if(empty($this->uid)){
            return json(['code'=>0,'msg'=>"请登录！"]);die;
        }
        if(!empty($input['colid'])){
            $is_order = Db::name('order')->where(['uid'=>$this->uid,'sonid'=>$input['colid']])->find(); 

            $col = $this->getcol(['colid'=>$input['colid']]);
            
            if(!empty($col[0])){ 
                $col = $col[0];
                if(!empty($is_order)){
                    $col['orderid'] = $is_order['orderid'];
                    return json(['code'=>1,'msg'=>$col]);die;
                }
                $total_fee = $col['price'];
                $title = isset($input['title'])?$input['title']:"app栏目支付";
                $type = isset($input['type'])?$input['type']:5;
                $colid = $input['colid'];
                //创建订单
                $data = array();
                $data['addtime'] = time();
                $data['orderid'] = date('YmdHis',$data['addtime']).$this->uid.$data['addtime'];
                $data['type'] = $type;
                $data['uid'] = $this->uid;
                $data['title'] = $title;
                $data['price'] = $total_fee;
                $data['status'] = 0;
                $data['tag'] = rand(10,99999);
                if ($type == 5) {
                   $data['sonid'] = $colid;
                }

                $is = Db::name('order')->insert($data);
                if($is){ 
                    $col['orderid'] = $data['orderid'];
                    return json(['code'=>1,'msg'=>$col]);
                }
            } 
        }
        return json(['code'=>0,'msg'=>"订单生成失败"]); 
    }

    /*
        $orderid   订单号
        $postObj   支付订单信息 
        uid  
    */
    public function column_order(){
        $input = input();
        if(empty($this->uid)){
            return json(['code'=>0,'msg'=>"请登录！"]);die;
        }
        $postObj = isset($input['postObj'])?$input['postObj']:'app支付';
        if(!empty($input['orderid'])){
            $orderlist = Db::name('order')->where(['uid'=>$this->uid,'orderid'=>$input['orderid'],'status'=>0])->find();
            if(!empty($orderlist)){
                $data['uid'] = $this->uid;
                $data['price'] = ($orderlist['price']/100); 
                $data['addtime'] = time();
                $data['colid'] = $orderlist['sonid'];
                //插入记录表
                Db::name('y_med_column_order')->insert($data);
                //修改支付状态
                Db::name('order')->where(['orderid'=>$orderlist['orderid']])->update(['status'=>1,'wxxml'=>json_encode($postObj)]);
                return json(['code'=>1,'msg'=>'支付成功']);die;
            }else{
                return json(['code'=>0,'msg'=>"订单错误！"]);die;
            }
        } 
        //发送消息
        // $this->add_message(['uid'=>$data['uid'],'colid'=>$data['colid']]); 
    }
}