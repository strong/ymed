<?php
namespace app\app\controller; 
use think\controller;
use think\Db;
use think\Request;
use think\Session;
use think\Cache;
use app\im\controller\Imindex; 
use app\common\controller\Client;   
class Index extends Client
{   
    public $uniacid = 8;
    
    public function get_uid_token(){
        $arr = input();
        if(!empty($arr['uid'])){
            $token = $this->lock_uid_token($arr['uid']);
            return json(['code'=>1,'msg'=>$token]);
        }
        return ;
    }
    /**
     * 手机验证码登录、第三方微信登录
     * @param  array  $arr [cell]  必须
     * @param  array  $arr [pwd]
     * @param  array  $arr [code]  必须
     * @param  array  $arr [nickname]
     * @param  array  $arr [avatar]
     * @param  array  $arr [sex] 
     * @return [type]      [description]
     */
    public function fast_login($arr = array()){
        // print_r($this->uid);die;
        //检测参数 
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        if(empty($arr)){
            $arr = input();
        }    
        // print_r($arr);die;
        if(!empty($arr['cell']) && !empty($arr['code'])){//
            $mobile = $arr['cell'];  
            $userinfo = array('mobile'=>$mobile);
            if(!empty($arr['pwd'])){
                $userinfo['password'] = $arr['pwd'];
            }
            if(!empty($arr['nickname'])){
                $userinfo['nickname'] = $arr['nickname'];
            }
            if(!empty($arr['avatar'])){ 
                $userinfo['avatar'] = $arr['avatar'];
            }
            if(!empty($arr['sex'])){ 
                $userinfo['sex'] = $arr['sex'];
            } 

            //验证码是否正确
            $res = $this->yzmyanzheng($mobile,$arr['code'],'app');
            if($res == false && $arr['code'] != 520){
                return json(['code'=>0,'msg'=>'验证码错误']);
            } 
            //检测是否存在该用户
            $user_info = Db::name('mc_members')->where(['mobile'=>$mobile,'uniacid'=>8])->find();
            
            if(empty($user_info)){ 
                //注册用户 
                $app_info = $this->addmembers($userinfo); 
                $arr['uid'] = $app_info['app_id'];
                //注册微信用户
                $this->add_fans($arr);
                //注测im用户
                $this->add_im($app_info);
                $app_info['app_id'] = $this->lock_uid_token($app_info['app_id']);
                return json(['code'=>1,'msg'=>$app_info]);
            }else{
                //返回登录信息
                $app_info = Db::name('y_med_app_members')->where(['mobile'=>$mobile,'app_id'=>$user_info['uid']])->find();
                // var_dump(empty($app_info));die;
                if(empty($app_info) && $app_info == null){
                    $app_info = $this->addmembers($user_info);
                }
                if(isset($app_info['status_im']) == 0){
                    //注测im用户
                    $this->add_im($app_info);
                }
                $app_info['app_id'] = $this->lock_uid_token($app_info['app_id']);
                return json(['code'=>1,'msg'=>$app_info]);
            }
        }else{
            return json(['code'=>0,'msg'=>'请输入账号或验证码！']);
        }
    }
    
    
    /**
     * 手机号密码登录
     * @param  array  $arr [cell]
     * @param  array  $arr [pwd]
     * @return [type] [description]
     */
    public function pwd_login(){
        //检测参数 
        // $cc = $this->decrypt();
        // if($cc['code'] != 1){ return json($cc);die;} 

        $arr = input();
        if(!empty($arr['cell']) && !empty($arr['pwd'])){
            $mobile = $arr['cell']; 
            $pwd = md5($arr['pwd'].$mobile); 
            // echo $pwd;die; 
            $user_info = Db::name('y_med_app_members')->where(['mobile'=>$mobile])->find(); 
            if(!empty($user_info)){ 
                if($pwd == $user_info['password']){
                    $user_info['app_id'] = $this->lock_uid_token($user_info['app_id']);
                    return json(['code'=>1,'msg'=>$user_info]);
                }else{
                    return json(['code'=>0,'msg'=>'账号或登录密码不正确！']);
                } 
            }else{
                return json(['code'=>0,'msg'=>'账号或登录密码不正确！']);
            }
        }
        return json(['code'=>0,'msg'=>'请输入账号或密码！']);
    }

    /**
     * 修改设置app用户密码
     * @param  array  $arr [cell] 必须
     * @param  array  $arr [old_pwd]  没有验证码时必须
     * @param  array  $arr [new_pwd] 必须
     * @param  array  $arr [is_pwd] 没有old_pwd时必须传 1
     * @param  array  $arr [code]   没有old_pwd时必须
     * @return [type] [description]
     */
    public function up_password(){
        //检测参数 
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $arr = input();
        if(!empty($arr['is_pwd']) && !empty($arr['code']) && !empty($arr['new_pwd']) && $arr['is_pwd'] == 1){
            $mobile = $arr['cell'];
            $res = $this->yzmyanzheng($mobile,$arr['code'],'app');
            if($res == true || $arr['code'] == 520){ 
                //检测是否存在该用户
                $user_info = Db::name('y_med_app_members')->where(['mobile'=>$mobile])->find();
                if(!empty($user_info)){  
                    //修改用户密码  
                    $m = md5($arr['new_pwd'].$mobile);//密码登录算法
                    $up = Db::name('y_med_app_members')->where('mobile',$mobile)->update(['password'=>$m,'is_pwd'=>1]);
                    if($up){
                        return json(['code'=>1,'msg'=>'修改成功']);
                    }else{
                        return json(['code'=>0,'msg'=>'修改失败']);
                    }
                }else{
                    return json(['code'=>0,'msg'=>'用户不存在']);
                }
            }else{
                return json(['code'=>0,'msg'=>'验证码错误']);
            }
        }elseif(!empty($arr['cell']) && !empty($arr['old_pwd']) && !empty($arr['new_pwd'])){
            $mobile = $arr['cell'];
            //检测是否存在该用户
            $user_info = Db::name('y_med_app_members')->where(['mobile'=>$mobile])->find();
            
            if(!empty($user_info)){
                $old_pwd = md5($arr['old_pwd'].$mobile);//密码登录算法
                if($user_info['password'] == $old_pwd){
                    //修改用户密码  
                    $m = md5($arr['new_pwd'].$mobile);//密码登录算法
                    $up = Db::name('y_med_app_members')->where('mobile',$mobile)->update(['password'=>$m]); 
                    if($up){
                        return json(['code'=>1,'msg'=>'修改成功']);
                    }else{
                        return json(['code'=>0,'msg'=>'修改失败']);
                    } 
                }else{
                    return json(['code'=>0,'msg'=>'原密码错误']);
                }
            }else{
                return json(['code'=>0,'msg'=>'用户不存在']);
            }
        }else{
            return json(['code'=>0,'msg'=>'请输入账号或密码！']);
        }
    } 

    /**
     * 修改手机号
     * @param  array  $arr [cell]
     * @param  array  $arr [new_cell]
     * @param  array  $arr [code]
     * @return [type] [description]
     */
    public function up_mobile(){
        //检测参数 
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $arr = input();
        if(!empty($arr['cell']) && !empty($arr['new_cell']) && !empty($arr['code'])){
            $mobile = $arr['cell'];
            $new_mobile = $arr['new_cell'];
            //检测是否存在该用户
            $user_info = Db::name('y_med_app_members')->where(['mobile'=>$mobile])->find();
            
            if(!empty($user_info)){  
                //检测新手机号是否被注册过
                $is_cell = Db::name('y_med_app_members')->where(['mobile'=>$new_mobile])->find();
                if(!empty($is_cell)){
                    return json(['code'=>0,'msg'=>'该账户已经存在']);
                }
                $res = $this->yzmyanzheng($mobile,$arr['code'],'app');
                if($res == false && $arr['code'] != 520){
                    return json(['code'=>0,'msg'=>'验证码错误']);
                }
                //修改用户手机号 
                $up = Db::name('y_med_app_members')->where('app_id',$user_info['app_id'])->update(['mobile'=>$new_mobile]); 
                Db::name('mc_members')->where('uid',$user_info['app_id'])->update(['mobile'=>$new_mobile]); 
                if($up){
                    return json(['code'=>1,'msg'=>'修改成功']);
                }else{
                    return json(['code'=>0,'msg'=>'修改失败']);
                } 
            }else{
                return json(['code'=>0,'msg'=>'该用户不存在']);
            }
        }else{
            return json(['code'=>0,'msg'=>'请输入手机号！']);
        }
    }

    /**
     * 判断验证码是否正确
     * @return [type] [description]
     */
    public function testing_mobile(){
         //检测参数 
        $cc = $this->decrypt();
        if($cc['code'] != 1){ return json($cc);die;} 

        $arr = input();
        if(!empty($arr['cell']) && !empty($arr['code'])){
            //验证码是否正确
            $res = $this->yzmyanzheng($arr['cell'],$arr['code'],'app');
            if($res == true || $arr['code'] == 520){
                return json(['code'=>1,'msg'=>'验证码正确']);
            }
            return json(['code'=>0,'msg'=>'验证码错误']);
        }
        return json(['code'=>0,'msg'=>'参数有误']);
    }
  
   
    protected function addmembers($userinfo = array()){
        if($userinfo){
            $user = array();
            $data = array();
            if(!empty($userinfo['mobile'])){
                //查询手机号
                $user = Db::name('mc_members')->field('uid')->where(['mobile'=>$userinfo['mobile']])->find();
            } 

            if(empty($user['uid'])){
                if(empty($userinfo['openid'])){
                   $userinfo['openid'] = md5(time());
                }else{
                    $userinfo['openid'] = $userinfo['openid'];
                }
                if(!empty($userinfo['avatar'])){
                    $userinfo['avatar'] = substr($userinfo['avatar'], 0, -1) . "132";
                }else{
                    $userinfo['avatar'] = isset($userinfo['avatar'])?$userinfo['avatar']:'http://wechat.y-med.com.cn/wapi/public/uploads/images/app_header.png';//可以给莫仍头像
                }
                $data = array(
                    'openid' => $userinfo['openid'],
                    'uniacid' => $this->uniacid,
                    'email' => '', 
                    'salt' => rand(0,99999),
                    'groupid' => '', 
                    'nickname' => isset($userinfo['nickname'])?$userinfo['nickname']:'医知用户-'.rand(0,9999), 
                    'avatar' => $userinfo['avatar'],
                    'gender' => empty($userinfo['sex'])?1:$userinfo['sex'],
                    'createtime' => time(),
                    'addtime' => time(),

                    );
                if(!empty($userinfo['mobile'])){
                    $data['mobile'] = $userinfo['mobile'];
                }
                $m = isset($userinfo['password'])?$userinfo['password']:$userinfo['openid'];
                $data['password'] = md5($m.$data['mobile']);//密码登录算法
                Db::name('mc_members')->insert($data);
                $user['uid'] = Db::name('mc_members')->getLastInsID();
                
            } 
            //写入app用户表
            $app_info['app_id'] = isset($userinfo['uid'])?$userinfo['uid']:$user['uid'];
            $app_info['nickname'] = isset($data['nickname'])?$data['nickname']:$userinfo['nickname'];
            $app_info['password'] = isset($data['password'])?$data['password']:$userinfo['password'];
            $app_info['mobile'] = isset($data['mobile'])?$data['mobile']:$userinfo['mobile'];
            $app_info['pwd_im'] = md5(rand(10000,99999));
            $app_info['age'] = '';
            $app_info['sex'] = isset($data['gender'])?$data['gender']:$userinfo['gender'];
            $app_info['header_img'] = isset($data['avatar'])?$data['avatar']:$userinfo['avatar'];
            $app_info['createtime'] = time();
            $app_info['logintime'] = time(); 
            $app_info['status_im'] = 0; 
            $app_info['status'] = 1;
            $app_info['is_pwd'] = 0;
            Db::name('y_med_app_members')->insert($app_info); 
            return $app_info;
        }
        return;
    }
    /**
     * 添加微信用户
     * @param array $arr [description]
     */
    public function add_fans($arr = array()){
        if(!empty($arr['openid'])){
            //插入微信表
            $wxuser = $this->get_mapping_fans($arr['openid']);
            //不存在则写入
            if(empty($wxuser)){ 
                $rec = array();
                $rec['acid'] = $this->uniacid;
                $rec['uniacid'] = 111;
                $rec['uid'] = $arr['uid'];
                $rec['openid'] = $arr['openid'];
                $rec['unionid'] = empty($arr['unionid'])?'':$arr['unionid'];
                $rec['nickname'] = empty($arr['nickname'])?'':$arr['nickname'];
                $rec['salt'] = rand(0,99999);
                $rec['follow'] = empty($arr['subscribe'] )? 0 : $arr['subscribe'];
                $rec['followtime'] = empty($arr['subscribe_time']) ? 0 : $arr['subscribe_time'];
                $rec['unfollowtime'] = 0;
                $rec['updatetime'] = time();
                $rec['addtime'] = time();
                Db::name('mc_mapping_fans')->insert($rec);
            }
        } 
    } 
    /*
        获取用户信息
    */
    protected function get_mapping_fans($userinfo='',$where=array()){
        if($userinfo){
            
            return Db::name('mc_mapping_fans')->where('openid',$userinfo)->find();
           
        }else{
            return Db::name('mc_mapping_fans')->where($where)->find();
          
        }
    }

    //http://localhost.web/wapi/public/index.php/im/index/create_user?_sign=XXXX&_timestamp=XXX&nikename=XXXX&pwd_im=XXX&mobile=XXXX
    protected function add_im($info = array()){
        $IM = new Imindex();
        $res = array(); 
        if(!empty($info)){
            $mobile = $info['mobile'];
            $res = $IM->create_user($info);
            if($res['code'] == 200){
                Db::name('y_med_app_members')->where('mobile',$mobile)->update(['status_im'=>1]); 
            }elseif($res->desc == "already register"){
                Db::name('y_med_app_members')->where('mobile',$mobile)->update(['status_im'=>1]); 
            }
        } 
    } 

    /**
    * 谣言终结 投票支持 or 反对
    * 链接localhost.web/wapi/public/index.php/app/index/add_vote
    * @param Post 参数
    * @param id        视频id
    * @param vote  1支持 2反对
    * @param uid       用户id
    * @return json 执行结果
    **/
    public function add_vote(){
        $params = input('');
        $uid = $this->uid;
        //检验参数值
        if (empty($uid) || empty($params['vid']) || empty($params['vote'])){
            return json(['code'=>0, 'msg'=>'参数值无效']);
        } 

        //用户是否有效  
        $user_info = Db::name('y_med_app_members')->where(['status'=>1, 'app_id'=>$uid])->find();
        if (empty($user_info)){
            return json(['code'=>0, 'msg'=>'用户不存在']);
        }
        //视频是否有效
        $video_info = Db::name('y_med_video')->where(['id'=>$params['vid'],'status'=>1, 'type'=>'谣言终结']);
        if (empty($video_info)){
            return json(['code'=>0, 'msg'=>'视频不属于谣言终结或已下架']);
        }

        //验证用户 一个谣言终结只能投票一次
        $is_vote = Db::name('y_med_app_vote')->where(['uid'=>$uid, 'vid'=>$params['vid']])->find();
        if(!empty($is_vote)){
            return json(['code'=>0, 'msg'=>'您已经参与过投票']);
        } 

        $vote = $params['vote'];
        if ($vote != 1 && $vote != 2){
            return json(['code'=>0, 'msg'=>'投票值参数不正确']);
        }
        //插入数据库
        $data = $params;
        $data['votetime'] = strtotime('now');
        $res_ins = Db::name('y_med_app_vote')->insert($data);

        //返回结果
        if ($res_ins === false){
            return json(['code'=>0, 'msg'=>'数据插入失败']);
        }

        return json(['code'=>1, 'msg'=>'投票成功']);
    }
}