<?php
namespace app\im\controller;  

use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
use app\im\lib\IMServer;

class Index extends Base
{ 
    public function __construct(){
        parent::__construct();    
    }

    /*
    * 创建群信息
    * http://localhost.web/wapi/public/index.php/im/index/create_group
    * @param POST 参数说明
    *    tname    必填 群名称 最大长度64字符 
    *    owner    必填 群主用户帐号最大长度32字符 
    *    members  必填 群成员 一次最多拉200个成员 
    *    msg      必填 邀请发送的文字  最大长度150字符
    *    announcement    群公告，最大长度1024字符  
    *    intro   群描述，最大长度512字符
    *    custom  第三方扩展 icon  群图标
         beinvitemode   被邀请人同意方式，0-需要同意(默认),1-不需要同意。其它返回414
    *    invitemode  谁可以邀请他人入群，0-管理员(默认),1-所有人。其它返回414
    *    uptinfomode 谁可以修改群资料，0-管理员(默认),1-所有人。其它返回414
    *    upcustommode    谁可以更新群自定义属性，0-管理员(默认),1-所有人。其它返回414
    */
    public function  create_group(){
        $cc = $this->decrypt();
        $params = input('post.');

        $im = new IMServer();
        if(empty($params['tname']) || empty($params['owner']) || empty($params['members']) ||empty($params['members']) || empty($params['msg']) ){
            return json_encode(['code'=> 0, 'msg'=>'参数不正确']);
            exit;
        }
        $data = $params;
        $data['members']      = json_encode($params['members']);   //群成员
        $data['magree']       = isset($params['$magree']) ? (int)$params['magree'] : 0;            
        $data['joinmode']     = isset($params['$joinmode']) ? (int)$params['$joinmode'] : 0; 
        $data['beinvitemode'] = isset($params['$beinvitemode']) ? $params['$beinvitemode'] : 0; 
        $data['invitemode']   = isset($params['$invitemode']) ? $params['$invitemode'] : 0;
        $data['uptinfomode']  = isset($params['$uptinfomode']) ? $params['$uptinfomode'] : 0; 
        $data['upcustommode'] = isset($params['$upcustommode']) ? $params['$upcustommode'] : 0;

        $res_data = $im->createGroup($data);
        
        return json_encode($res_data);        
    }

    /*
    * 获取IM用户信息
    * @param accids IM账号名称
    * http://localhost.web/wapi/public/index.php/im/index/get_user_info?accids=test
    */
    public function get_user_info(){
        //检测参数 
        $cc = $this->decrypt();
        $params = input('post.'); 
        if (empty($params['accids'])){
            return json_encode(['code'=>414, 'msg'=>'用户ID 为空！']);
            exit;
        }

        $im = new IMServer();
        $res_data = $im->getUinfos(array($params['accids']));
        
        return json_encode($res_data);
    }

    /*
    * 拉人入群
    * http://localhost.web/wapi/public/index.php/im/index/add_group?tid=672637468&owner=test&members[]=dhn(geturl)
    * param 参数说明
    *   tid  必填 群ID 最大长度128字符
    *   owner   群主用户帐号，最大长度32字符
    *   member  群群成员
    *   msg     邀请发送的文字
    *   attach  扩展字段
    *   magree   管理后台建群时，0不需要被邀请人同意加入群，1需要被邀请人同意才可以加入群。其它会返回414
    */
    public function add_group(){
        //检测参数
        $cc = $this->decrypt();
        $params = input('post.');

        $data = $params;
        $data['magree']   = isset($params['magree']) ? $params['magree'] : 0;
        $data['msg']      = isset($params['msg']) ? $params['msg'] : '欢迎入群';
        $data['members'] = json_encode($params['members']);

        if (empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
            exit;
        }

        $im = new IMServer();
        $res_data = $im->addIntoGroup($data);

        return json_encode($res_data);
    }

    //消息抄送
    public function message_copy(){
        $post_data = file_get_contents("php://input");
        $post_data = mb_convert_encoding($post_data, 'UTF-8', 'auto');

        $header_data = array();
        foreach (getallheaders() as $name => $value) {
            $header_data[$name] = $value;
        }
        $im = new IMServer();
        $app_key   = '';
        $md5_data  = '';
        $check_sum = '';
        $cur_time  = '';

        if (!empty($header_data)){
            $app_key   = $header_data['AppKey'];
            $md5_data  = $header_data['MD5'];
            $check_sum = $header_data['CheckSum'];
            $cur_time  = $header_data['CurTime'];
        }
        
        $verifyMD5 = md5($post_data);
        //md5值校验。判断传输中值是否发生改变
        if ($md5_data != $verifyMD5){
            echo json_encode(array('code'=>414));
            exit;   
        }

        //验证CheckSum
        $verifyChecksum = $im->getMessageCheckSum($md5_data, $cur_time);

        if ($check_sum != $verifyChecksum){
            echo json_decode(array('code'=>414));
            exit;
        }
        $content = $im->json_to_array($post_data);
        $content['base_data'] = stripslashes($post_data);

        if (!empty($content['eventType'])){
            Db::name('y_med_app_im_message_record')->insert($content);
        }
       
        echo json_encode(array('code'=>200));
    }

    /*创建用户
    * http://localhost.web/wapi/public/index.php/im/index/create_user?_sign=XXXX&_timestamp=XXX&nikename=XXXX&pwd_im=XXX&mobile=XXXX
    */
    public function create_user(){
        //检测参数
        //$cc = $this->decrypt();
        $params = input('get.');
        
        $im = new IMServer();
        $data = $params;
        $data['accid'] = $params['mobile'];
        $data['name']  = $params['nikename'];
        $data['token'] = $params['pwd_im'];
        $data['mobile'] = $params['mobile'];
        $res_data = $im->createUser($data); 

        return json_encode($res_data);
    }
}