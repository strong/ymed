<?php
namespace app\im\controller;  

use think\controller;
use think\Db;
use think\Request;
use think\Session;
use \think\Cache;
use app\common\controller\Base; 
use app\im\lib\IMServer;

class Imindex extends Base
{ 
    public function __construct(){
        parent::__construct();    
    }

    /*
    * 创建群信息
    * http://localhost.web/wapi/public/index.php/im/imindex/create_group
    * @param POST 参数说明
    *    tname    必填 群名称 最大长度64字符 
    *    owner    必填 群主用户帐号最大长度32字符 
    *    members  必填 群成员 一次最多拉200个成员 
    *    msg      必填 邀请发送的文字  最大长度150字符
    *    announcement    群公告，最大长度1024字符  
    *    intro   群描述，最大长度512字符
    *    custom  第三方扩展
    *    icon  群图标
    *    beinvitemode   被邀请人同意方式，0-需要同意(默认),1-不需要同意。其它返回414
    *    invitemode  谁可以邀请他人入群，0-管理员(默认),1-所有人。其它返回414
    *    uptinfomode 谁可以修改群资料，0-管理员(默认),1-所有人。其它返回414
    *    upcustommode    谁可以更新群自定义属性，0-管理员(默认),1-所有人。其它返回414
    */
    public function  create_group(){
        $cc = $this->decrypt();
        $params = input('post.');

        $im = new IMServer();
        if(empty($params['tname']) || empty($params['owner']) || empty($params['members']) || empty($params['msg']) ){
            return json_encode(['code'=> 414, 'msg'=>'参数不正确']);
            exit;
        }

        if (count($params['members']) > 200){
            return json_encode(['code'=> 414, 'msg'=>'群成员人数太多了，一次最多200人']);
        }

        $data = $params;
        $data['members']      = json_encode($params['members']);   //群成员
        $data['magree']       = isset($params['$magree']) ? (int)$params['magree'] : 0;            
        $data['joinmode']     = isset($params['$joinmode']) ? (int)$params['$joinmode'] : 0; 
        $data['beinvitemode'] = isset($params['$beinvitemode']) ? $params['$beinvitemode'] : 0; 
        $data['invitemode']   = isset($params['$invitemode']) ? $params['$invitemode'] : 0;
        $data['uptinfomode']  = isset($params['$uptinfomode']) ? $params['$uptinfomode'] : 0; 
        $data['upcustommode'] = isset($params['$upcustommode']) ? $params['$upcustommode'] : 0;

        $res_data = $im->createGroup($data);
        
        return json_encode($res_data);        
    }

    /**
    * 高级群基本信息修改
    * localhost.web/wapi/public/index.php/im/imindex/upd_group?tid=672637468&owner=test&intro=放假放假
    * @param POST 参数说明
    *   tid           群ID 必填
    *   tname         群名称，最大长度64字符
    *   owner         群主用户帐号，最大长度32字符 必填
    *   announcement  群公告，最大长度1024字符
    *   intro         群描述，最大长度512字符
    *   joinmode      群建好后，sdk操作时，0不用验证，1需要验证,2不允许任何人加入。其它返回414
    *   custom        自定义高级群扩展属性，,最大长度1024字符
    *   icon          群头像，最大长度1024字符
    *   beinvitemode  被邀请人同意方式，0-需要同意(默认),1-不需要同意。其它返回414
    *   invitemode    谁可以邀请他人入群，0-管理员(默认),1-所有人。其它返回414
    *   uptinfomode   谁可以修改群资料，0-管理员(默认),1-所有人。其它返回414
    *   upcustommode  谁可以更新群自定义属性，0-管理员(默认),1-所有人。其它返回414
    */
    public function upd_group(){
        $cc = $this->decrypt();
        $params = input('post.');

        if(empty($params['tid']) || empty($params['owner'])){
            return json_encode(['code'=> 414, 'msg'=>'参数不正确']);
            exit;
        }
        $im = new IMServer();
        $res_data = $im->updGroup($params);
        
        return json_encode($res_data);      
    }


    /*
    * 获取IM用户信息
    * @param accids IM账号名称
    * http://localhost.web/wapi/public/index.php/im/imindex/get_user_info?accids=test
    */
    public function get_user_info(){
        //检测参数 
        $cc = $this->decrypt();
        $params = input('post.'); 
        if (empty($params['accids'])){
            return json_encode(['code'=>414, 'msg'=>'用户ID 为空！']);
            exit;
        }

        $im = new IMServer();
        $res_data = $im->getUinfos(array($params['accids']));
        
        return json_encode($res_data);
    }

    /*
    * 拉人入群
    * http://localhost.web/wapi/public/index.php/im/imindex/add_group?tid=672637468&owner=test&members[]=dhn(geturl)
    * param 参数说明
    *   tid  必填 群ID 最大长度128字符
    *   owner   群主用户帐号，最大长度32字符
    *   member  群群成员
    *   msg     邀请发送的文字
    *   attach  扩展字段
    *   magree   管理后台建群时，0不需要被邀请人同意加入群，1需要被邀请人同意才可以加入群。其它会返回414
    */
    public function add_group(){
        //检测参数
        $cc = $this->decrypt();
        //$params = input('post.');
        $params = input('post.');
        if (empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
            exit;
        }

        if (count($params['members']) > 200){
            return json_encode(['code'=> 414, 'msg'=>'群成员人数太多了，一次最多200人']);
        }

        $data = $params;
        $data['magree']   = isset($params['magree']) ? $params['magree'] : 0;
        $data['msg']      = isset($params['msg']) ? $params['msg'] : '欢迎入群';
        $data['members'] = json_encode($params['members']);

        $im = new IMServer();
        $res_data = $im->addIntoGroup($data);

        return json_encode($res_data);
    }

    /*
    * 踢人出群 (需要提供群主accid以及要踢除人的accid)
    * tid 群ID 
    * owner 群主的帐号，最大长度32字符
    * members 移除人的账号集合 [aaa","bbb"] 一次最多操作200个accid
    * attach 扩展 可用于些踢出群原因
    */
    public function kick_out_group(){
        //检测参数
        $cc = $this->decrypt();
        $params = input('post.');

        if (empty($params['tid']) || empty($params['owner']) || empty($params['members']) ){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        
        $im = new IMServer();
        $res_data = $im->kick($params);
        return json_encode($res_data);
    }

    /**
    * 删除群
    * localhost.web/wapi/public/index.php/im/imindex/remove_group?tid=672637468&owner=test
    * tid    群ID
    * owner  群主用户帐号，最大长度32字符
    */
    public function remove_group(){
        //检测参数
        $cc = $this->decrypt();
        $params = input('post.');

        if (empty($params['tid']) || empty($params['owner'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        
        $im = new IMServer();
        $res_data = $im->removerGoup($params);
        return json_encode($res_data);
    }

    /**
    * 任命管理员 提升普通成员为群管理员，可以批量，但是一次添加最多不超过10个人。
    * localhost.web/wapi/public/index.php/im/imindex/add_manager?tid=672637468&owner=test&members[]=test
    * @param post 参数说明
        tid 群ID
        owner    群主用户帐号，最大长度32字符
        members  被任命为管理员的账号长度最大1024字符（一次添加最多10个管理员）
     */
    public function add_manager(){
        //检测参数
        $cc = $this->decrypt();
        $params = input('post.');
        if (empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        if (count($params['members']) > 10){
             return json_encode(['code'=>414, 'msg'=>'一次任命管理员不能超过10个']);
        }

        $im = new IMServer();
        $res_data = $im->addManager($params);

        return json_encode($res_data);
    }
 
    /**
    * 移除管理员 一次最多十个
    * localhost.web/wapi/public/index.php/im/imindex/remove_manager?tid=672637468&owner=test&members[]=test
    * @param post 参数说明
        tid 群ID
        owner    群主用户帐号，最大长度32字符
        members  被任命为管理员的账号长度最大1024字符（一次添加最多10个管理员）
     */
    public function remove_manager(){
        //检测参数
        $cc = $this->decrypt();
        $params = input('post.');
        if (empty($params['tid']) || empty($params['owner']) || empty($params['members'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        if (count($params['members']) > 10){
             return json_encode(['code'=>414, 'msg'=>'一次解除管理员不能超过10个']);
        }

        $im = new IMServer();
        $res_data = $im->removeManager($params);

        return json_encode($res_data);
    }
 

    /*
    * 查询指定群的详细信息（群信息+成员详细信息）
    * $tid     指定群id
    */
    public function get_group_info(){
        $gid = 
        $group_info = Db::name('y_med_app_im_group')->where(array('id'=>$id, 'status'=>1))->find();
        //检测参数
        $cc = $this->decrypt();
        $params = input('');

        if (empty($params['tid'])){
            return json_encode(['code'=>414, 'msg'=>'参数不正确！']);
        }
        
        $im = new IMServer();
        $res_data = $im->getGroupInfo($params);

        return json_encode($res_data);
    }

    //消息抄送
    public function message_copy(){
        $post_data = file_get_contents("php://input");
        $post_data = mb_convert_encoding($post_data, 'UTF-8', 'auto');

        $header_data = array();
        foreach (getallheaders() as $name => $value) {
            $header_data[$name] = $value;
        }
        $im = new IMServer();
        $app_key   = '';
        $md5_data  = '';
        $check_sum = '';
        $cur_time  = '';

        if (!empty($header_data)){
            $app_key   = $header_data['AppKey'];
            $md5_data  = $header_data['MD5'];
            $check_sum = $header_data['CheckSum'];
            $cur_time  = $header_data['CurTime'];
        }
        
        $verifyMD5 = md5($post_data);
        //md5值校验。判断传输中值是否发生改变
        if ($md5_data != $verifyMD5){
            echo json_encode(array('code'=>414));
            exit;   
        }

        //验证CheckSum
        $verifyChecksum = $im->getMessageCheckSum($md5_data, $cur_time);

        if ($check_sum != $verifyChecksum){
            echo json_decode(array('code'=>414));
            exit;
        }
        $content = $im->json_to_array($post_data);
        $content['base_data'] = stripslashes($post_data);

        if (!empty($content['eventType'])){
            Db::name('y_med_app_im_message_record')->insert($content);
        }
       
        echo json_encode(array('code'=>200));
    }

    /*创建用户
    * http://localhost.web/wapi/public/index.php/im/index/
    create_user?_sign=XXXX&_timestamp=XXX&nikename=XXXX&pwd_im=XXX&mobile=XXXX
    * nikename 用户昵称
    * pwd_im  密码
    * mobile 手机号  用作IM的账号
    */
    public function create_user($params = array()){ 
        if(empty($params)){
            //检测参数
            $cc = $this->decrypt();
            $params = input('post.');
        } 

        if (empty($params['mobile']) || empty($params['nikename']) || empty($params['pwd_im'])){
            return json_encode(array('code'=>414, 'msg'=>'参数不正确！'));
            exit;
        }
        $data = $params;
        //网易云通信ID，最大长度32字符 
        $data['accid'] = $params['mobile'];
        if (mb_strlen($data['accid']) > 32){
            return json_encode(array('code'=>405, 'msg'=>'账号参数长度过长'));
        }
        //网易云通信ID昵称，最大长度64字符，
        $data['name']  = $params['nikename'];
        if(mb_strlen($data['name']) > 64){
            return json_encode(array('code'=>405, 'msg'=>'昵称参数长度过长'));
        }
        //登录token值，最大长度128字符，
        $data['token'] = $params['pwd_im'];
        if(mb_strlen($data['token']) > 128){
            return json_encode(array('code'=>405, 'msg'=>'token参数长度过长'));
        }

        $data['mobile'] = $params['mobile'];
        $im = new IMServer();
        $res_data = $im->createUser($data); 

        return json_encode($res_data);
    }
}