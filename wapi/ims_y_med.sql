/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : yuntehu_db

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2018-05-29 09:06:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ims_y_med_advertisement
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_advertisement`;
CREATE TABLE `ims_y_med_advertisement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '广告id',
  `title` varchar(255) DEFAULT NULL COMMENT '广告名称',
  `img` text COMMENT '广告图片',
  `status` enum('启用','不启用') DEFAULT '启用' COMMENT '启用状态',
  `hid` int(11) unsigned DEFAULT '0' COMMENT '医院id',
  `uniacid` tinyint(4) unsigned DEFAULT '8' COMMENT '公众号',
  `locationid` tinyint(11) DEFAULT '0' COMMENT '位置id',
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_status_unid` (`id`,`status`,`uniacid`) USING BTREE,
  KEY `title_status_unid` (`title`,`status`,`uniacid`) USING BTREE,
  KEY `hid_status_unid` (`hid`,`status`,`uniacid`) USING BTREE,
  KEY `unid` (`uniacid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='广告表';

-- ----------------------------
-- Records of ims_y_med_advertisement
-- ----------------------------
INSERT INTO `ims_y_med_advertisement` VALUES ('2', '1111', '[{\"img\":\"images\\/8\\/2017\\/11\\/kKwE51k9z29hKeeLevekjh5B2k10EK.jpg\",\"url\":\"aaaaa\"},{\"img\":\"images\\/8\\/2017\\/11\\/OA1GaAwshsGZbwFC0WYbwW5bfYA9AF.jpg\",\"url\":\"bbbbbb\"},{\"img\":\"images\\/8\\/2017\\/11\\/SmKkgUv7mT3UY33wK37K3GyKANtk3u.png\",\"url\":\"cccccccc\"}]', '启用', '19', '8', '1', '0');
INSERT INTO `ims_y_med_advertisement` VALUES ('3', '22', '[{\"img\":\"images\\/8\\/2017\\/11\\/FzlXW14T331W63QWK6336B34LHwkSW.jpg\",\"url\":\"11\"},{\"img\":\"images\\/8\\/2017\\/11\\/SmKkgUv7mT3UY33wK37K3GyKANtk3u.png\",\"url\":\"22\"},{\"img\":\"images\\/8\\/2017\\/11\\/qVsdlqbrsd6d2D24dslOkrcO6qozBc.jpg\",\"url\":\"33\"}]', '启用', '11', '8', '2', '9');
INSERT INTO `ims_y_med_advertisement` VALUES ('4', '22', '[{\"img\":\"images\\/8\\/2017\\/11\\/FzlXW14T331W63QWK6336B34LHwkSW.jpg\",\"url\":\"11\"},{\"img\":\"images\\/8\\/2017\\/11\\/SmKkgUv7mT3UY33wK37K3GyKANtk3u.png\",\"url\":\"22\"},{\"img\":\"images\\/8\\/2017\\/11\\/qVsdlqbrsd6d2D24dslOkrcO6qozBc.jpg\",\"url\":\"33\"}]', '启用', '0', '8', '1', '9');

-- ----------------------------
-- Table structure for ims_y_med_advertising_position
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_advertising_position`;
CREATE TABLE `ims_y_med_advertising_position` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '位置说明',
  `location` varchar(255) NOT NULL COMMENT '广告位置',
  `status` tinyint(4) DEFAULT '0',
  `uniacid` tinyint(4) DEFAULT '8',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='广告位置';

-- ----------------------------
-- Records of ims_y_med_advertising_position
-- ----------------------------
INSERT INTO `ims_y_med_advertising_position` VALUES ('1', '首页轮播图', 'index_1', '1', '8');
INSERT INTO `ims_y_med_advertising_position` VALUES ('2', '开始听讲轮播图', 'video_1', '1', '8');
INSERT INTO `ims_y_med_advertising_position` VALUES ('3', '视频详情视频下面', 'video_list_1', '1', '8');

-- ----------------------------
-- Table structure for ims_y_med_category
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_category`;
CREATE TABLE `ims_y_med_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `parentid` int(10) unsigned NOT NULL,
  `ico` varchar(255) DEFAULT NULL,
  `sort` int(3) unsigned NOT NULL,
  `addtime` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '描述',
  `en_title` varchar(255) DEFAULT NULL COMMENT '英文描述',
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `uniacid` (`uniacid`,`parentid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COMMENT='分类疾病表';

-- ----------------------------
-- Records of ims_y_med_category
-- ----------------------------
INSERT INTO `ims_y_med_category` VALUES ('144', '8', '痘痘死', '61', '', '0', '1526874330', '', 'doudou', '');
INSERT INTO `ims_y_med_category` VALUES ('18', '8', '肾内科', '0', 'images/8/2017/07/YEjFfQwKwPEF86fE516fiE7E1IWQd7.png', '0', '1499837429', null, null, 'images/8/2017/11/xo71LhhR7Zhhb57W13hHE133Em7leB.jpg');
INSERT INTO `ims_y_med_category` VALUES ('19', '8', '妇产科', '0', 'images/8/2017/07/K56k2e6S6TzQB5jKTiEZyBJBEZTEB3.png', '7', '1499837482', null, null, 'images/8/2017/11/xo71LhhR7Zhhb57W13hHE133Em7leB.jpg');
INSERT INTO `ims_y_med_category` VALUES ('21', '8', '骨科', '0', 'images/8/2017/07/tlzxN94jLjYWaFJnxhxI8fwVe5FfwG.png', '9', '1499837534', '111111111111111111111111111111111111111234234234', 'guke', 'images/8/2017/11/xo71LhhR7Zhhb57W13hHE133Em7leB.jpg');
INSERT INTO `ims_y_med_category` VALUES ('23', '8', '内分泌科', '0', 'images/8/2017/07/gPL9x99xlxOK8RIxlrLji09rvJwNxX.png', '6', '1499837571', null, null, 'images/8/2017/11/xo71LhhR7Zhhb57W13hHE133Em7leB.jpg');
INSERT INTO `ims_y_med_category` VALUES ('24', '8', '肿瘤科', '0', 'images/8/2017/07/dde810eFkHPFFC80ydFPPyaKMFn81f.png', '6', '1499837588', null, null, 'images/8/2017/11/xo71LhhR7Zhhb57W13hHE133Em7leB.jpg');
INSERT INTO `ims_y_med_category` VALUES ('124', '8', '血管外科', '0', '', '5', '1508398766', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('57', '8', '中医', '0', 'images/8/2017/07/BaR5AjbXqhl0cdYEX0rEfEr6q9BqH2.png', '0', '1501207700', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('132', '8', '康复科', '0', '', '0', '1510032924', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('59', '8', '心内科', '0', 'images/8/2017/07/S4uHoIxFF30PZ20FFRuqRV2rZ40p3V.png', '0', '1501207740', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('60', '8', '耳鼻喉科', '0', 'images/8/2017/07/Ir8zrdOm9ZZmAcVrc83aaaV5iWZIZZ.png', '0', '1501207756', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('61', '8', '皮肤科', '0', 'images/8/2017/07/nHGLt83rnuqq3e8go3T7NqnNPF8uhN.png', '8', '1501222354', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('121', '8', '神经衰弱', '58', '', '0', '1508295127', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('129', '8', '肺癌', '127', '', '0', '1509968840', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('142', '8', '三骨科', '21', 'images/8/2018/05/LR6z6nE6vV2Sv9dgDDIGN9IvNr59Bv.png', '0', '1526365094', '111111111111111', 'three骨科', 'images/8/2017/11/xo71LhhR7Zhhb57W13hHE133Em7leB.jpg');
INSERT INTO `ims_y_med_category` VALUES ('135', '8', '血管外', '0', '', '0', '1515477452', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('136', '8', '医疗美容', '0', '', '0', '1515477509', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('140', '8', '康复科', '0', '', '0', '1510032924', null, null, null);
INSERT INTO `ims_y_med_category` VALUES ('141', '8', '咕咕疼', '21', 'images/8/2018/05/LR6z6nE6vV2Sv9dgDDIGN9IvNr59Bv.png', '0', '1526362304', '二级骨科测试', 'second gu ke', 'images/8/2017/11/xo71LhhR7Zhhb57W13hHE133Em7leB.jpg');
INSERT INTO `ims_y_med_category` VALUES ('143', '8', '2', '135', '4', '65', '1526365170', '5', '3', '4');

-- ----------------------------
-- Table structure for ims_y_med_collect
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_collect`;
CREATE TABLE `ims_y_med_collect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `vid` int(11) DEFAULT '0' COMMENT '视频id',
  `tid` int(11) unsigned DEFAULT '0' COMMENT '医师id',
  `ctype` enum('视频','讲师','栏目','疾病','评论','一次性订阅') NOT NULL COMMENT '1是视频 2是讲师,3是栏目,4是疾病,5是评论6一次性订阅',
  `addtime` int(11) NOT NULL,
  `eid` int(11) DEFAULT NULL COMMENT '评论id',
  `cid` varchar(255) DEFAULT '0',
  `colid` int(11) unsigned DEFAULT '0' COMMENT '栏目id',
  `uniacid` tinyint(4) unsigned NOT NULL DEFAULT '8',
  `hid` int(11) DEFAULT NULL COMMENT '一次性订阅',
  PRIMARY KEY (`id`),
  KEY `u_collecton` (`uid`,`tid`,`cid`,`colid`,`uniacid`,`ctype`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='用户关系表（点赞收藏关注）';

-- ----------------------------
-- Records of ims_y_med_collect
-- ----------------------------
INSERT INTO `ims_y_med_collect` VALUES ('1', '666', '0', null, '疾病', '1527303421', null, '141', null, '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('2', '666', '0', '0', '疾病', '1527040318', null, '144', null, '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('3', '666', '0', null, '栏目', '1527303421', null, null, '1', '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('4', '666', '5', null, '视频', '1527040318', null, null, null, '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('6', '520', '0', null, '栏目', '1527040318', null, null, '1', '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('29', '666', '0', '0', '疾病', '1527303421', null, '142', '0', '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('8', '520', '0', '1364', '讲师', '1527040318', null, null, null, '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('12', '520', '0', '1361', '讲师', '1527303421', null, null, null, '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('21', '666', '0', '0', '疾病', '1527303421', null, '132', '0', '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('20', '666', '0', '0', '疾病', '1527303421', null, '123', '0', '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('23', '666', '0', '0', '疾病', '1527303421', null, '312', '0', '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('25', '520', '5', '0', '视频', '1527303962', null, '0', '0', '8', null);
INSERT INTO `ims_y_med_collect` VALUES ('26', '666', '0', '0', '一次性订阅', '1527303962', null, '0', '0', '8', '1');
INSERT INTO `ims_y_med_collect` VALUES ('27', '520', '0', '0', '一次性订阅', '1527303962', null, '0', '0', '8', '2');

-- ----------------------------
-- Table structure for ims_y_med_column
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_column`;
CREATE TABLE `ims_y_med_column` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `img` varchar(200) NOT NULL COMMENT '栏目封面',
  `price` float(5,2) NOT NULL DEFAULT '0.00',
  `content` text COMMENT '栏目简介',
  `price_content` text COMMENT '收费简介',
  `subscribe` int(11) unsigned DEFAULT '0' COMMENT '订阅量',
  `status` enum('启用','不启用') NOT NULL DEFAULT '启用' COMMENT '是否启用 1启用 2不启用',
  `hid` int(11) unsigned DEFAULT '0' COMMENT '医院id',
  `uniacid` tinyint(4) unsigned NOT NULL DEFAULT '8',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `displayorder` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `title_un_sta` (`title`,`status`,`uniacid`,`hid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='栏目表';

-- ----------------------------
-- Records of ims_y_med_column
-- ----------------------------
INSERT INTO `ims_y_med_column` VALUES ('1', '回复规则', 'images/8/2018/05/Tn0E3eEt87EnNgmE6nEdM8O56gzOU3.jpg', '999.00', '1231231242173563265873268561328587326578326578365832756132785681275682365872165783258216587326857', '123123123123', '0', '启用', '24', '8', '1231231231', null);
INSERT INTO `ims_y_med_column` VALUES ('2', '1231232134132', 'images/8/2017/11/EN6N6RqydQSHhrs6Wx46KX6rHvy5hr.jpg', '1.00', '&lt;p style=&quot;margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;bjh-p&quot;&gt;&lt;span class=&quot;bjh-strong&quot; style=&quot;font-size: 18px; font-weight: 700;&quot;&gt;关于领克02，有些话一定要说在最前面。&lt;/span&gt;&lt;span class=&quot;bjh-br&quot;&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;&lt;div class=&quot;img-container&quot; style=&quot;margin-top: 30px; font-family: arial; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;img class=&quot;large&quot; data-loadfunc=&quot;0&quot; src=&quot;https://t10.baidu.com/it/u=2127371787,43254630&fm=173&app=25&f=JPEG?w=640&h=424&s=1E2FE30409D31EC64446E88E03007088&quot; data-loaded=&quot;0&quot; style=&quot;border: 0px; width: 537px; display: block;&quot;/&gt;&lt;/div&gt;&lt;p style=&quot;margin-top: 26px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;bjh-p&quot;&gt;“02”，从数字直观上看，它代表着领克品牌推出的第二款车型，同样是基于与沃尔沃联合开发的CMA基础模块架构打造。不过与领克01传统的SUV风格相比，领克02拥有更加酷炫的跨界风格，用一个时髦的词就是：轿跑SUV。从市场定位来说，领克02未来主要瞄准的是奔驰GLA、宝马X1把持的豪华紧凑型SUV市场。&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', '232323', '232323', '启用', '0', '8', '1231231231', '0');
INSERT INTO `ims_y_med_column` VALUES ('3', '甲亢疾病科普', 'images/8/2018/05/Tn0E3eEt87EnNgmE6nEdM8O56gzOU3.jpg', '0.00', '甲状腺患者的学习园地', '', '0', '启用', '0', '8', '1526980829', null);
INSERT INTO `ims_y_med_column` VALUES ('5', '测试栏目', 'images/8/2018/05/Tn0E3eEt87EnNgmE6nEdM8O56gzOU3.jpg', '123.00', '123', '123', '123', '启用', '0', '8', '1527140978', null);
INSERT INTO `ims_y_med_column` VALUES ('6', '栏目栏目', 'images/8/2017/11/qVsdlqbrsd6d2D24dslOkrcO6qozBc.jpg', '0.00', '&lt;p style=&quot;margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;bjh-p&quot;&gt;&lt;span class=&quot;bjh-strong&quot; style=&quot;font-size: 18px; font-weight: 700;&quot;&gt;关于领克02，有些话一定要说在最前面。&lt;/span&gt;&lt;span class=&quot;bjh-br&quot;&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;&lt;div class=&quot;img-container&quot; style=&quot;margin-top: 30px; font-family: arial; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;img class=&quot;large&quot; data-loadfunc=&quot;0&quot; src=&quot;https://t10.baidu.com/it/u=2127371787,43254630&fm=173&app=25&f=JPEG?w=640&h=424&s=1E2FE30409D31EC64446E88E03007088&quot; data-loaded=&quot;0&quot; style=&quot;border: 0px; width: 537px; display: block;&quot;/&gt;&lt;/div&gt;&lt;p style=&quot;margin-top: 26px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;bjh-p&quot;&gt;“02”，从数字直观上看，它代表着领克品牌推出的第二款车型，同样是基于与沃尔沃联合开发的CMA基础模块架构打造。不过与领克01传统的SUV风格相比，领克02拥有更加酷炫的跨界风格，用一个时髦的词就是：轿跑SUV。从市场定位来说，领克02未来主要瞄准的是奔驰GLA、宝马X1把持的豪华紧凑型SUV市场。&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', '123', '111', '启用', '0', '8', '1527141160', '11');

-- ----------------------------
-- Table structure for ims_y_med_cooperation
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_cooperation`;
CREATE TABLE `ims_y_med_cooperation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL COMMENT '性别',
  `age` int(11) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL COMMENT '职称',
  `hospital` varchar(50) DEFAULT NULL COMMENT '医院',
  `department` varchar(50) DEFAULT NULL COMMENT '科室',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `cooperation` text COMMENT '合作意向',
  `type` tinyint(4) DEFAULT '1' COMMENT '1合作意向 2意见反馈',
  `hid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ims_y_med_cooperation
-- ----------------------------
INSERT INTO `ims_y_med_cooperation` VALUES ('1', 'weafa', '男', '0', 'ewafawefwae', 'wefawef', 'awefaew', 'weafawef', 'waefaewf', null, '17037', null, '1', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('2', 'awef', '男', '234', 'ewfewf', 'efwef', 'wefewa', '2343294234', '23532532@qq.com', null, '17037', null, '1', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('3', '张三', '男', null, 'aefwae', null, null, '12234512344', '1310227465@qq.com', null, '0', null, '1', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('4', '张三', '男', '67', 'aefwae', null, null, '12234512344', '1310227465@qq.com', null, '0', '你是个傻逼', '1', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('5', '张三', '男', '67', 'aefwae', '医院', 'awefeaw', '12234512344', '1310227465@qq.com', null, '0', '你是个傻逼', '1', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('6', '张三', '男', '67', 'aefwae', '医院', 'awefeaw', '12234512344', '1310227465@qq.com', null, '520', '你是个傻逼', '1', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('7', '张三', '男', '67', 'aefwae', '医院', 'awefeaw', '12234512344', '1310227465@qq.com', null, '520', '你是个傻逼', '1', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('8', null, null, null, null, null, null, null, null, null, '520', '你是个傻逼', '2', null);
INSERT INTO `ims_y_med_cooperation` VALUES ('9', '张三', '男', '67', 'aefwae', '医院', 'awefeaw', '12234512344', '1310227465@qq.com', null, '520', '你是个傻逼', '1', null);

-- ----------------------------
-- Table structure for ims_y_med_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_evaluate`;
CREATE TABLE `ims_y_med_evaluate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `uniacid` int(11) NOT NULL,
  `openid` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `grade` tinyint(1) NOT NULL,
  `content` text NOT NULL,
  `addtime` int(10) NOT NULL,
  `vid` int(11) NOT NULL DEFAULT '0' COMMENT '单个视频id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '评论的父级id',
  `status` int(11) DEFAULT '1' COMMENT '0删除 1是待审核 2是未通过 3是已通过',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=168 DEFAULT CHARSET=utf8 COMMENT='评论表';

-- ----------------------------
-- Records of ims_y_med_evaluate
-- ----------------------------
INSERT INTO `ims_y_med_evaluate` VALUES ('1', '4', 'oeCITwqzASGYmhKpSaNPcrD0m0G4', '11', '希金影业', '1', '感谢老师', '1498121509', '5', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('2', '4', 'oeCITwlJQuBKsAGFermV8DsQj5vc', '18', '王辉', '1', '', '1498726150', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('3', '4', 'ok0umxAtLnqq-Gx3mnA2NkpKnKuo', '42', '王蒙', '1', '强', '1498997032', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('4', '4', 'ok0umxGMTtuX9cLclC5DqxqrwG6c', '33', '超', '1', '今天看了老师的讲课这是受益匪浅呀，喜欢以后都有这样的教育视频。', '1499155397', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('5', '4', 'ok0umxDkTzHeewt_evqMjsG8fxnk', '56', '云特护-李老师', '1', '讲的特别好，听完收益很多^_^', '1499155601', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('6', '4', 'ok0umxGMTtuX9cLclC5DqxqrwG6c', '33', '超', '1', '微课堂很有针对性，里面讲的内容我很受用，孙医生讲课的方式我非常喜欢。', '1499155624', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('7', '4', 'ok0umxNGHCXIPZsxHjdwqDqwGTFo', '40', '陈博', '1', '受益匪浅，有的时候我们患者还有家属是需要这样系统的学习病症的，久病成医，同病魔抗争是持久战', '1499155668', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('8', '4', 'ok0umxDkTzHeewt_evqMjsG8fxnk', '56', '云特护-李老师', '1', '感谢给我们提供视频的人，这视频太及时了。。。', '1499155762', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('9', '4', 'ok0umxNGHCXIPZsxHjdwqDqwGTFo', '40', '陈博', '1', '形式新颖，把重点我们要听的内容摘取出来，内容也都是我们需要的，感觉不错，希望专家可以多录一些视频', '1499155868', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('10', '4', 'ok0umxCje_15QuiVZzvsidSB-TCc', '55', '韩不困', '1', '了解到了关心的问题，觉得这种方式特别好，很喜欢', '1499156028', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('11', '4', 'ok0umxCje_15QuiVZzvsidSB-TCc', '55', '韩不困', '1', '今天看了微课堂受益匪浅呀，希望以后都有这样的教育视频。', '1499156508', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('12', '4', 'ok0umxGRqFOcpslsoo7oyvJYLGds', '46', '六 一', '1', '我觉得挺有帮助的。从什么都不懂到能有一点基础，这样心里就不会因为不懂而慌张。挺好的～', '1499156537', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('13', '4', 'ok0umxGRqFOcpslsoo7oyvJYLGds', '46', '六 一', '1', '非常清楚的知道了重点是什么。特别好的一个平台，请专业的让人放心的教授来教我们。非常赞', '1499156774', '0', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('22', '8', 'ok0umxGRqFOcpslsoo7oyvJYLGds', '104', '六 一', '1', '挺好', '1502160031', '0', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('24', '8', 'ok0umxP08RL-8940iENao1i75OUE', '127', '刘武帅', '0', '是打发斯蒂芬', '1504838402', '17', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('141', '8', 'ok0umxElUdtQYTzUSL4kUwthzsVY', '102', '高雅', '0', '', '1510652564', '125', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('26', '8', 'ok0umxP08RL-8940iENao1i75OUE', '127', '刘武帅', '0', '测试评论刷新', '1504852675', '17', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('27', '8', 'ok0umxP08RL-8940iENao1i75OUE', '127', '刘武帅', '0', '两秒刷新', '1504852797', '17', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('28', '8', 'ok0umxP08RL-8940iENao1i75OUE', '127', '刘武帅', '0', '俩买地方', '1504852857', '17', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('147', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '', '0', '凤凰花', '1510668403', '127', '133', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('142', '8', 'ok0umxP08RL-8940iENao1i75OUE', '1375', '刘武帅', '0', '但事实上', '1510652636', '127', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('143', '8', 'ok0umxCzdnCFUNctx6Lv1xqu_lTc', '796', '丶丶丶Glucky', '0', '   ', '1510652791', '127', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('32', '8', 'ok0umxCzdnCFUNctx6Lv1xqu_lTc', '505', 'Glucky', '0', '讲的很好', '1505878886', '85', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('33', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '266', '宝', '0', '多高飞得更高', '1505997053', '6', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('34', '8', 'ok0umxElUdtQYTzUSL4kUwthzsVY', '102', '高雅', '0', '讲的真好', '1505997058', '6', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('35', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '266', '宝', '0', '看了视频，对我帮助很大', '1506003757', '8', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('144', '8', 'ok0umxCzdnCFUNctx6Lv1xqu_lTc', '796', '丶丶丶Glucky', '0', '', '1510652864', '127', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('145', '8', 'ok0umxGMTtuX9cLclC5DqxqrwG6c', '1311', '', '0', '很不错', '1510666945', '167', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('146', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '', '0', '每天都有新变内容', '1510668306', '127', '133', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('41', '8', 'ok0umxGMTtuX9cLclC5DqxqrwG6c', '99', '超', '0', '不错', '1506095466', '85', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('56', '12', 'oj048wSUZZTKt54VCh29zY3o_EyM', '244', '*彼岸&花開*', '0', '讲的很好，很有用', '1506692627', '82', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('57', '12', 'oj048wSUZZTKt54VCh29zY3o_EyM', '244', '*彼岸&花開*', '0', '讲的特别好，很实用', '1506692716', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('58', '12', 'oj048wSUZZTKt54VCh29zY3o_EyM', '244', '*彼岸&花開*', '0', '讲的很好，很专业', '1506692876', '54', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('45', '12', 'oj048waup3Fu3xlhktZvGYvB8QQ4', '282', '高雅', '0', '看来窗口期真的很重要', '1506687808', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('46', '12', 'oj048wfpol5AiN1oVY1zjaZGy-1A', '445', '六 一', '0', ' 听了感觉还挺有用的 ', '1506688182', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('47', '12', 'oj048wRE0BOKuCQtmx5wCPgZzuHY', '270', '八月初六', '0', '讲的深入浅出听完很有帮助，希望能够经常看到这种猎鹰的视频', '1506688240', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('48', '12', 'oj048wfpol5AiN1oVY1zjaZGy-1A', '445', '六 一', '0', '好得有个方向了听了这个 决定再看看别的', '1506688255', '54', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('49', '12', 'oj048wfpol5AiN1oVY1zjaZGy-1A', '445', '六 一', '0', '正好家里人关节最近不舒服… 挺不错', '1506688311', '20', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('50', '12', 'oj048wcVchPAmYdpgEkyVLp4uX4w', '284', '王蒙', '0', '挺不错的，很有帮助。', '1506688318', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('51', '12', 'oj048wfpol5AiN1oVY1zjaZGy-1A', '445', '六 一', '0', '嗯 一系列听下来感觉上了个讲座课一样 值得保存', '1506688375', '82', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('52', '12', 'oj048wfWW3zQsZSxv0-gEBy1yn_A', '405', '王辉', '0', '很实用的讲解，对于患者来说通俗易懂，手动点赞', '1506688400', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('53', '12', 'oj048wTesPH6c076eeBfW6OxRYEc', '238', '超', '0', '视频讲的太有教育作用了，我看了之后特别受用，尤其是视频做的特别到位，市面上很少有这么好的视频。', '1506689711', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('54', '12', 'oj048wTesPH6c076eeBfW6OxRYEc', '238', '超', '0', '这个视频讲的太专业了，给医生点赞', '1506689779', '54', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('55', '12', 'oj048wTesPH6c076eeBfW6OxRYEc', '238', '超', '0', '值得观看的视频，真值得收藏', '1506689831', '82', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('59', '12', 'oj048wSUZZTKt54VCh29zY3o_EyM', '244', '*彼岸&花開*', '0', '原来术后有这么多注意事项啊！涨知识了', '1506693006', '20', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('60', '12', 'oj048wZtmk4gPTJiVBzgL4WK5fxQ', '323', '刘武帅', '0', '讲的不错，呱唧呱唧。', '1506761627', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('61', '12', 'oj048wfg7sfLxdC3oX1xC83J7o4M', '576', '二月', '0', '医生术后有什么忌口的吗', '1506761809', '55', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('62', '8', 'ok0umxKT8AeYYrDUf66i2Oq0yGWw', '704', '如向日葵般的花季少女', '0', '你好', '1507442458', '101', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('63', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '266', '宝', '0', '刚吃过早饭', '1507634330', '102', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('64', '7', '99', '99', '超', '0', '很不错', '1507807516', '88', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('65', '7', '99', '99', '超', '0', '不错', '1507807684', '29', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('66', '7', '99', '99', '超', '0', '对对对', '1507808080', '88', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('67', '7', '99', '99', '超', '0', 'r', '1507808175', '88', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('68', '7', '826', '826', '88975', '0', '讲的很好', '1507860152', '29', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('165', '8', 'ok0umxCJjEFkUz8pWj9PR9uEdy4c', '8101', '行者', '0', '不错不错', '1511246278', '125', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('164', '8', 'ok0umxCJjEFkUz8pWj9PR9uEdy4c', '8101', '行者', '0', '很不错', '1511244527', '125', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('148', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '', '0', '属蛇的人，不易长唑疮', '1510668506', '127', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('149', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '', '0', '宝宝们儿', '1510669047', '134', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('150', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '宝', '0', '得关', '1510710367', '165', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('151', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '宝', '0', '我是类风湿', '1510711519', '166', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('108', '8', 'ok0umxPsfZgbF2lK51YOr5OUyTjU', '1165', 'CH-Jie', '0', '怎么治疗面瘫', '1509624577', '24', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('152', '8', 'ok0umxM6hnp6hbtTVKDyKajxPzFI', '347', '云云', '0', '视频太短了', '1510716092', '159', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('111', '8', 'ok0umxGMTtuX9cLclC5DqxqrwG6c', '99', '超', '0', '视频不错呀', '1509755745', '70', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('112', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '266', '宝', '0', '赶紧去', '1509759491', '100', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('113', '8', 'ok0umxCnyICoBvAJ85mngfFyW5ic', '882', 'Simon', '0', '看看', '1509803271', '100', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('114', '8', 'ok0umxCZWn2d67A0V_SyPykIEnik', '103', '见一', '0', '原来是这样！', '1509858335', '100', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('115', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '266', '宝', '0', '好', '1510023994', '100', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('118', '8', 'ok0umxNGHCXIPZsxHjdwqDqwGTFo', '101', '陈博', '0', '谢谢王老师！', '1510049086', '100', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('166', '8', 'ok0umxCJjEFkUz8pWj9PR9uEdy4c', '8101', '行者', '0', '不错', '1511256200', '100', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('120', '8', 'ok0umxHMzwulvyFI_pmSm5LhuV7s', '1285', 'yuntehu002', '0', '视频很不错', '1510053411', '123', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('121', '8', 'ok0umxP08RL-8940iENao1i75OUE', '1268', '刘武帅', '0', '不错哦！', '1510054741', '135', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('122', '8', 'ok0umxHMzwulvyFI_pmSm5LhuV7s', '1285', 'yuntehu002', '0', '视频不错', '1510054760', '138', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('127', '8', 'ok0umxElUdtQYTzUSL4kUwthzsVY', '102', '高雅', '0', '太有用了！', '1510056199', '133', '114', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('130', '8', 'ok0umxJgfPngqJRd0OtIWCT3b2M0', '720', '丁俊强1302008', '0', '点击查看表情', '1510060899', '125', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('131', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1286', '宝', '0', '糖尿病不好志', '1510296292', '137', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('162', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '宝', '0', '新生儿视频的流量大', '1511241243', '138', '0', '1');
INSERT INTO `ims_y_med_evaluate` VALUES ('133', '8', 'ok0umxNN4paTkkwuYsrvNDXOguz4', '1188', '赵达治', '0', '有点意思', '1510320556', '127', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('134', '8', 'ok0umxGMTtuX9cLclC5DqxqrwG6c', '1311', '', '0', '不错', '1510326797', '141', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('135', '8', 'ok0umxElUdtQYTzUSL4kUwthzsVY', '102', '高雅', '0', '感觉都快变成周老师的粉丝了！哈哈哈', '1510326836', '137', '0', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('161', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '宝', '0', '准妈妈，为了孩子，要牺牲自己，难得', '1510795372', '148', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('159', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '宝', '0', '骨关', '1510730983', '166', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('160', '8', 'ok0umxH7SLeOn7FC-Bm70luwumuE', '1337', '宝', '0', '化疗，很辛苦的', '1510794190', '171', '0', '2');
INSERT INTO `ims_y_med_evaluate` VALUES ('140', '8', 'ok0umxM6RN9sJadw3ERJnxmMHN24', '1335', '元晋', '0', '你好', '1510327768', '137', '135', '3');
INSERT INTO `ims_y_med_evaluate` VALUES ('167', '0', '', '0', '', '0', '123123123', '1527240989', '11', '0', '3');

-- ----------------------------
-- Table structure for ims_y_med_group
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_group`;
CREATE TABLE `ims_y_med_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '群id',
  `group_name` varchar(100) NOT NULL COMMENT '群名称',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '现有人数',
  `content` varchar(250) NOT NULL COMMENT '简介',
  `qr_img` varchar(250) NOT NULL COMMENT '二维码',
  `max` int(11) NOT NULL DEFAULT '10' COMMENT '最大人数',
  `uniacid` tinyint(1) unsigned NOT NULL DEFAULT '8',
  `cid` int(11) unsigned DEFAULT NULL COMMENT '疾病id',
  `img` varchar(255) DEFAULT NULL,
  `status` enum('启用','不启用','已满') NOT NULL DEFAULT '启用' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='群组表';

-- ----------------------------
-- Records of ims_y_med_group
-- ----------------------------
INSERT INTO `ims_y_med_group` VALUES ('1', '群1', '50', '加热管家偶尔几个哦哦我瑞吉欧', 'images/8/2017/11/FjV7zyzYAvlt3bVfY1JzzH3V6zY88o.png', '500', '8', '141', 'images/8/2017/11/qVsdlqbrsd6d2D24dslOkrcO6qozBc.jpg', '启用');
INSERT INTO `ims_y_med_group` VALUES ('3', '骨科群', '20', '骨科交流群', 'images/8/2017/11/Aye1iEQEJy5Ei6ryswhR199h91yEfy.png', '200', '8', '19', 'images/8/2018/05/LR6z6nE6vV2Sv9dgDDIGN9IvNr59Bv.png', '启用');

-- ----------------------------
-- Table structure for ims_y_med_history
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_history`;
CREATE TABLE `ims_y_med_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '浏览视频id',
  `uniacid` int(11) NOT NULL DEFAULT '8',
  `uid` int(11) NOT NULL,
  `vid` int(11) NOT NULL,
  `addtime` int(11) NOT NULL,
  `colid` int(11) DEFAULT '0' COMMENT '栏目id',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `lessonid` (`vid`)
) ENGINE=MyISAM AUTO_INCREMENT=1702 DEFAULT CHARSET=utf8 COMMENT='视频浏览表';

-- ----------------------------
-- Records of ims_y_med_history
-- ----------------------------
INSERT INTO `ims_y_med_history` VALUES ('1', '8', '520', '5', '1527230041', '6');
INSERT INTO `ims_y_med_history` VALUES ('2', '8', '520', '7', '0', '6');

-- ----------------------------
-- Table structure for ims_y_med_log_message
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_log_message`;
CREATE TABLE `ims_y_med_log_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `err` varchar(255) DEFAULT NULL,
  `content` text,
  `status` tinyint(4) DEFAULT NULL,
  `sendtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='发送消息日志表';

-- ----------------------------
-- Records of ims_y_med_log_message
-- ----------------------------
INSERT INTO `ims_y_med_log_message` VALUES ('2', null, 'ok', '{\"touser\":\"ok0umxE3pp5EyXhyKEZEmuFeLxg8\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u7532\\u4ea2\\u60a3\\u8005\\u7a81\\u773c\\u600e\\u4e48\\u6cbb\\uff1f\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:05:24');
INSERT INTO `ims_y_med_log_message` VALUES ('3', null, 'ok', '{\"touser\":\"ok0umxG27hieIhPhsm8DaHSStUbU\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u7532\\u4ea2\\u60a3\\u8005\\u7a81\\u773c\\u600e\\u4e48\\u6cbb\\uff1f\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:05:25');
INSERT INTO `ims_y_med_log_message` VALUES ('4', null, 'ok', '{\"touser\":\"ok0umxE3pp5EyXhyKEZEmuFeLxg8\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u4f8b\\u5982 \\u7b2c\\u4e00\\u8282\\uff1a\\u521d\\u6b65\\u8ba4\\u8bc6AJAX\\u30011-1 \\u521d\\u6b65\\u8ba4\\u8bc6AJAX\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:05:26');
INSERT INTO `ims_y_med_log_message` VALUES ('5', null, 'ok', '{\"touser\":\"ok0umxG27hieIhPhsm8DaHSStUbU\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u4f8b\\u5982 \\u7b2c\\u4e00\\u8282\\uff1a\\u521d\\u6b65\\u8ba4\\u8bc6AJAX\\u30011-1 \\u521d\\u6b65\\u8ba4\\u8bc6AJAX\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:05:26');
INSERT INTO `ims_y_med_log_message` VALUES ('6', null, 'ok', '{\"touser\":\"ok0umxE3pp5EyXhyKEZEmuFeLxg8\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u7532\\u4ea2\\u60a3\\u8005\\u7a81\\u773c\\u600e\\u4e48\\u6cbb\\uff1f\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:38:44');
INSERT INTO `ims_y_med_log_message` VALUES ('7', null, 'ok', '{\"touser\":\"ok0umxG27hieIhPhsm8DaHSStUbU\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u7532\\u4ea2\\u60a3\\u8005\\u7a81\\u773c\\u600e\\u4e48\\u6cbb\\uff1f\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:38:45');
INSERT INTO `ims_y_med_log_message` VALUES ('8', null, 'ok', '{\"touser\":\"ok0umxE3pp5EyXhyKEZEmuFeLxg8\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u4f8b\\u5982 \\u7b2c\\u4e00\\u8282\\uff1a\\u521d\\u6b65\\u8ba4\\u8bc6AJAX\\u30011-1 \\u521d\\u6b65\\u8ba4\\u8bc6AJAX\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:38:45');
INSERT INTO `ims_y_med_log_message` VALUES ('9', null, 'ok', '{\"touser\":\"ok0umxG27hieIhPhsm8DaHSStUbU\",\"template_id\":\"zRPfPx2mg4LSZNq_m45zj6b-dtzfW18GfTV0CZrgdso\",\"url\":\"http:\\/\\/wechat.yuntehu.com\\/v\\/index.php\\/index\\/video\\/details1\\/i\\/8\\/sonid\\/1132.html\",\"topcolor\":\"#FF0000\",\"data\":{\"first\":{\"value\":\"\\u4eb2\\u7231\\u7684\\u5b66\\u5458\\uff0c\\u5b66\\u4e60\\u65f6\\u95f4\\u5230\\u4e86\\uff0c\\u7ee7\\u7eed\\u6309\\u8ba1\\u5212\\u5faa\\u5e8f\\u6e10\\u8fdb\\u5730\\u638c\\u63e1\\u63a7\\u7cd6\\u77e5\\u8bc6\\u5427\\uff01\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"\\u4f8b\\u5982 \\u7b2c\\u4e00\\u8282\\uff1a\\u521d\\u6b65\\u8ba4\\u8bc6AJAX\\u30011-1 \\u521d\\u6b65\\u8ba4\\u8bc6AJAX\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"2018\\u5e7405\\u670828\\u65e5\",\"color\":\"#173177\"},\"remark\":{\"value\":\"\\u6b22\\u8fce\\u6536\\u770b\\u75be\\u75c5\\u8bfe\\u5802\\uff0c\\u66f4\\u591a\\u7cbe\\u5f69\\u5185\\u5bb9\\u8425\\u517b\\u4e13\\u5bb6\\u544a\\u8bc9\\u60a8!\",\"color\":\"#173177\"}}}', '1', '2018-05-28 14:38:46');

-- ----------------------------
-- Table structure for ims_y_med_question
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_question`;
CREATE TABLE `ims_y_med_question` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `cid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '疾病id',
  `content` varchar(255) CHARACTER SET utf8 NOT NULL,
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  `status` enum('未回答','已回答') NOT NULL DEFAULT '未回答' COMMENT '1 未回答 2已回答',
  `age` tinyint(3) unsigned DEFAULT '0',
  `sex` enum('男','女') DEFAULT '男' COMMENT '性别 1男 2女',
  `uniacid` tinyint(1) unsigned NOT NULL DEFAULT '8',
  `hid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `uid_unionid` (`uid`,`uniacid`,`status`) USING BTREE,
  KEY `cid_unionid` (`cid`,`status`,`uniacid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COMMENT='提问表';

-- ----------------------------
-- Records of ims_y_med_question
-- ----------------------------
INSERT INTO `ims_y_med_question` VALUES ('1', '520', '1', 'afd', '1526809094', '未回答', '1', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('2', '520', '1', 'afd', '1526865152', '未回答', '1', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('3', '520', '18', '1', '1526868247', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('4', '520', '18', '1', '1526868253', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('5', '520', '18', '1', '1526868357', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('6', '520', '18', '1', '1526868423', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('7', '520', '18', '1', '1526868606', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('8', '520', '18', '1', '1526868743', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('9', '520', '18', 'wefkopkepf', '1526869036', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('10', '520', '18', '1', '1526869553', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('11', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526869574', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('12', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526869615', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('13', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526869846', '未回答', '2', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('14', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526870989', '未回答', '2', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('15', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526871109', '未回答', '2', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('16', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526871175', '未回答', '2', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('17', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526871250', '未回答', '2', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('18', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526871295', '未回答', '0', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('19', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526872525', '未回答', '1', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('20', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526872581', '未回答', '1', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('21', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526872604', '未回答', '0', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('22', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526872839', '未回答', '1', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('23', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526872979', '未回答', '1', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('24', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526872991', '未回答', '0', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('25', '520', '18', '那个我姐夫呢我我我莫非我奇偶if金额我飞', '1526873162', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('26', '520', '18', '安静而我if金额', '1527043345', '', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('27', '520', '18', '安静而我if金额', '1527043361', '', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('28', '520', '18', '安静而我if金额', '1527043386', '', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('29', '520', '18', '安静而我if金额', '1527043423', '', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('30', '520', '18', '安静而我if金额', '1527043445', '已回答', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('31', '520', '18', '安静而我if金额', '1527043476', '已回答', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('32', '520', '18', '安静而我if金额', '1527043509', '已回答', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('33', '520', '18', '安静而我if金额', '1527043615', '已回答', '24', '', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('34', '520', '18', '安静而我if金额', '1527043653', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('35', '520', '18', '安静而我if金额', '1527043763', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('36', '520', '18', '安静而我if金额', '1527043816', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('37', '520', '18', '安静而我if金额', '1527043839', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('38', '520', '18', '安静而我if金额', '1527043860', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('39', '520', '18', '安静而我if金额', '1527043881', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('40', '520', '18', '安静而我if金额', '1527043934', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('41', '520', '18', '安静而我if金额', '1527043946', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('42', '520', '18', '安静而我if金额', '1527043983', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('43', '520', '18', '安静而我if金额', '1527044456', '已回答', '24', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('44', '520', '18', 'anefiowj', '1527213748', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('45', '520', '18', 'anefiowj', '1527213757', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('46', '520', '18', 'anefiowj', '1527213767', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('47', '520', '18', 'anefiowj', '1527213792', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('48', '520', '18', 'anefiowj', '1527213855', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('49', '520', '18', 'anefiowj', '1527213875', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('50', '520', '18', 'anefiowj', '1527213941', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('51', '520', '18', 'anefiowj', '1527213966', '未回答', '12', '男', '8', '0');
INSERT INTO `ims_y_med_question` VALUES ('52', '520', '18', 'anefiowj', '1527214399', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('53', '520', '18', 'anefiowj', '1527214446', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('54', '520', '18', 'anefiowj', '1527214460', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('55', '520', '18', 'anefiowj', '1527214521', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('56', '520', '18', 'anefiowj', '1527214553', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('57', '520', '18', 'anefiowj', '1527214596', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('58', '520', '18', 'anefiowj', '1527214625', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('59', '520', '18', 'anefiowj', '1527214677', '未回答', '12', '男', '8', '4');
INSERT INTO `ims_y_med_question` VALUES ('60', '520', '18', 'anefiowj', '1527216333', '未回答', '12', '男', '8', '4');

-- ----------------------------
-- Table structure for ims_y_med_relation
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_relation`;
CREATE TABLE `ims_y_med_relation` (
  `colid` int(11) DEFAULT NULL COMMENT '栏目id',
  `vid` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  `hid` int(11) DEFAULT NULL,
  `type` tinyint(11) DEFAULT NULL COMMENT '1是视频，2是首页分类，3是医生，4是医院 5栏目',
  KEY `cid_uid` (`cid`,`colid`,`type`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='关系表';

-- ----------------------------
-- Records of ims_y_med_relation
-- ----------------------------
INSERT INTO `ims_y_med_relation` VALUES ('6', '14', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('6', '12', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('6', '6', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, null, '2', null, null);
INSERT INTO `ims_y_med_relation` VALUES ('6', '5', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('2', null, null, null, '14', '4');
INSERT INTO `ims_y_med_relation` VALUES ('2', null, null, null, '0', '4');
INSERT INTO `ims_y_med_relation` VALUES ('2', null, '143', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('2', null, '129', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('2', null, '18', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('6', '8', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('5', null, '18', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, null, null, '4', '4');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, '132', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, '24', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, '19', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, '18', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, null, null, '25', '4');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, null, null, '14', '4');
INSERT INTO `ims_y_med_relation` VALUES ('6', null, null, null, '0', '4');
INSERT INTO `ims_y_med_relation` VALUES ('6', '9', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('6', '10', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('2', '8', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('6', '7', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('3', '13', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('6', '11', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('3', null, null, null, '0', '4');
INSERT INTO `ims_y_med_relation` VALUES ('3', '7', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('5', '7', null, null, null, '1');
INSERT INTO `ims_y_med_relation` VALUES ('5', null, null, null, '32', '4');
INSERT INTO `ims_y_med_relation` VALUES ('5', null, '141', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('5', null, '121', null, null, '2');
INSERT INTO `ims_y_med_relation` VALUES ('5', null, null, null, '35', '4');
INSERT INTO `ims_y_med_relation` VALUES ('5', null, null, null, '6', '4');

-- ----------------------------
-- Table structure for ims_y_med_send_message
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_send_message`;
CREATE TABLE `ims_y_med_send_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '状态（1已发送，2发送失败，默认0）',
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ims_y_med_send_message
-- ----------------------------
INSERT INTO `ims_y_med_send_message` VALUES ('3', '{\"vid\":8,\"keyword1\":\"\\u7532\\u4ea2\\u60a3\\u8005\\u7a81\\u773c\\u600e\\u4e48\\u6cbb\\uff1f\",\"keyword2\":\"2018\\u5e7405\\u670828\\u65e5\"}', '1', '2018-05-28 09:55:06');
INSERT INTO `ims_y_med_send_message` VALUES ('2', '{\"vid\":7,\"keyword1\":\"\\u4f8b\\u5982 \\u7b2c\\u4e00\\u8282\\uff1a\\u521d\\u6b65\\u8ba4\\u8bc6AJAX\\u30011-1 \\u521d\\u6b65\\u8ba4\\u8bc6AJAX\",\"keyword2\":\"2018\\u5e7405\\u670828\\u65e5\"}', '1', '2018-05-28 09:53:02');

-- ----------------------------
-- Table structure for ims_y_med_video
-- ----------------------------
DROP TABLE IF EXISTS `ims_y_med_video`;
CREATE TABLE `ims_y_med_video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '视频id',
  `title` varchar(255) NOT NULL,
  `videourl` text,
  `videotime` varchar(100) NOT NULL,
  `content` text,
  `displayorder` int(4) unsigned zerofill NOT NULL DEFAULT '0000' COMMENT '排序',
  `is_free` enum('试听','不试听') NOT NULL DEFAULT '试听' COMMENT '是否试听 1试听 2不试听',
  `addtime` int(10) NOT NULL,
  `logo_time` int(11) NOT NULL COMMENT '头图',
  `img` char(255) NOT NULL DEFAULT '0',
  `right_img` char(255) NOT NULL DEFAULT '0',
  `left_img` char(255) NOT NULL DEFAULT '0',
  `browsenum` int(11) NOT NULL DEFAULT '1' COMMENT '浏览量',
  `collection` int(11) NOT NULL DEFAULT '0' COMMENT '收藏量',
  `teacherid` int(11) unsigned DEFAULT '0' COMMENT '医生id',
  `status` enum('上架','下架') NOT NULL DEFAULT '上架' COMMENT '上下架 1上 2下',
  `cid` int(11) unsigned DEFAULT '0' COMMENT '分类id',
  `best_num` int(11) unsigned DEFAULT '0' COMMENT '点赞',
  `video_head` varchar(250) DEFAULT NULL COMMENT '头部视频',
  `video_foot` varchar(250) DEFAULT NULL COMMENT '尾部视频',
  `spelling` tinyint(11) NOT NULL DEFAULT '1' COMMENT '拼接状态 1拼接 2不拼接 3已拼接',
  `uniacid` tinyint(1) unsigned DEFAULT '8',
  `type` enum('免费视频','收费视频','变现视频') DEFAULT '免费视频' COMMENT '视频类型 1免费 2 收费 3变现',
  `label` varchar(255) DEFAULT NULL COMMENT '视频搜索标签',
  `bianhao` varchar(200) DEFAULT NULL COMMENT '视频编号',
  `hid` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacherid` (`teacherid`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='视频表';

-- ----------------------------
-- Records of ims_y_med_video
-- ----------------------------
INSERT INTO `ims_y_med_video` VALUES ('5', '画眼睛', 'http://video.yuntehu.com/splicing_video_yes_222.mp4', '', '视频标题', '0000', '试听', '1526366395', '5', 'http://video.yuntehu.com/splicing_video_img_222.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '1', '0', '0', '上架', '141', '0', 'http://video.yuntehu.com/1.mp4', 'http://video.yuntehu.com/3.mp4', '3', '8', '免费视频', null, null, '0000000000');
INSERT INTO `ims_y_med_video` VALUES ('6', '初步认识', 'http://video.yuntehu.com/splicing_video_yes_splicing_video_right_544/呃呃.mp4', '', '七牛 请用URL如：http://wx.haoshu888.com/e1.mp4(注意区分url大小写)，建议使用视频格式用mp4', '0001', '试听', '1526373948', '5', 'http://video.yuntehu.com/splicing_video_img_splicing_video_right_544/呃呃.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '0', '0', '603', '上架', '141', '0', 'http://video.yuntehu.com/544/头.mp4', 'http://video.yuntehu.com/544/尾部.mp4', '3', '8', '变现视频', '1231232323123123', null, '0000000000');
INSERT INTO `ims_y_med_video` VALUES ('7', '初步认识AJAX', 'http://video.yuntehu.com/splicing_video_right_韩盈_b.mp4', '', '视频音频简介', '0011', '不试听', '1526536042', '0', 'http://video.yuntehu.com/splicing_video_img_韩盈_b.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '123', '12', '603', '上架', '141', '11', 'http://video.yuntehu.com/韩盈_1.mp4', 'http://video.yuntehu.com/韩盈_2.mp4', '3', '8', '免费视频', '', '', '0000000000');
INSERT INTO `ims_y_med_video` VALUES ('8', '甲亢患者突眼怎么治？', 'http://video.yuntehu.com/splicing_video_yes_544/谷伟军 汇诊改疑问19：甲亢患者突眼怎么治？20180326---Trim.mp4', '', '甲亢会造成突眼，那么这种突眼需不需要治疗呢？又该怎么治呢？', '0000', '不试听', '1526981594', '7', 'http://video.yuntehu.com/splicing_video_img_544/谷伟军 汇诊改疑问19：甲亢患者突眼怎么治？20180326---Trim.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '0', '0', '317', '上架', '141', '0', 'http://video.yuntehu.com/544/头.mp4', 'http://video.yuntehu.com/544/尾部.mp4', '3', '8', '免费视频', '中国人民解放军总医院，301医院，内分泌科，副主任，谷伟军，副主任医师，甲状腺功能亢进症，甲亢，甲状腺，激素，突眼，眼病，闭眼障碍，治疗，下视露白，戒烟，激素，放疗，手术', '', '0000000016');
INSERT INTO `ims_y_med_video` VALUES ('9', '测试陈进国1', 'http://video.yuntehu.com/splicing_video_yes_1364/王成锋 谣言终结4：慢性胰腺炎会发展成胰腺癌？.mp4', '', '1111111111', '0000', '不试听', '1527056020', '7', 'http://video.yuntehu.com/splicing_video_img_1364/王成锋 谣言终结4：慢性胰腺炎会发展成胰腺癌？.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '0', '0', '0', '上架', '141', '0', 'http://video.yuntehu.com/544/头.mp4', 'http://video.yuntehu.com/544/尾部.mp4', '3', '8', '免费视频', '1111111111111111111', null, '0000000000');
INSERT INTO `ims_y_med_video` VALUES ('10', '测试陈进国2', 'http://video.yuntehu.com/splicing_video_yes_544/小测试11谷伟军 汇诊改疑问19：甲亢患者突眼怎么治？20180326---Trim.mp4', '', '1111111111', '0000', '不试听', '1527056022', '7', 'http://video.yuntehu.com/splicing_video_img_1364/王成锋 谣言终结4：慢性胰腺炎会发展成胰腺癌？.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '0', '0', '0', '上架', '141', '0', 'http://video.yuntehu.com/544/头.mp4', 'http://video.yuntehu.com/544/尾部.mp4', '3', '8', '变现视频', '1111111111111111111', null, '0000000000');
INSERT INTO `ims_y_med_video` VALUES ('11', '测试陈进3', 'http://video.yuntehu.com/splicing_video_yes_1364/王成锋 谣言终结4：慢性胰腺炎会发展成胰腺癌？.mp4', '', '1111111111', '0000', '不试听', '1527056028', '7', 'http://video.yuntehu.com/splicing_video_img_1364/王成锋 谣言终结4：慢性胰腺炎会发展成胰腺癌？.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '0', '0', '0', '上架', '144', '0', 'http://video.yuntehu.com/544/头.mp4', 'http://video.yuntehu.com/544/尾部.mp4', '3', '8', '免费视频', '1111111111111111111', null, '0000000000');
INSERT INTO `ims_y_med_video` VALUES ('12', '小测试', 'http://video.yuntehu.com/splicing_video_yes_544/小测试11谷伟军 汇诊改疑问19：甲亢患者突眼怎么治？20180326---Trim.mp4', '', '111', '0000', '不试听', '1527057539', '7', 'http://video.yuntehu.com/splicing_video_img_544/小测试11谷伟军 汇诊改疑问19：甲亢患者突眼怎么治？20180326---Trim.mp4.jpg', 'http://video.yuntehu.com/re.jpg', 'http://video.yuntehu.com/re.jpg', '0', '0', '0', '上架', '144', '0', 'http://video.yuntehu.com/544/头.mp4', 'http://video.yuntehu.com/544/尾部.mp4', '3', '8', '免费视频', '111', null, '0000000000');
INSERT INTO `ims_y_med_video` VALUES ('13', '姜辉测试', 'http://video.yuntehu.com/splicing_video_yes_1369/部分27.mp4', '', '222', '0000', '不试听', '1527060782', '7', 'http://video.yuntehu.com/splicing_video_img_1369/部分27.mp4.jpg', 'http://video.yuntehu.com/rejpg', 'http://video.yuntehu.com/rejpg', '0', '0', '1362', '上架', '142', '0', 'http://video.yuntehu.com/1369/片尾.mp4', 'http://video.yuntehu.com/1369/0409 最终版片头.mp4', '3', '8', '免费视频', '2222', null, '0000000000');
